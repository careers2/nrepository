package ui.smoke.testcases.CompensationLocalizer;



import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.isElementExisting;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.*;
import static pages.CompensationLocalizer_pages.Input.*;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import pages.CompensationLocalizer_pages.Input;
import pages.CompensationLocalizer_pages.Login;
import pages.CompensationLocalizer_pages.Logout;
import pages.CompensationLocalizer_pages.Preview;
import pages.CompensationLocalizer_pages.Results;
import pages.CompensationLocalizer_pages.Start;
import utilities.InitTests;
import verify.SoftAssertions;

public class CompensationLocalizer_smoke extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailId = "";
	public static String MFAEmailPassword = "";
	Login loginobj;
	Start startobj;
	Input inputobj;
	Preview previewobj;
	Results resultsobj;
	Logout logoutobj;

public CompensationLocalizer_smoke(String appname) {
	super(appname);
}
@BeforeMethod
public void setUp() throws Exception {
	props.load(input);
	MFAChoice = props.getProperty("MFAChoice");
	MFAEmailId = props.getProperty("MFAEmailId");
	MFAEmailPassword = props.getProperty("MFAEmailPassword");
	@SuppressWarnings("unused")
	CompensationLocalizer_smoke obj = new CompensationLocalizer_smoke("CompensationLocalizer");
	test = reports.createTest("Compensation Localizer");
	test.assignCategory("smoke");
	initWebDriver(BASEURL,"PHONTOMJS","latest","","local",test,"");
	loginobj = new Login();
	startobj = new Start();
	inputobj = new Input();
	previewobj = new Preview();
	resultsobj = new Results();
	logoutobj = new Logout();
	
}
@Test(priority = 1, enabled = true)
public void GHRM() throws Exception {
	try {
		loginobj.login(USERNAME, PASSWORD);
		assertTrue(isElementExisting(driver,loginobj.AccountSelectionHeader,20),"Login Successful",test);
		loginobj.accountSelection("MERCER","97216906 GHRM Clients");
		verifyElementTextContains(loginobj.LoggedInAccount,"97216906 GHRM Clients",test);
		startobj.openCompensationLocalizer();
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		verifyElementTextContains(startobj.LoggedInAccount,"97216906",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Germany, Berlin", "Switzerland, Zurich");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("Yes","40000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"0",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("LessThan",10),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
		resultsobj.excelExport();
		
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.pdfExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.runAnotherCalculation();
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Australia, Adelaide", "Vietnam, Hanoi");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.initialise();
		inputobj.setBaseSalary("80000");
		inputobj.setMaritalStatus("Married with 3 children", "Single with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("No","");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setHousingData("Expatriate Level Rental Housing", "Less Expensive Area", "Expatriate Level Rental Housing", "Less Expensive Area");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"22.5",test);
		previewobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("LessThan",100),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
		resultsobj.excelExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.pdfExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		logoutobj.logout();
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} finally {
		reports.flush();
		driver.quit();
	}

	}
@Test(priority = 1, enabled = true)
public void ICS() throws Exception {
	try {
		loginobj.login(USERNAME, PASSWORD);
		assertTrue(isElementExisting(driver,loginobj.AccountSelectionHeader,20),"Login Successful",test);
		loginobj.accountSelection("Mercer (New York)","11325 Multi-National Pay with Americans Abroad - Catherine Bruning");
			verifyElementTextContains(loginobj.LoggedInAccount,"11325 Multi-National Pay with Americans Abroad - Catherine Bruning",test);
		startobj.openCompensationLocalizer();
		verifyElementTextContains(startobj.ApplicationName,"Compensation Localizer",test);
		verifyElementTextContains(startobj.LoggedInAccount,"11325",test);
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("United Kingdom, Urban United Kingdom", "Germany, Berlin");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.setBaseSalary("100000");
		inputobj.setAllowances("5000", "5%");
		inputobj.setDeductions("15000", "10%");
		inputobj.setMaritalStatus("Married with 3 children", "Married with no children");
		inputobj.setExchangeRate("Specific Week");
		inputobj.setTargetSalary("Yes","100000");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		verifyEquals(Integer.parseInt(previewobj.Allowance1Preview.getText()),Allowance1,test);
		verifyEquals(Integer.parseInt(previewobj.Allowance2Preview.getText()),Allowance2,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction1Preview.getText()),Deduction1,test);
		verifyEquals(Integer.parseInt(previewobj.Deduction2Preview.getText()),Deduction2,test);
		assertTrue(previewobj.checkFamilyAllowance("Equals",0),"Family Allowance greater than 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"",test);
		verifyEquals(previewobj.computeHardshipPremium("10"),HardshipPremiumValue,test);
		previewobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("Equals",0),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("GreaterThan",0),"Compensation difference is within the range",test);
		resultsobj.excelExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.pdfExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.runAnotherCalculation();
		verifyElementTextContains(startobj.ActiveTab,"Start",test);
		startobj.startCalculation("Australia, Adelaide", "Egypt, Cairo");
		verifyElementTextContains(startobj.ActiveTab,"Calculation Inputs",test);
		inputobj.initialise();
		inputobj.setBaseSalary("80000");
		inputobj.setMaritalStatus("Single with 3 children", "Single with no children");
		inputobj.setExchangeRate("Specific Month");
		inputobj.setTargetSalary("No","");
		inputobj.setMercerStandardAssumptions("No");
		inputobj.setHousingData("Expatriate Level Rental Housing", "Exclusive Area","Expatriate Level Rental Housing",  "Exclusive Area");
		inputobj.setAdditionalTaxableIncome("10000");
		inputobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"Preview Calculations",test);
		assertTrue(previewobj.checkFamilyAllowance("GreaterThan",0),"Family Allowance equals 0",test);
		verifyEquals(previewobj.computeNetIncome(),NetIncomeValue,test);
		verifyEquals(previewobj.computeCostOfLivingAdjustments(),COLAValue,test);
		assertTrue(previewobj.checkIfCOLAIsNegative(),"COLA Value is negetive",test);
		verifyEquals(previewobj.computeAdjustedNetIncome(),AdjustedNetIncomeValue,test);
		verifyEquals(previewobj.computeAdjustedNetIncomeInHost(),AdjustedNetIncomeInHostValue,test);
//		previewobj.addEducationAllowance();
//		verifyEquals(Integer.parseInt(previewobj.EducationCostAdded.getAttribute("value")),EducationCostValue,test);
		previewobj.addHardshipPremium();
		verifyElementTextContains(previewobj.HardshipPercentageOfGross,"25",test);
		previewobj.setCustomAllowance("1000");
		previewobj.submit();
		verifyElementTextContains(startobj.ActiveTab,"View Results",test);
		verifyEquals(resultsobj.computeHomeCompensation(),HomeTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHomeNetIncome(),HomeNetIncomeValue,test);
		verifyEquals(resultsobj.computeHomeAdjustedNetIncome(),HomeAdjustedNetValue,test);
		verifyEquals(resultsobj.computeCalculatedHostCompensation(),HostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeHostNetIncome(),HostNetIncomeValue,test);
		verifyEquals(resultsobj.computeHostAdjustedNetIncome(),HostAdjustedNetValue,test);
		assertTrue(resultsobj.computeAdjustedNetIncomeInHostDifference("Equals",0),"Adjusted Net Income difference falls within the allowed range",test);
		verifyEquals(resultsobj.computeProposedHostCompensation(),ProposedHostTotalCompensationValue,test);
		verifyEquals(resultsobj.computeProposedHostNetIncome(),ProposedHostNetIncomeValue,test);
		verifyEquals(resultsobj.computeProposedHostAdjustedNetIncome(),ProposedHostAdjustedNetValue,test);
		assertTrue(resultsobj.computeCompensationDifference("LessThan",0),"Compensation difference is within the range",test);
		resultsobj.excelExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		resultsobj.pdfExport();
		assertTrue(resultsobj.isFileDownloaded(home+"\\Downloads","compensation.localizer"),"File was downloaded successfully",test);
		logoutobj.logout();
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

	} finally {
		reports.flush();
		driver.quit();
	}

	}

}