package ui.smoke.testcases.MobilityExchange;



import org.testng.annotations.AfterSuite;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static driverfactory.Driver.*;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.MobilityExchange_pages.Admin;
import pages.MobilityExchange_pages.Dashboard;
import pages.MobilityExchange_pages.Login;
import pages.MobilityExchange_pages.Logout;
import pages.MobilityExchange_pages.Purchase;
import utilities.InitTests;
import verify.SoftAssertions;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.*;

public class MobilityExchange extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailId = "";
	public static String MFAEmailPassword = "";
	Login Loginobj;
	Dashboard Dashboardobj;
	Purchase Purchaseobj;
	Logout Logoutobj;

	public MobilityExchange(String appName) {
		super(appName);
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		props.load(input);
		MFAChoice = props.getProperty("MFAChoice");
		MFAEmailId = props.getProperty("MFAEmailId");
		MFAEmailPassword = props.getProperty("MFAEmailPassword");
		@SuppressWarnings("unused")
		MobilityExchange ME = new MobilityExchange("MobilityExchange");
		test = reports.createTest("Mobility Exchange");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,"CHROME","latest","","local",test,"");
		Loginobj = new Login();
		Dashboardobj = new Dashboard();
		Purchaseobj = new Purchase();
		Logoutobj = new Logout();
	}

	@Test(priority = 1, enabled = true)
	public void GHRM() throws Exception {
		try {
			
			Loginobj.login(USERNAME, PASSWORD);					
				assertTrue(isElementExisting(driver,Loginobj.AccountSelectionHeader,20),"Login Successful",test);
			Loginobj.accountSelection("MERCER","97216906 GHRM Clients");
				verifyElementTextContains(Loginobj.LoggedInAccount,"97216906 GHRM Clients",test);
			
			Dashboardobj.openLink("Balance Sheet Calculator (QA)");
				verifyElementTextContains(Dashboardobj.GHRMApplicationName, "Balance Sheet Calculator",test);
			Dashboardobj.MyDataTab("Cost Of Living");
				assertTrue(Dashboardobj.purchased,"Purchased Products are displayed in My Data section",test);
				assertTrue(Dashboardobj.available,"Available Products for purchase are displayed in My Data Section",test);
			
			Purchaseobj.openProductCatalog();
				assertTrue(isElementExisting(driver,Purchaseobj.ProductCatalogBreadcrumb,20),"Inside Product Catalog page",test);
			Purchaseobj.purchaseProduct("Hardship");
				assertTrue(isElementExisting(driver,Purchaseobj.SecureCheckoutButton,20),"Product(s) added to cart",test);
			Purchaseobj.continueShopping();
			Purchaseobj.purchaseProduct("Mercer Passport");
				assertTrue(isElementExisting(driver,Purchaseobj.SecureCheckoutButton,20),"Product(s) added to cart",test);
			Purchaseobj.checkout();
				verifyElementTextContains(Purchaseobj.OrderComplete,"Thank You! Order Received. ",test);
			
			Logoutobj.logout();
				assertTrue(isElementExisting(driver,Logoutobj.HomePageSurvey,20), "Logged out successfully",test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.quit();
			
		}

	}

	@Test(priority = 2, enabled = false)
	public void ICS() throws Exception {
		try {
			Loginobj.login(USERNAME, PASSWORD);	
			
				assertTrue(isElementExisting(driver,Loginobj.AccountSelectionHeader,20),"Login Successful",test);
			Loginobj.accountSelection("Mercer (New York)","9999 Multi-National Pay with Americans Abroad - Catherine Bruning");
				verifyElementTextContains(Loginobj.LoggedInAccount,"9999 Multi-National Pay with Americans Abroad - Catherine Bruning",test);
			
			Dashboardobj.openLink("Compensation Tables");
				verifyElementTextContains(Dashboardobj.ICSApplicationName, "Current Tables",test);
			Dashboardobj.MyDataTab("Education Report");
			assertTrue(Dashboardobj.purchased,"Purchased Products are displayed in My Data section",test);
			assertTrue(Dashboardobj.available,"Available Products for purchase are displayed in My Data Section",test);
			
			Purchaseobj.openProductCatalog();
				assertTrue(isElementExisting(driver,Purchaseobj.ProductCatalogBreadcrumb,20),"Inside Product Catalog page",test);
			Purchaseobj.purchaseProduct("Car Norms");
				assertTrue(isElementExisting(driver,Purchaseobj.SecureCheckoutButton,20),"Product(s) added to cart",test);
			Purchaseobj.continueShopping();
			Purchaseobj.purchaseProduct("Compensation Localizer");
				assertTrue(isElementExisting(driver,Purchaseobj.SecureCheckoutButton,20),"Product(s) added to cart",test);
			Purchaseobj.checkout();
				verifyElementTextContains(Purchaseobj.OrderComplete,"Thank You! Order Received. ",test);
			
			Logoutobj.logout();
				assertTrue(isElementExisting(driver,Logoutobj.HomePageSurvey,20), "Logged out successfully",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

			ATUReports.add("ICS()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

			ATUReports.add("ICS()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.quit();

		}
	}

	@Test(priority = 3, enabled = false)
	public void Admin() throws Exception {
		try {
			Admin Adminobj = new Admin();
			Loginobj.login(USERNAME, PASSWORD);	
			Loginobj.accountSelection("MERCER","97216906 GHRM Clients");
			Adminobj.adminPage();
				assertTrue(isElementExisting(driver,Adminobj.ProductCatalogCreationButton,20),"Product Catalog Creation Button verified",test);
				assertTrue(isElementExisting(driver,Adminobj.PublishUnpublishButton,20),"Product - Publish/unPublish Button verified",test);
				assertTrue(isElementExisting(driver,Adminobj.UpdateProductDescriptionsButton,20),"Update product Descriptions Button verified",test);
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);

			ATUReports.add("Admin()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Admin()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.quit();
		}
	}


	@AfterSuite
	public void kill() {
		killBrowserExe(BROWSER_TYPE);
	}
}
