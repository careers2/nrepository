package ui.smoke.testcases.dataSubmission;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.DataSubmission_Pages.Uploadpage;
import utilities.InitTests;
import verify.SoftAssertions;


	public class TC01_Uploadfile extends InitTests {
		public TC01_Uploadfile(String appName) {
			super(appName);
			// TODO Auto-generated constructor stub
		}

		@Test(enabled = true)
		public void uploadfile() throws Exception {
			try {
				test = reports.createTest("Logging to data submission");
				test.assignCategory("smoke");
				
				TC01_Uploadfile file = new TC01_Uploadfile("DataSubmission");
				
				System.out.println("Logging in to application");
				
				initWebDriver(BASEURL, "CHROME", "","","local", test, "");
				System.out.println("BaseURL is: " + BASEURL);

				Uploadpage uploadfile=new Uploadpage();
				
				waitForElementToDisplay(Uploadpage.surveyTitle);
				SoftAssertions.verifyElementTextContains(Uploadpage.surveyTitle, "Survey Submission Upload",test);
				
				waitForElementToDisplay(Uploadpage.RegionLabel);
				Driver.selEleByVisbleText(Uploadpage.regionDropDown,"United States");
				
				waitForElementToDisplay(Uploadpage.languageLabel);
				Driver.selEleByVisbleText(Uploadpage.languageDropDown,"English (United States)");
				
			
				uploadfile.uploadFile();
				
				waitForElementToDisplay(Uploadpage.successMgs);
				SoftAssertions.verifyElementTextContains(Uploadpage.successMgs, "Your file file.xls was uploaded successfully.",test);
				
				
				

			} catch (Error e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} catch (Exception e) {
				e.printStackTrace();

				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			}

		}

		@AfterSuite
		public void killDriver() {

			killBrowserExe(BROWSER_TYPE);

		}

	}