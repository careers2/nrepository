package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;


public class TC_9_Attachment extends InitTests {
	
	public TC_9_Attachment(String appName) {
		super(appName);
		}

	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_9() throws Exception {
	try {
		TC_9_Attachment a1=new TC_9_Attachment("WIN");
		test = reports.createTest("Diagnostic Report");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		Attachment attach= new Attachment();
		attach.accessAttachment();
		Title tt=new Title();
		waitForElementToDisplay(tt.title_tab);
		verifyElementText(tt.title_tab , "Attachments",test);
	
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_9", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_9()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
	
		reports.flush();
		driver.close();

	}
}
}