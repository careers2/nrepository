package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import pages.WINPages.*;

public class TC_2_GlobalPrint extends InitTests{
	

	public TC_2_GlobalPrint(String appName) {
	super(appName);
	}
	

	SoftAssert softAssert = new SoftAssert();
	
				@Test(priority = 1, enabled =true)
				public void WIN_TC_2() throws Exception {
				try {
					TC_2_GlobalPrint a1=new TC_2_GlobalPrint("WIN");
					test = reports.createTest("Global Print Functionality");
					test.assignCategory("smoke");
					initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
					LoginPage loginPage = new LoginPage();
					loginPage.login(USERNAME,PASSWORD);
					selectOrgPage Org= new selectOrgPage();
					Org.Organization();
					MMDPage marketData= new MMDPage();
					marketData.global();
					waitForElementToDisplay(MMDPage.global_MercerMarketData);
					verifyElementText(MMDPage.global_MercerMarketData , "Mercer Market Data for Year: ",test);
					GlobalSearchPage search= new GlobalSearchPage();
					search.GlobalJobView();
					waitForElementToDisplay(GlobalSearchPage.MMDResultPage);
					verifyElementText(GlobalSearchPage.MMDResultPage , "Mercer Market Data Results",test);
					
					Global_PrintFunctionality Print = new Global_PrintFunctionality();
					Global_Export Export = new Global_Export();
					Print.chartPrint();
					Export.GlobalReportfileOptions("globalChart");
					//Export.closePopup();
					ProgressCenter prog= new ProgressCenter();
					prog.waitReportGeneration();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "globalChart",test);
					prog.progress(prog.GlobalProgress_close);
					
					Print.GridPrint();
					Export.GlobalReportfileOptions("globalGrid");
					//Export.closePopup();
					prog.waitReportGeneration();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "globalGrid",test);
					prog.progress(prog.GlobalProgress_close);
					
					Export.sendGlobalexport();
					Export.GlobalReportfileOptions("Global_Export");
					prog.waitReportGeneration();
					prog.progress(prog.GlobalprogressCenter_button);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					waitForElementToDisplay(ProgressCenter.globalreport_Table);
					delay(3000);
					waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
					verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
					waitForElementToDisplay(ProgressCenter.Globalreport_name);
					verifyElementText(ProgressCenter.Globalreport_name, "Global_Export",test);
					prog.progress(prog.GlobalProgress_close);
					
					Sign_out sign = new Sign_out();
					sign.signout(sign.signout_1);
					
				} catch (Error e) {
					e.printStackTrace();
					SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
					ATUReports.add("WIN_TC_2", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
					

				} catch (Exception e) {
					e.printStackTrace();
					SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
					ATUReports.add("WIN_TC_2()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

				} 
				finally
				{
					reports.flush();
					driver.close();

				}
			}			
}
