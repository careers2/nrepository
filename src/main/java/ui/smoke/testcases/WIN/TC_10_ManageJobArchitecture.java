package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;

public class TC_10_ManageJobArchitecture extends InitTests{
	
	public TC_10_ManageJobArchitecture(String appName) {
		super(appName);
		}

		SoftAssert softAssert = new SoftAssert();
		
		@Test(priority = 1, enabled = true)
		public void WIN_TC_10() throws Exception {
			try {
				TC_10_ManageJobArchitecture a1=new TC_10_ManageJobArchitecture("WIN");
				test = reports.createTest("Admin");
				test.assignCategory("smoke");
				initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
				LoginPage loginPage = new LoginPage();
				loginPage.login(USERNAME,PASSWORD);
				selectOrgPage Org= new selectOrgPage();
				Org.Organization();
				Title tt= new Title();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab , "My Homepage",test);
				
				WidgetsLink widget= new WidgetsLink();
				widget.accessLink(WidgetsLink.adminPage_link);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
				
				label_ManageJobArchitecture lab = new label_ManageJobArchitecture();
				AdministrationPage admin= new AdministrationPage();
				admin.accessAdminTab(admin.manageJobAchitecture_Tab);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Job Architecture:",test);
				lab.label();
				verifyElementText(lab.family_Hierarchy , "Family Hierarchy Labels",test);
				verifyElementText(lab.career_stream , "Career Stream Labels",test);
				lab.save();
				
				Export_ManageJobArchitecture exp = new Export_ManageJobArchitecture();
				exp.export();
				verifyElementText(exp.file_Type, "File Type",test);
				verifyElementText(exp.architecture_selection, "Architecture Selection",test);
				verifyElementText(exp.Hierachy_selection, "Hierarchy Selection",test);
				MJl_Export mjl= new MJl_Export();
				mjl.sendToProgressCenter(exp.export_button);
				waitForElementToDisplay(mjl.Mjl_notification);
				verifyElementText(mjl.Mjl_notification, "This item has been sent to the Progress Center for processing.",test);
				mjl.sendToProgressCenter(mjl.MJlclose_Notification);
				ProgressCenter prog= new ProgressCenter();
				prog.waitReportGeneration();
				prog.progress(prog.JobprogressCenter_button);
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobreport_name, "MUPCS Catalog_Career Stream",test);
				prog.progress(prog.JobprogressCenter_button);
				
				Sign_out sign = new Sign_out();
				sign.signout(sign.signout_2);
				
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_10()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_10()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} 
			finally
			{
				
				reports.flush();
				driver.close();

			}
		}

}