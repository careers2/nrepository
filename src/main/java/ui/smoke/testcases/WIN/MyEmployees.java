package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import utilities.InitTests;

public class MyEmployees extends InitTests{
		
		SoftAssert softAssert = new SoftAssert();
		public MyEmployees(String appName) {
			super(appName);
			}

		@Test()
		public void testSearchJob() throws Exception {
			try {
				MyEmployees a1=new MyEmployees("WIN");
				test = reports.createTest("testSearchJob");
				test.assignCategory("smoke");
				initWebDriver(BASEURL,"CHROME", "", "", "local", test, "");
				
				Login loginPage = new Login();			
				loginPage.login(USERNAME,PASSWORD);				
	
				
				Select_Organization selectorg=new Select_Organization();
				assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
				selectorg.select_organization();
				assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);
				
				My_Employees myempl= new My_Employees();
				myempl.Myemployees();
				assertTrue(My_Employees.Myemployees.isDisplayed(),"WIN Application has reached My Employees page",test);
				
				MyEmpdetails myemp = new MyEmpdetails();
				myemp.empdetails();
				assertTrue(myemp.empdetailslinkclose.isDisplayed(),"Employee details pop up accessed",test);
				myemp.empdetailsclose();
				myemp.jobdetails();	
				assertTrue(myemp.jobdetailsclose.isDisplayed(),"Job Details popup accessed",test);
				myemp.jobdetailsclose();
				
				MyEmployees_Edit_View myemployeeseditview= new MyEmployees_Edit_View();
				myemployeeseditview.editviewbutton();
				assertTrue(myemployeeseditview.editviewpage.isDisplayed(),"Edit view page",test);				
				myemployeeseditview.editviewselections();				
				myemployeeseditview.edit_view_save();
		
				
				AddEmployee empadd= new AddEmployee();
				empadd.addemp();
				verifyElementText(empadd.employeename,empadd.empname,test);
				empadd.addempclose();
				
				
				MyEmployees_Resultspage employeeresultspage= new MyEmployees_Resultspage();
				employeeresultspage.resultspage();
				assertTrue(myemp.empdetailslinkclose.isDisplayed(),"Employee details pop up accessed",test);
				employeeresultspage.emplinkclose();
				employeeresultspage.jobdetails();
				assertTrue(myemp.jobdetailsclose.isDisplayed(),"Job Details popup accessed",test);
				employeeresultspage.jobdetailsclose();
				employeeresultspage.save();	
				verifyEquals(employeeresultspage.resultsetname,getTextFromWebElementUsingJavaScript(MyEmployees_Resultspage.pagetitle),test);
				employeeresultspage.resultset();
				verifyEquals(employeeresultspage.resultsetname,getTextFromWebElementUsingJavaScript(MyEmployees_Resultspage.pagetitle),test);
				
				Results_page_Print print=new Results_page_Print();
				print.print_results();
				ProgressCenter prog= new ProgressCenter();
				prog.waitReportGeneration();
				prog.progress(prog.Progress_Center);
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
				prog.progress(prog.GlobalProgress_close);
				
				Results_page_Export export=new Results_page_Export();
				export.export_results();
				prog.waitReportGeneration();
				prog.progress(prog.Progress_Center);
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
				prog.progress(prog.GlobalProgress_close);
				export.export_results_xls();
				prog.waitReportGeneration();
				prog.progress(prog.Progress_Center);
				verifyElementText(ProgressCenter.complete_status, "Complete",test);
				verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
				prog.progress(prog.GlobalProgress_close);
				
				
				Sign_out sign_out=new Sign_out();
				sign_out.signout();

			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearchJob()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("testSearchJob()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} 
			finally
			{
				reports.flush();
				driver.close();

			}

		}
}
