package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;

import utilities.InitTests;
import verify.SoftAssertions;

public class Administration extends InitTests {
	
SoftAssert softAssert = new SoftAssert();
     public Administration(String appName) 
     {
	super(appName);
    }
	
	@Test
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  Administration a1=new Administration("WIN");
		  test = reports.createTest("testSearch");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);
		  
		  AdminPage adminpage=new AdminPage();
		  adminpage.Adminbutton();
		  assertTrue(AdminPage.Administration.isDisplayed(),"Administration page",test);		  
		 			
		  AdministrationPage admin= new AdministrationPage();
		  Title tt=new Title();
			admin.accessAdminTab(admin.resetData_Button);
			waitForPageLoad();
			waitForElementToDisplay(tt.title_tab);
			verifyElementText(tt.title_tab , "Reset My Data",test);
			
			navigateViaBreadCrumb navigate = new navigateViaBreadCrumb();
			navigate.navigateBack(navigate.admin_breadcrumb);
			waitForPageLoad();
			waitForElementToDisplay(tt.title_tab);
			verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
			admin.accessAdminTab(admin.addMercerData);
			waitForElementToDisplay(admin.addMercerData_popup);
			verifyElementText(admin.addMercerData_popup, "Add Mercer Data",test);
			admin.accessAdminTab(admin.ok_button);
			waitForElementToDisplay(tt.MMD_title);
			verifyElementText(tt.MMD_title , " Mercer Job Library for Year: ",test);
			
			navigate.navigateusingtools(navigate.Tools_dropdown);
			waitForPageLoad();
			waitForElementToDisplay(tt.title_tab);
			verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
			
			 mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
				admin.accessAdminTab(admin.mercerJobLibrary_button);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Job Library",test);
				MJLSearchPage.admin_mjl();
				waitForElementToDisplay(MJLSearchPage.admin_tableresult);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Job Library Results",test);
				
				sendToMyLibraryPage send= new sendToMyLibraryPage();
				send.sendRecord(send.MJlsendToLibraryTab);
				send.generateJobcode();	
				waitForElementToDisplay(send.referenceJobText_message);
				verifyElementText(send.referenceJobText_message, "The data has now been sent to My Reference Job Library and is now available for use in Job Evaluation",test);
				send.closePopup(send.alert_close);
				 
				MJL_PrintFunctionality Print = new MJL_PrintFunctionality();
				MJl_Export Export = new MJl_Export();
				Print.printReport(Print.list_tab);
				Export.generateReport("MJL_List");
				Export.sendToProgressCenter(Export.MJlclose_Notification);
				Print.printReport(Print.summary_tab);
				Export.generateReport("MJL_summary");
				Export.sendToProgressCenter(Export.MJlclose_Notification);
				ProgressCenter prog= new ProgressCenter();
				prog.waitforReportGeneration();
				prog.progress(prog.JobprogressCenter_button);
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobreport_name, "MJL_summary",test);
				prog.progress(prog.JobprogressCenter_button);
		  
				navigate.navigateBack(navigateViaBreadCrumb.Administration);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
				
		  Manage_Benchmark_Settings benchmark=new Manage_Benchmark_Settings();
		  benchmark.benchmarkbutton();
		  assertTrue(Manage_Benchmark_Settings.Benchmarkpopup.isDisplayed(),"Manage Benchmark Settings pop up accessed",test);
		  benchmark.benchmarkpopupcancel();
		  
		  Manage_User_Profiles userprofiles=new Manage_User_Profiles();
		  userprofiles.Profiles();
		  assertTrue(Manage_User_Profiles.ManageUserProfile.isDisplayed(),"Manage User Profile page",test);
		  userprofiles.Profilelink();
		  
		  Manage_IPE_Settings ipesettings=new Manage_IPE_Settings();
		  ipesettings.ipe();
		  assertTrue(Manage_IPE_Settings.IPESettingspopup.isDisplayed(),"IPE Settings pop up",test);
		  ipesettings.ipepopup();
		  
		  MUPCS_Catalog mupcs=new MUPCS_Catalog();
		  mupcs.mupcscatalog();
		  assertTrue(MUPCS_Catalog.MUPCSCatalogpage.isDisplayed(),"MUPCS Catalog Search page",test);
		  mupcs.mupcscatalogpage();
		  
		  MUPCS_Catalog_Results_Page mupcsresultspage=new MUPCS_Catalog_Results_Page();
		  mupcsresultspage.mupcscatalogresultspage();
		  assertTrue(MUPCS_Catalog_Results_Page.MUPCSCatalogResultspage.isDisplayed(),"MUPCS Catalog Results page",test);
		  assertTrue(MUPCS_Catalog_Results_Page.Sendtomyreferencelibrary.isDisplayed(),"Send To mY Reference Library button is Enabled",test);
		  mupcsresultspage.mupcscatalogresults();
		  verifyElementText(MUPCS_Catalog_Results_Page.Sendtomyreferencelibrarypopup, "The data has now been sent to My Reference Job Library and is now available for use in Job Evaluation",test);
		  mupcsresultspage.sendtoreferenecelibrarypopup();
		  
		  Results_page_Print print=new Results_page_Print();
		  print.print_results();
		  prog.waitReportGeneration();
		  prog.waitReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
		  prog.progress(prog.GlobalProgress_close);
		  
		  
		  Job_Architecture_Family_Tab jobarch=new Job_Architecture_Family_Tab();
		  jobarch.JobArchitecturefamily();
		  assertTrue(jobarch.FamilyTab.isDisplayed(),"Family Tab",test);
		  assertTrue(jobarch.newbutton.isDisplayed(),"New Button",test);
		  assertTrue(jobarch.copyall.isDisplayed(),"Copy All Button",test);
		  jobarch.sortverify();
		  assertTrue(jobarch.sortorder1.isDisplayed(),"Sortorder 1",test);
		  assertTrue(jobarch.sortorder2.isDisplayed(),"Sortorder 2",test);
		  assertTrue(jobarch.sortorder3.isDisplayed(),"Sortorder 3",test);
		  assertTrue(jobarch.sortorder4.isDisplayed(),"Sortorder 4",test);
		  jobarch.selectverify();
		  assertTrue(jobarch.selectorder2.isDisplayed(),"Select Order 1",test);
		  assertTrue(jobarch.selectorder2.isDisplayed(),"Select Order 2",test);
		  jobarch.expand();
		  
		  Job_Architecture_Career_Stream_Tab Careerstreamtab=new Job_Architecture_Career_Stream_Tab();
		  Careerstreamtab.JobArchitecturecareer();
		  assertTrue(Careerstreamtab.CareerStreamtab.isDisplayed(),"Career Stream Tab",test);
		  assertTrue(Careerstreamtab.newbutton.isDisplayed(),"New Button",test);
		  assertTrue(Careerstreamtab.copyall.isDisplayed(),"Copy All Button",test);
		  Careerstreamtab.sortverify();
		  assertTrue(Careerstreamtab.sortorder1.isDisplayed(),"Sortorder 1",test);
		  assertTrue(Careerstreamtab.sortorder2.isDisplayed(),"Sortorder 2",test);
		  assertTrue(Careerstreamtab.sortorder3.isDisplayed(),"Sortorder 3",test);
		  assertTrue(Careerstreamtab.sortorder4.isDisplayed(),"Sortorder 4",test);
		  Careerstreamtab.selectverify();
		  assertTrue(Careerstreamtab.selectorder2.isDisplayed(),"Select Order 1",test);
		  assertTrue(Careerstreamtab.selectorder2.isDisplayed(),"Select Order 2",test);
		  Careerstreamtab.expand();
		  
		  label_ManageJobArchitecture lab = new label_ManageJobArchitecture();
			lab.label();
			verifyElementText(lab.family_Hierarchy , "Family Hierarchy Labels",test);
			verifyElementText(lab.career_stream , "Career Stream Labels",test);
			lab.save();
			
			Export_ManageJobArchitecture exp = new Export_ManageJobArchitecture();
			exp.export();
			verifyElementText(exp.file_Type, "File Type",test);
			verifyElementText(exp.architecture_selection, "Architecture Selection",test);
			verifyElementText(exp.Hierachy_selection, "Hierarchy Selection",test);
			MJl_Export mjl= new MJl_Export();
			mjl.sendToProgressCenter(exp.export_button);
			waitForElementToDisplay(mjl.Mjl_notification);
			verifyElementText(mjl.Mjl_notification, "This item has been sent to the Progress Center for processing.",test);
			mjl.sendToProgressCenter(mjl.MJlclose_Notification);
			prog.waitReportGeneration();
			prog.progress(prog.JobprogressCenter_button);
			waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
			verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
			waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
			verifyElementText(ProgressCenter.Jobreport_name, "MUPCS Catalog_Career Stream",test);
			prog.progress(prog.JobprogressCenter_button);
			
		  Sign_out sign_out=new Sign_out();
		  sign_out.signout();
		  

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		driver.close();

	}
}


}
