package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;
import java.util.concurrent.TimeUnit;
import verify.SoftAssertions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;

public class Tool_Links extends InitTests{
	
	public Tool_Links(String appName) {
		super(appName);
		}

	SoftAssert softAssert = new SoftAssert();
	
	@Test()
    public void testSearch() throws Exception {
	  try
	  {
		  test = reports.createTest("Tool_Links");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  SelectOrg so = new SelectOrg();
		  assertTrue(SelectOrg.orgname.isDisplayed(),"WIN QA Organization [winqa123]')]",test);
		  so.Organization();
		  HomePage hp = new HomePage();
		  assertTrue(HomePage.homepage.isDisplayed(),"My Homepage",test);
		  hp.HomePage();
		  
		  Tools tl = new Tools();
		  
		  tl.About();
		  waitForElementToDisplay(Tools.About_Page);
		  verifyElementText(Tools.About_Page , "About",test);
		  tl.About_Page();
		 
		  tl.Administration();
		  waitForElementToDisplay(Tools.Administration_Page);
		  verifyElementText(Tools.Administration_Page , "Mercer WIN - System Administration",test);
		  tl.Home();
		 
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  
		  tl.My_Profile();
		  waitForElementToDisplay(Tools.User_Profile_Page);
		  verifyElementText(Tools.User_Profile_Page , "winqa01 test user1 - WIN QA Organization's Profile",test);
		  tl.Home();

		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Tool_Links()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("Tool_Links()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{

		reports.flush();
		driver.close();
	}
  }

}




