package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;

public class TC_8_Admin extends InitTests{
	
	public TC_8_Admin(String appName) {
		super(appName);
		}

		SoftAssert softAssert = new SoftAssert();
		
		@Test(priority = 1, enabled = true)
		public void WIN_TC_8() throws Exception {
			try {
				TC_8_Admin a1=new TC_8_Admin("WIN");
				test = reports.createTest("Admin");
				test.assignCategory("smoke");
				initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
				LoginPage loginPage = new LoginPage();
				loginPage.login(USERNAME,PASSWORD);
				selectOrgPage Org= new selectOrgPage();
				Org.Organization();
				Title tt= new Title();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab , "My Homepage",test);
				
				WidgetsLink widget= new WidgetsLink();
				widget.accessLink(WidgetsLink.adminPage_link);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
				
				mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
				AdministrationPage admin= new AdministrationPage();
				admin.accessAdminTab(admin.mercerJobLibrary_button);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Job Library",test);
				MJLSearchPage.admin_mjl();
				waitForElementToDisplay(MJLSearchPage.admin_tableresult);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Job Library Results",test);
				
				sendToMyLibraryPage send= new sendToMyLibraryPage();
				send.sendRecord(send.MJlsendToLibraryTab);
				send.generateJobcode();
				waitForElementToDisplay(send.referenceJobText_message);
				verifyElementText(send.referenceJobText_message, "The data has now been sent to My Reference Job Library and is now available for use in Job Evaluation",test);
				send.closePopup(send.alert_close);
				
				MJL_PrintFunctionality Print = new MJL_PrintFunctionality();
				MJl_Export Export = new MJl_Export();
				Print.printReport(Print.list_tab);
				Export.generateReport("MJL_List");
				Export.sendToProgressCenter(Export.MJlclose_Notification);
				Print.printReport(Print.summary_tab);
				Export.generateReport("MJL_summary");
				Export.sendToProgressCenter(Export.MJlclose_Notification);
				/*ProgressCenter prog= new ProgressCenter();
				prog.waitdiagnosticReportGeneration();
				prog.progress(prog.JobprogressCenter_button);
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobcomplete_status, "Complete");
				waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
				verifyElementText(ProgressCenter.Jobreport_name, "MJL_summary");
				prog.progress(prog.JobprogressCenter_button);*/
				
				navigateViaBreadCrumb navigate = new navigateViaBreadCrumb();
				navigate.navigateBack(navigate.admin_breadcrumb);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
				admin.accessAdminTab(admin.resetData_Button);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab , "Reset My Data",test);
				
				
				navigate.navigateBack(navigate.admin_breadcrumb);
				waitForPageLoad();
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
				admin.accessAdminTab(admin.addMercerData);
				waitForElementToDisplay(admin.addMercerData_popup);
				verifyElementText(admin.addMercerData_popup, "Add Mercer Data",test);
				admin.accessAdminTab(admin.ok_button);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , " Mercer Job Library for Year: ",test);
				
				Sign_out sign = new Sign_out();
				sign.signout(sign.signout_2);
				
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_8()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_8()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} 
			finally
			{
			
				reports.flush();
				driver.close();

			}
		}

}

