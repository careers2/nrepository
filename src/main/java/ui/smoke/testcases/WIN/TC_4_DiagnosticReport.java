package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;

public class TC_4_DiagnosticReport extends InitTests {
	

	public TC_4_DiagnosticReport(String appName) {
	super(appName);
	}
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_4() throws Exception {
	try {
		TC_4_DiagnosticReport a1=new TC_4_DiagnosticReport("WIN");
		test = reports.createTest("Diagnostic Report");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		DiagnosticReportPage diagnostic= new DiagnosticReportPage();
		diagnostic.ClickDiagnosticReport();
		waitForElementToDisplay(DiagnosticReportPage.diagnosticPage);
		verifyElementText(DiagnosticReportPage.diagnosticPage , "Diagnostic Report",test);
		diagnostic.ReportSelections();
		diagnostic.RunReport();
		ProgressCenter prog= new ProgressCenter();
		prog.waitdiagnosticReportGeneration();
		prog.progress(prog.DiagnosticProgressCenter_button);
		waitForElementToDisplay(ProgressCenter.Globalcomplete_status);
		verifyElementText(ProgressCenter.Globalcomplete_status, "Complete",test);
		waitForElementToDisplay(ProgressCenter.Globalreport_name);
		verifyElementText(ProgressCenter.Globalreport_name, "DiagnosticReport_GlobalMVE2E3",test);
		prog.progress(prog.GlobalProgress_close);
		
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_4", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_4()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
}
}
