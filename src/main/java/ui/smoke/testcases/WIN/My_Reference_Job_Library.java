package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class My_Reference_Job_Library extends InitTests {
SoftAssert softAssert = new SoftAssert();
	public My_Reference_Job_Library(String appName) {
	super(appName);
	}
	
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  test = reports.createTest("testSearch");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);
		  
		  My_Reference_Job_Library_Search_page referencejobs= new My_Reference_Job_Library_Search_page();
		  referencejobs.reference_jobs();
		  assertTrue(My_Reference_Job_Library_Search_page.MyReferenceJobLibrary.isDisplayed(),"WIN Application has reached Reference Library page",test);
		  
		  My_Reference_Job_Library_details reference_jobs = new My_Reference_Job_Library_details();
		  reference_jobs.reference_jobs_details(); 
		  assertTrue(My_Reference_Job_Library_details.popupclose.isDisplayed(),"Details pop up accessed",test);
		  reference_jobs.detailsclose();
		  
		  My_Reference_Job_Library_Resultspage results_page=new My_Reference_Job_Library_Resultspage();
		  results_page.referenece_jobs_results_page();
		  assertTrue(My_Reference_Job_Library_details.popupclose.isDisplayed(),"Details pop up accessed",test);
		  results_page.	detailsclose();	  
		  results_page.save();
		  verifyEquals(results_page.resultsetname,getTextFromWebElementUsingJavaScript(My_Reference_Job_Library_Resultspage.pagetitle),test);
		  results_page.resultset();
		  verifyEquals(results_page.resultsetname,getTextFromWebElementUsingJavaScript(My_Reference_Job_Library_Resultspage.pagetitle),test);
		  
		  	Results_page_Print print=new Results_page_Print();
			print.print_results();
			ProgressCenter prog= new ProgressCenter();
			prog.waitReportGeneration();
			prog.progress(prog.Progress_Center);
			verifyElementText(ProgressCenter.complete_status, "Complete",test);
			verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
			prog.progress(prog.GlobalProgress_close);
			
			Results_page_Export export=new Results_page_Export();
			export.export_results();
			prog.waitReportGeneration();
			prog.progress(prog.Progress_Center);
			verifyElementText(ProgressCenter.complete_status, "Complete",test);
			verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
			prog.progress(prog.GlobalProgress_close);
			export.export_results_xls();
			prog.waitReportGeneration();
			prog.progress(prog.Progress_Center);
			verifyElementText(ProgressCenter.complete_status, "Complete",test);
			verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
			prog.progress(prog.GlobalProgress_close);
			
			Sign_out sign_out=new Sign_out();
			sign_out.signout();
		  
		  
		  
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		driver.close();

	}

	}
	}
