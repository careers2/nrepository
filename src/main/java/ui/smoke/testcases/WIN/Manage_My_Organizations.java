package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;

public class Manage_My_Organizations extends InitTests {
SoftAssert softAssert = new SoftAssert();
  
    public Manage_My_Organizations(String appName) {
	super(appName);
     }
	
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  Manage_My_Organizations a1=new Manage_My_Organizations("WIN");
		  test = reports.createTest("testSearch");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);
		  
		  Manage_My_Organization organizationpage=new Manage_My_Organization();
		  organizationpage.organization();
		  
		  organizationpage.organizationpage();
		  
		  Organization_Page_Edit_View edit= new Organization_Page_Edit_View();
		  edit.editviewbutton();
		  assertTrue(edit.editviewpage.isDisplayed(),"Edit view page",test);
		  edit.editviewselections();
		  edit.edit_view_save();
		  
		  Manage_My_Organization_Resultspage resultspage=new Manage_My_Organization_Resultspage();
		  resultspage.Organization_results_page();
		  resultspage.detailsclose();
		  resultspage.rowedit();
		  resultspage.Resultspage();
		  resultspage.save();
		  verifyEquals(Manage_My_Organization_Resultspage.resultsetname,getTextFromWebElementUsingJavaScript(Manage_My_Organization_Resultspage.pagetitle),test);
		  resultspage.resultset();
		  verifyEquals(Manage_My_Organization_Resultspage.resultsetname,getTextFromWebElementUsingJavaScript(Manage_My_Organization_Resultspage.pagetitle),test);
		  
		  Results_page_Print print=new Results_page_Print();
		  print.print_results();
		  ProgressCenter prog= new ProgressCenter();
		  prog.waitforReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
		  prog.progress(prog.GlobalProgress_close);
		  
		  Results_page_Export export=new Results_page_Export();
		  export.export_results();
		  prog.waitforReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.progress(prog.GlobalProgress_close);
		  export.export_results_xls();
		  prog.waitforReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.progress(prog.GlobalProgress_close);
			
		  Sign_out sign_out=new Sign_out();
		  sign_out.signout();
		  

	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		driver.close();

	}
}

}
