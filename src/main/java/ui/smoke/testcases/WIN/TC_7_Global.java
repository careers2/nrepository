package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import pages.WINPages.*;

public class TC_7_Global extends InitTests{
	
	public TC_7_Global(String appName) {
		super(appName);
		}
		
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_7() throws Exception {
	try {
		TC_7_Global a1=new TC_7_Global("WIN");
		test = reports.createTest("Global Print Functionality");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		MMDPage marketData= new MMDPage();
		marketData.global();
		waitForElementToDisplay(MMDPage.global_MercerMarketData);
		verifyElementText(MMDPage.global_MercerMarketData , "Mercer Market Data for Year: ",test);
		GlobalSearchPage search= new GlobalSearchPage();
		search.GlobalJobView();
		waitForElementToDisplay(GlobalSearchPage.MMDResultPage);
		verifyElementText(GlobalSearchPage.MMDResultPage , "Mercer Market Data Results",test);
		
		Global_editView edit = new Global_editView();
		edit.editviewbutton();
		Title tt= new Title();
		waitForElementToDisplay(tt.title_tab);
		verifyElementText(tt.title_tab , "Edit View: ",test);
		edit.editviewselections();
		edit.edit_view_save();
		
		refineMarketPage refine= new refineMarketPage();
		refine.ClickonRefinement(refine.GloablrefineMarket_Tab);
		waitForElementToDisplay(tt.title_tab);
		verifyElementText(tt.title_tab , "Refine Market",test);
		refine.peerGroupRefinement();
		waitForElementToDisplay(refine.globalresult_page);
		waitForElementToDisplay(refine.peer_Resultpage);
		verifyElementText(refine.peer_Resultpage , "Refine Market ",test);
		
		sendToMyLibraryPage send= new sendToMyLibraryPage();
		send.sendRecord(send.GlobalsendToLibrary_Tab);
		waitForElementToDisplay(send.Global_popup);
		verifyElementText(send.Global_popup, "This data has been sent to the My Market Library and is now available for benchmarking.",test);
		send.closePopup(send.popup_close);
		
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_7", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_7()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		
		reports.flush();
		driver.close();

	}
}	
}