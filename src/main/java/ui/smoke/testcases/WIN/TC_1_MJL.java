package ui.smoke.testcases.WIN;


import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;
import pages.WINPages.*;

public class TC_1_MJL extends InitTests{
	
		public TC_1_MJL(String appName) {
		super(appName);
		}
		
		SoftAssert softAssert = new SoftAssert();
		
		@Test(priority = 1, enabled = true)
		public void WIN_TC_1() throws Exception {
			try {
				TC_1_MJL a1=new TC_1_MJL("WIN");
				test = reports.createTest("Job View");
				test.assignCategory("smoke");
				initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
				LoginPage loginPage = new LoginPage();
				loginPage.login(USERNAME,PASSWORD);
				selectOrgPage Org= new selectOrgPage();
				Org.Organization();
				MMDPage marketData= new MMDPage();
				marketData.jobLibrary();
				waitForElementToDisplay(MMDPage.mercerJobLibrary);
				verifyElementText(MMDPage.mercerJobLibrary , "Mercer Job Library",test);
				Title tt=new Title();
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Market Data for Year: ",test);
				
				mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
				MJLSearchPage.jobView();
				waitForElementToDisplay(mercerJobLibraryPage.currentViewResult);
				verifyElementText(mercerJobLibraryPage.currentViewResult , "Job",test);
				
				editViewPage Edit= new editViewPage();
				Edit.clickEdit();
				Edit.select_mercerMarketData();
				Edit.select_Statistic();
				Edit.select_Save();
				waitForElementToDisplay(editViewPage.result_page);
				verifyElementText(tt.MMD_title , "Mercer Market Data Results: Library",test);
				waitForElementToDisplay(editViewPage.currentViewResult);
				verifyElementText(editViewPage.currentViewResult , "Job_2",test);
				
				combineJobPage CJ= new combineJobPage();
				CJ.clickcombineJob();
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Combine Jobs",test);
				CJ.combinejob();
				CJ.jobDetail();
				waitForElementToDisplay(editViewPage.result_page);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Market Data Results: Library",test);
				waitForElementToDisplay(combineJobPage.combine1_ResultPage);
				verifyElementTextContains(combineJobPage.combine1_ResultPage, "combine1",test);
				
				
				manageMarketDataPage MD= new manageMarketDataPage();
				MD.combineMVPage();
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Manage Market Views",test);
				MD.combineMv();
				waitForElementToDisplay(editViewPage.result_page);
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Market Data Results: Library",test);
				scrollToElement(driver,manageMarketDataPage.combineMarketView1);
				waitForElementToDisplay(manageMarketDataPage.combineMarketView1);
				verifyElementText(manageMarketDataPage.combineMarketView1 , "Combined Market View 1",test);
				
				refineMarketPage RM= new refineMarketPage();
				waitForElementToDisplay(tt.MMD_title);
				verifyElementText(tt.MMD_title , "Mercer Market Data Results: Library",test);
				RM.ClickonRefinement(RM.MJlrefineMarket_Tab);
				waitForElementToDisplay(tt.title_tab);
				verifyElementText(tt.title_tab , "Refine Market",test);
				RM.selectrefinement();
				waitForElementToDisplay(editViewPage.result_page);
				verifyElementText(tt.MMD_title , "Mercer Market Data Results: Library",test);
				waitForElementToDisplay(refineMarketPage.BankingText_Resultpage);
				verifyElementTextContains(refineMarketPage.BankingText_Resultpage, "Industry - Super Sector: Banking/Financial Services",test);
				waitForElementToDisplay(refineMarketPage.consumerText_Resultpage);
				verifyElementTextContains(refineMarketPage.consumerText_Resultpage, "Industry - Super Sector: Consumer Goods",test);
				waitForElementToDisplay(refineMarketPage.EnergyText_Resultpage);
				verifyElementTextContains(refineMarketPage.EnergyText_Resultpage, "Industry - Super Sector: Energy",test);
				
				
				navigateViaBreadCrumb navigate = new navigateViaBreadCrumb();
				navigate.navigateBack(navigate.mmd_breadcrumb);
				MJLSearchPage.navigateJobView();
				compareDataPage CD= new compareDataPage();
				CD.clickcompareMyData();
				waitForElementToDisplay(tt.MmdPopUp_Title);
				verifyElementText(tt.MmdPopUp_Title , "Compare My Data",test);
				CD.compare();
				/*
				scrollToElement(compareDataPage.amountdiff_resultPage);
				waitForElementToDisplay(compareDataPage.amountdiff_resultPage);
				verifyElementText(compareDataPage.amountdiff_resultPage, "Diff Inc Wtd 25th %ile");
				waitForElementToDisplay(compareDataPage.percentilediff_resultPage);
				verifyElementText(compareDataPage.percentilediff_resultPage, "% Diff Inc Wtd 25th %ile");*/
				
				
				NormalizeDataPage ND=new NormalizeDataPage();
				ND.ClicknormalizeData();
				waitForElementToDisplay(tt.MmdPopUp_Title);
				verifyElementText(tt.MmdPopUp_Title , "Normalize Data",test);
				ND.normalize();
				waitForElementToDisplay(NormalizeDataPage.normalizeOn_resultpage);
				verifyElementTextContains(NormalizeDataPage.normalizeOn_resultpage, "ON",test);
				
				ageDataPage AD= new ageDataPage();
				AD.clickageData();
				waitForElementToDisplay(tt.MmdPopUp_Title);
				verifyElementText(tt.MmdPopUp_Title , "Age Data",test);
				AD.ageData();
				waitForElementToDisplay(ageDataPage.ageON_resultpage);
				verifyElementTextContains(ageDataPage.ageON_resultpage, "ON",test);
				
				sendToMyLibraryPage send= new sendToMyLibraryPage();
				send.sendRecord(send.MJlsendToLibraryTab);
				waitForElementToDisplay(send.text_message);
				verifyElementText(send.text_message, "This data has been sent to the My Market Library and is now available for benchmarking.",test);
				send.closePopup(send.alert_close);
				
				Sign_out sign = new Sign_out();
				sign.signout(sign.signout_2);
				
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_1()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				

			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
				ATUReports.add("WIN_TC_1()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

			} 
			finally
			{
				reports.flush();
				driver.close();

			}
		}

		
	}

