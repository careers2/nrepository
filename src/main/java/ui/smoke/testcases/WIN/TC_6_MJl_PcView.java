package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

public class TC_6_MJl_PcView  extends InitTests{
	

	public TC_6_MJl_PcView(String appName) {
	super(appName);
	}
	
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_6() throws Exception {
	try {
		TC_6_MJl_PcView a1=new TC_6_MJl_PcView("WIN");
		test = reports.createTest("Combine Pc Functionality");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		MMDPage marketData= new MMDPage();
		marketData.jobLibrary();
		waitForElementToDisplay(MMDPage.mercerJobLibrary);
		verifyElementText(MMDPage.mercerJobLibrary , "Mercer Job Library",test);
		
		mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
		MJLSearchPage.pcView();
		waitForElementToDisplay(mercerJobLibraryPage.result_Page);
		verifyElementText(mercerJobLibraryPage.result_Page , "Mercer Market Data Results: Library",test);
		
		combinePCpage combine= new combinePCpage();
		combine.pcCombine();
		waitForElementToDisplay(combinePCpage.result_table);
		waitForElementToDisplay(mercerJobLibraryPage.result_Page);
		verifyElementText(mercerJobLibraryPage.result_Page , "Mercer Market Data Results: Library",test);
		waitForElementToDisplay(combine.combine1_result);
		verifyElementText(combine.combine1_result , "41 - 44 (combined)",test);
		waitForElementToDisplay(combine.combine2_result);
		verifyElementText(combine.combine2_result , "59 - 61 (combined)",test);
		
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_2);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_6", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_6()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
  } 
}
