package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

import pages.WINPages.*;

public class TC_5_MyDashboard extends InitTests{

	public TC_5_MyDashboard(String appName) {
	super(appName);
	}
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =true)
	public void WIN_TC_5() throws Exception {
	try {
		TC_5_MyDashboard a1=new TC_5_MyDashboard("WIN");
		test = reports.createTest("My Dashboard");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		
		WidgetsLink widget= new WidgetsLink();
		widget.accessLink(WidgetsLink.downloadMercerSurveyReport_link);
		waitForElementToDisplay(WidgetsLink.surveyReportPopup);
		verifyElementText(WidgetsLink.surveyReportPopup , "Download a Mercer survey report",test);
		widget.close();
		
		Title tt=new Title();
		widget.accessLink(WidgetsLink.editMyProfile_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab , "winqa01 test user1 - win's Profile",test);
		navigateViaBreadCrumb navigate = new navigateViaBreadCrumb();
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.evaluateAJob_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab, "My Jobs",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.importData_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab , "Import",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.manageMyOrganization_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab , "My Organizations",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.runDiagnosticReport_link);
		verifyElementText(tt.title_tab , "Diagnostic Report",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.searchmyReferenceJob_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab , "Reference Jobs",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.viewMMDJob_link);
		waitForElementToDisplay(tt.MMD_title );
		verifyElementText(tt.MMD_title , "Mercer Market Data for Year: ",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.viewMMDPC_link);
		waitForElementToDisplay(tt.MMD_title );
		verifyElementText(tt.MMD_title , "Mercer Market Data for Year: ",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		widget.accessLink(WidgetsLink.adminPage_link);
		waitForElementToDisplay(tt.title_tab );
		verifyElementText(tt.title_tab, "Mercer WIN - System Administration",test);
		navigate.navigateBack(navigate.home_Breadcrumb);
		
		MymarketView mv= new MymarketView();
		mv.SearchmarketView();
		mv.selectmarketview(mv.Nonlibrary_marketview);
		waitForElementToDisplay(mv.Nonlibpopup_element);
		waitForElementToDisplay(mv.nonlib_PopupTitle);
		verifyElementText(mv.nonlib_PopupTitle , "2016 Beta BE TRS - PR only+global",test);
		mv.close_Popup(mv.Nonlibclose_Button);
		
		mv.selectmarketview(mv.library_marketview);
		waitForElementToDisplay(mv.libpopup_element);
		waitForElementToDisplay(tt.MmdPopUp_Title);
		verifyElementText(tt.MmdPopUp_Title , "2016 BE TRS MJC1",test);
		mv.close_Popup(mv.Libclose_Button);
		
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_1);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_5()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_5()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
}

}
