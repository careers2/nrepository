package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.*;

public class TC_3_JobPrint extends InitTests{
	

	public TC_3_JobPrint(String appName) {
	super(appName);
	}
	
	
	SoftAssert softAssert = new SoftAssert();
	
	@Test(priority = 1, enabled =false)
	public void WIN_TC_3() throws Exception {
	try {
		TC_3_JobPrint a1=new TC_3_JobPrint("WIN");
		test = reports.createTest("Job Print Functionality");
		test.assignCategory("smoke");
		initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		LoginPage loginPage = new LoginPage();
		loginPage.login(USERNAME,PASSWORD);
		selectOrgPage Org= new selectOrgPage();
		Org.Organization();
		MMDPage marketData= new MMDPage();
		marketData.jobLibrary();
		waitForElementToDisplay(MMDPage.mercerJobLibrary);
		verifyElementText(MMDPage.mercerJobLibrary , "Mercer Job Library",test);
		
		mercerJobLibraryPage MJLSearchPage = new mercerJobLibraryPage();
		MJLSearchPage.jobView();
		waitForElementToDisplay(mercerJobLibraryPage.currentViewResult);
		verifyElementText(mercerJobLibraryPage.currentViewResult , "Job",test);
		
		MJL_PrintFunctionality Print = new MJL_PrintFunctionality();
		MJl_Export Export = new MJl_Export();
		Print.chartPrint();
		Export.MJLReportFileOptions("chart_job");
		Export.sendToProgressCenter(Export.Jobclose_Notification);
		ProgressCenter prog= new ProgressCenter();
		prog.waitReportGeneration();
		prog.progress(prog.JobprogressCenter_button);
		waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
		verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
		waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
		verifyElementText(ProgressCenter.Jobreport_name, "chart_job",test);
		prog.progress(prog.JobprogressCenter_button);
		
		Print.comparisonPrint();
		Export.MJLReportFileOptions("comaprison_Report");
		Export.sendToProgressCenter(Export.Jobclose_Notification);
		prog.waitReportGeneration();
		prog.progress(prog.JobprogressCenter_button);
		waitForElementToDisplay(ProgressCenter.MJlreport_Table);
		waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
		verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
		verifyElementText(ProgressCenter.Jobreport_name, "comaprison_Report",test);
		prog.progress(prog.JobprogressCenter_button);
		
		Export.sendToProgressCenter(Export.export_Button);
		Export.MJLReportFileOptions("Mjl_Export");
		Export.sendToProgressCenter(Export.Jobclose_Notification);
		prog.waitReportGeneration();
		prog.progress(prog.JobprogressCenter_button);
		waitForElementToDisplay(ProgressCenter.MJlreport_Table);
		waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
		verifyElementText(ProgressCenter.Jobcomplete_status, "Complete",test);
		verifyElementText(ProgressCenter.Jobreport_name, "Mjl_Export",test);
		waitForElementToDisplay(ProgressCenter.Jobcomplete_status);
		prog.progress(prog.JobprogressCenter_button);
		
		Sign_out sign = new Sign_out();
		sign.signout(sign.signout_2);
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_3", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("WIN_TC_3()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		reports.flush();
		driver.close();

	}
}
}
