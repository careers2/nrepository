package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import pages.WINPages.*;
import utilities.InitTests;

public class Edit_User_Profile extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Edit_User_Profile(String appName) 
    {
	super(appName);
   }
	
	@Test()
    public void testSearch() throws Exception {
	  try
	  {
		  test = reports.createTest("Edit_User_Profile");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  SelectOrg so = new SelectOrg();
		  assertTrue(SelectOrg.orgname.isDisplayed(),"WIN QA Organization [winqa123]')]",test);
		  so.Organization();
		  HomePage hp = new HomePage();
		  assertTrue(HomePage.homepage.isDisplayed(),"My Homepage",test);
		  hp.HomePage();
		  
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  
		  Tools tl = new Tools();
		  tl.My_Profile();
		  waitForElementToDisplay(Tools.User_Profile_Page);
		  verifyElementText(Tools.User_Profile_Page , "winqa01 test user1 - WIN QA Organization's Profile",test);
		  
		  User_Profile up = new User_Profile();
		  up.Edit_User();
		  
		  Thread.sleep(5000);
		 
		  up.Change_Language_Portu_Brazil();
		  Thread.sleep(5000);
		  verifyElementText(User_Profile.Portuguese_Brazil_Button , "Salvar",test);
		  
		  up.Change_Language_English();
		  Thread.sleep(5000);
		  verifyElementText(User_Profile.Save_Button , "Save",test);
		  
		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("Edit_User_Profile()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("Edit_User_Profile()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		//driver.close();

	}
  }

	private void verifyElementTextContains(WebElement administration_Page, String string) {
		// TODO Auto-generated method stub
		
	}
}





