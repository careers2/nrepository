package ui.smoke.testcases.WIN;

import static driverfactory.Driver.*;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;

public class Access_Manage_Evaluation_Approval extends InitTests {
	SoftAssert softAssert = new SoftAssert();

	public Access_Manage_Evaluation_Approval(String appName) {
		super(appName);
	}

	@Test
	public void testSearch() throws Exception {
		try {
			Access_Manage_Evaluation_Approval a1 = new Access_Manage_Evaluation_Approval("WIN");
			test = reports.createTest("smoketest");
			test.assignCategory("smoke");
			initWebDriver(BASEURL, "CHROME", "", "", "local", test, "");
			Login loginPage = new Login();
			loginPage.login(USERNAME, PASSWORD);
			SelectOrg so = new SelectOrg();
			assertTrue(SelectOrg.orgname.isDisplayed(), "WIN QA Organization [winqa123]')]", test);
			so.Organization();

			HomePage hp = new HomePage();
			assertTrue(HomePage.homepage.isDisplayed(), "My Homepage", test);
			hp.HomePage();

			Tools t = new Tools();
			t.Administration();

			Access_Manage_Evaluation a = new Access_Manage_Evaluation();
			a.Manage_Evaluation_Approvals();

			Signout s = new Signout();
			s.Signout();

		} catch (Error e) {
			e.printStackTrace();
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}
	}

	@AfterSuite
	public void tearDown() {
		reports.flush();
		driver.close();
	}
}