package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyElementText;
import static verify.SoftAssertions.verifyEquals;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class MyJobs extends InitTests {
	SoftAssert softAssert = new SoftAssert();
	
	public MyJobs(String appName) {
		super(appName);
	}
	@Test()
    public void testSearch() throws Exception {
	  System.out.println("in test");
	  try
	  {
		  MyJobs a1=new MyJobs("WIN");
		  test = reports.createTest("testSearch");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  
		  
		  Select_Organization selectorg=new Select_Organization();
		  assertTrue(Select_Organization.signOut_link.isDisplayed(),"WIN Application has reached Select Organization page",test);
		  selectorg.select_organization();
		  assertTrue(Select_Organization.Homepage.isDisplayed(),"My Homepage",test);
		  
		  
		  My_jobs_search_page searchpage=new My_jobs_search_page();
		  searchpage.jobssearchpage();
		  assertTrue(My_jobs_search_page.Myjobssearchpage.isDisplayed(),"My Jobs Search page",test);
		  
		  My_jobs myjob = new My_jobs();
		  myjob.job();
		  assertTrue(My_jobs.jobdetailsclose.isDisplayed(),"Accessed Job Details pop up",test);
		  myjob.jobclose();
		 
		  My_Jobs_Create_from_Reference_jobs createreferencejob=new My_Jobs_Create_from_Reference_jobs();
		  createreferencejob.create_from_referenece_jobs();
		  ProgressCenter prog= new ProgressCenter();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name,"Create Jobs Results",test);
		  prog.progress(prog.GlobalProgress_close);
		  
		  My_Jobs_Edit_View myjobedit=new My_Jobs_Edit_View();
		  myjobedit.editviewbutton();
		  assertTrue(myjobedit.editviewpage.isDisplayed(),"Edit view page",test);
		  myjobedit.editviewselections();
		  myjobedit.edit_view_save();
		  
		  My_Jobs_Add_New_Job addjob= new My_Jobs_Add_New_Job();	  
		  addjob.add_job();
		  verifyElementText(My_Jobs_Add_New_Job.jobnameverify,addjob.Jobtitle,test);
		  addjob.add_job_close();
		  
		  MyJobs_Resultspage resultspage=new MyJobs_Resultspage();
		  resultspage.myjobs_results_page();
		  assertTrue(My_jobs.jobdetailsclose.isDisplayed(),"Accessed Job Details pop up",test);
		  resultspage.jobdetailsclose();
		  resultspage.benchmarkicon();
		  assertTrue(MyJobs_Resultspage.benchmarkclose.isDisplayed(),"Accessed Job Details pop up",test);
		  resultspage.benchmarkclose();
		  resultspage.save();
		  verifyEquals(MyJobs_Resultspage.resultsetname,getTextFromWebElementUsingJavaScript(MyJobs_Resultspage.pagetitle),test);
		  resultspage.resultset();
		  verifyEquals(MyJobs_Resultspage.resultsetname,getTextFromWebElementUsingJavaScript(MyJobs_Resultspage.pagetitle),test);
		  
		  My_Jobs_Row_Edit edit=new My_Jobs_Row_Edit();
		  edit.rowedit();
		  assertTrue(My_Jobs_Row_Edit.confirmation_message.isDisplayed(),"Job Evaluated",test);
		  edit.results_page();
		  
		  Results_page_Print print=new Results_page_Print();
		  print.print_results();
		  prog.waitReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Print.Reportname,test);
		  prog.progress(prog.GlobalProgress_close);
			
		  Results_page_Export export=new Results_page_Export();
		  export.export_results();
		  prog.waitReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.progress(prog.GlobalProgress_close);
		  export.export_results_xls();
		  prog.waitReportGeneration();
		  prog.progress(prog.Progress_Center);
		  verifyElementText(ProgressCenter.complete_status, "Complete",test);
		  verifyElementText(ProgressCenter.report_name, Results_page_Export.Exportname,test);
		  prog.progress(prog.GlobalProgress_close);
		
		  Sign_out sign_out=new Sign_out();
		  sign_out.signout();
	  

	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		driver.close();

	}
  }


}
