package ui.smoke.testcases.WIN;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.initWebDriver;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.assertTrue;
import static verify.SoftAssertions.verifyEquals;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.WINPages.*;
import utilities.InitTests;

public class Help_Link extends InitTests{
	SoftAssert softAssert = new SoftAssert();
	public Help_Link(String appName) 
    {
	super(appName);
   }
	
	@Test()
    public void testSearch() throws Exception {
	  try
	  {
		  test = reports.createTest("testSearch");
		  test.assignCategory("smoke");
		  initWebDriver(BASEURL,BROWSER_TYPE, "", "", "local", test, "");
		  Login loginPage = new Login();
		  loginPage.login(USERNAME,PASSWORD);
		  SelectOrg so = new SelectOrg();
		  assertTrue(SelectOrg.orgname.isDisplayed(),"WIN QA Organization [winqa123]')]",test);
		  so.Organization();
		  HomePage hp = new HomePage();
		  assertTrue(HomePage.homepage.isDisplayed(),"My Homepage",test);
		  hp.HomePage();
		  
		  Help t1 = new Help();
		  t1.Help_Link();
		  Actions action= new Actions(driver);
		  action.keyDown(Keys.CONTROL).sendKeys(Keys.TAB).build().perform();
		  verifyEquals(driver.getTitle(), "Mercer WIN Online Help Center",test);
		  for(int i = driver.getWindowHandles().size() -1 ; i > 0 ; i--){

		        String winHandle = driver.getWindowHandles().toArray()[i].toString();

		        driver.switchTo().window(winHandle);  

		        driver.close();
		  } 
		  Signout s = new Signout();
		  s.Signout();
		  	  
	} catch (Error e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} catch (Exception e) {
		e.printStackTrace();
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

	} 
	finally
	{
		//driver.close();

	}
  }
}