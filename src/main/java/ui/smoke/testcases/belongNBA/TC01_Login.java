package ui.smoke.testcases.belongNBA;


import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getScreenPath;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import utilities.InitTests;
import verify.SoftAssertions;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;


import java.io.IOException;

import pages.BelongNBA_Pages.Homepage;
import pages.BelongNBA_Pages.Loginpage;


public class TC01_Login extends InitTests {
	public TC01_Login(String appName) {
		super(appName);
		// TODO Auto-generated constructor stub
	}
	@Test(priority = 1, enabled = true)
	public void login() throws IOException {
		try {
			test = reports.createTest("login");
			test.assignCategory("smoke");
			
			TC01_Login login = new TC01_Login("BelongNBA");

			System.out.println("Logging in to application");
			initWebDriver(BASEURL, "CHROME", "","","local", test, "");
			System.out.println("BaseURL is: " + BASEURL);
			
			
			Loginpage loginPage = new Loginpage();
			loginPage.login(USERNAME, PASSWORD);
			
			
			Homepage homePage = new Homepage();
			waitForElementToDisplay(homePage.NBAHeader);
			SoftAssertions.verifyElementTextContains(homePage.NBAHeader, "Welcome to Your NBA Benefits",test);
			
		}catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();

			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		}

	}

	@AfterSuite
	public void killDriver() {

		killBrowserExe(BROWSER_TYPE);

	}
	}
