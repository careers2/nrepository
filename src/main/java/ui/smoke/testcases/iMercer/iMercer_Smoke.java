package ui.smoke.testcases.iMercer;

import static driverfactory.Driver.*;
import static driverfactory.Driver.getScreenPath;
import static driverfactory.Driver.initWebDriver;
import static driverfactory.Driver.killBrowserExe;
import static utilities.MyExtentReports.reports;
import static utilities.MyExtentReports.test;
import static verify.SoftAssertions.*;
import static verify.SoftAssertions.verifyElementTextContains;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import pages.iMercer_pages.DNNsitePurchase;
import pages.iMercer_pages.DNNsiteSurveys;
import pages.iMercer_pages.LegacyPurchase;
import pages.iMercer_pages.LegacySurvey;
import pages.iMercer_pages.Login;
import pages.iMercer_pages.UserEmail;
import utilities.InitTests;
import verify.SoftAssertions;

public class iMercer_Smoke extends InitTests {
	public static String MFAChoice = "";
	public static String MFAEmailId = "";
	public static String MFAEmailPassword = "";

	public iMercer_Smoke(String appName) {
		super(appName);
	}
	
	@BeforeMethod
	public void setUp() throws Exception {
		props.load(input);
		MFAChoice = props.getProperty("iMercer_MFAChoice");
		MFAEmailId = props.getProperty("MFAEmailId");
		MFAEmailPassword = props.getProperty("MFAEmailPassword");
		test = reports.createTest("Logging in to iMercer");
		test.assignCategory("smoke");
		@SuppressWarnings("unused")
		iMercer_Smoke iMercerobj = new iMercer_Smoke("iMercer");
		initWebDriver(BASEURL, "CHROME", "latest","","local", test, "");
	}
	@Test(priority = 1, enabled = true)
	public void smokeTestCases() throws Exception {
		try {
			Login loginobj = new Login();
			LegacySurvey surveyobj = new LegacySurvey();
			LegacyPurchase purchaseobj = new LegacyPurchase();
			DNNsiteSurveys DNNsurveyobj = new DNNsiteSurveys();
			DNNsitePurchase DNNpurchaseobj = new DNNsitePurchase();
			UserEmail useremailobj = new UserEmail();
			
				assertTrue(isElementExisting(driver,loginobj.WelcomeToiMercer,20),"Imercer site has loaded successfully",test);
			loginobj.regionSelection("Latin America");
				verifyElementTextContains(loginobj.RegionBanner,"Latin America",test);
			loginobj.login(USERNAME, PASSWORD);
				assertTrue(isElementExisting(driver,loginobj.LoggedInUserName,20),"User login Successful",test);
			
			surveyobj.selectSurvey("Club Oil Gas Colombia English Test");
				verifyElementTextContains(surveyobj.SurveyPageHeader,"Club Oil Gas Colombia English Test",test);
			
			purchaseobj.addToCart();
				verifyElementTextContains(purchaseobj.BillingInfoHeader,"Shipping & Billing Information",test);
			
			purchaseobj.checkout();	
			assertTrue(isElementExisting(driver,purchaseobj.OrderReceipt,20),"Order Details page loaded successfully",test);
			assertTrue(isElementExisting(driver,purchaseobj.OrderNumber,20),"Order Number is generated and displayed",test);
			assertTrue(isElementExisting(driver,purchaseobj.OrderSummary,20),"Order information is displayed",test);
				
			useremailobj.openOutlookEmail();
			assertTrue(isElementExisting(driver,useremailobj.MessageSenderLegacy,20),"Order invoice email has been verified",test);
			useremailobj.switchToMainWindow();
			
			DNNsurveyobj.changeLocation("United States");
				verifyElementTextContains(DNNsurveyobj.LocationLabel,"United States",test);
			DNNsurveyobj.openSurveysAndReportsPage();
			assertTrue(isElementExisting(driver,DNNsurveyobj.SurveysAndReportsPageHeader,20),"Surveys And Reports page has been loaded",test);
			DNNsurveyobj.selectSurvey("COMPENSATION","US","Department/Function","Engineering","Pre_Testing_HC");
				verifyElementTextContains(DNNsurveyobj.SurveyTitle,"Pre_Testing_HC",test);
			
			DNNpurchaseobj.addToCart();
			assertTrue(isElementExisting(driver,DNNpurchaseobj.CartPageHeader,20),"Shopping Cart page has loaded successfully",test);
			DNNpurchaseobj.checkout();
			assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderReceipt,20),"Order Receipt page has loaded",test);
			assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderNumber,20),"Order Number is generated and displayed",test);
			assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderSummary,20),"Order Information is displayed ",test);
				
			useremailobj.openOutlookEmail();
			assertTrue(isElementExisting(driver,useremailobj.MessageSenderDNN,20),"Order invoice email has been verified",test);
			useremailobj.switchToMainWindow();
			
			DNNsurveyobj.changeLocation("Asia");
			verifyElementTextContains(DNNsurveyobj.LocationLabel,"Asia",test);
		DNNsurveyobj.openSurveysAndReportsPage();
		assertTrue(isElementExisting(driver,DNNsurveyobj.SurveysAndReportsPageHeader,20),"Surveys And Reports page has been loaded",test);
		DNNsurveyobj.selectSurvey("Benefits","Benefits Survey Asia Pacific");
			verifyElementTextContains(DNNsurveyobj.SurveyTitle,"Benefits Survey Asia Pacific",test);
		
		DNNpurchaseobj.addToCart();
		assertTrue(isElementExisting(driver,DNNpurchaseobj.CartPageHeader,20),"Shopping Cart page has loaded successfully",test);
		DNNpurchaseobj.checkout();
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderReceipt,20),"Order Receipt page has loaded",test);
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderNumber,20),"Order Number is generated and displayed",test);
		assertTrue(isElementExisting(driver,DNNpurchaseobj.OrderSummary,20),"Order Information is displayed ",test);
			
		useremailobj.openOutlookEmail();
		assertTrue(isElementExisting(driver,useremailobj.MessageSenderDNN,20),"Order invoice email has been verified",test);
		useremailobj.switchToMainWindow();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()),test);
			ATUReports.add("GHRM()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} finally {
			reports.flush();
			driver.quit();
		}

	}
	@AfterSuite
	public void kill() {
		killBrowserExe(BROWSER_TYPE);
	}
}
