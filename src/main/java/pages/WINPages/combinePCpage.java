package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class combinePCpage {

	@FindBy(xpath="//button[@data-stat-type='Actual']")
	public static WebElement Actual_Tab;
	
	@FindBy(xpath="//button[@data-purpose='combine-pcs']")
	public static WebElement combinePC_Tab;
	
	@FindBy(xpath="//div[@data-pc='41']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_41;
	
	@FindBy(xpath="//div[@data-pc='44']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_44;
	
	@FindBy(xpath="//div[@data-pc='59']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_59;
	
	@FindBy(xpath="//div[@data-pc='61']//input[@class='combine-pc-toggle']")
	public static WebElement selectPC_61;
	
	@FindBy(xpath="//button[@data-purpose='combine']")
	public static WebElement combine_button;
	
	@FindBy(xpath="//button[@data-purpose='done-page']")
	public static WebElement save_Button;
	
	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//span[@title='41 - 44 (combined)']")
	public  WebElement combine1_result;

	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//span[@title='59 - 61 (combined)']")
	public  WebElement combine2_result;
	
	@FindBy(xpath="//div[@class='split-grid']//div[@class='summary data-grid display-below portal']")
	public static WebElement result_table;
	
	public combinePCpage() {
		PageFactory.initElements(driver, this);
	}
	
	public void pcCombine() {
		waitForPageLoad();
		waitForElementToDisplay(result_table);
		waitForElementToClickable(Actual_Tab);
		clickElementUsingJavaScript(driver,Actual_Tab);
		waitForElementToClickable(combinePC_Tab);
		clickElementUsingJavaScript(driver,combinePC_Tab);
		clickElement(selectPC_41);
		clickElement(selectPC_44);
		clickElement(combine_button);
		waitForElementToClickable(selectPC_59);
		clickElement(selectPC_59);
		clickElement(selectPC_61);
		clickElement(combine_button);
		clickElement(save_Button);
		
	}
}
