package pages.WINPages;

import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdministrationPage {
	@FindBy(xpath="//button[@class='medium action reset-mydata-button']")
	public WebElement resetData_Button;
	
	@FindBy(xpath="//button[@class='medium action mjl-button']")
	public WebElement mercerJobLibrary_button;
	
	@FindBy(xpath="//button[@class='medium action add-mercer-button']")
	public WebElement addMercerData;
	
	@FindBy(xpath="//div[@class='popup has-title size-to-fit']//div[@class='title']")
	public WebElement addMercerData_popup;
	
	@FindBy(xpath="//div[@class='popup has-title size-to-fit']//button[@class='medium ok']")
	public WebElement ok_button;
	
	@FindBy(xpath="//button[@class='medium action job-arch-button']")
	public WebElement manageJobAchitecture_Tab;
	
	public AdministrationPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void accessAdminTab(WebElement ele) {
		waitForElementToClickable(ele);
		clickElementUsingJavaScript(driver,ele);
	}
}
