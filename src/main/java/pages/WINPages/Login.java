package pages.WINPages;
import utilities.InitTests;
import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
public class Login {
	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement email;
	
	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement password;

	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signin;	
	

	public Login() {
		PageFactory.initElements(driver, this);
	}
	public void login(String Username,String Password)
	{
		setInput(email, Username);
		setInput(password, Password);
		clickElement(signin);
		
	}
}
