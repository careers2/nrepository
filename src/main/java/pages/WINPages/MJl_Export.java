package pages.WINPages;

import static driverfactory.Driver.*;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MJl_Export {
	
	@FindBy(xpath="//button[@data-purpose='report-export']")
	public WebElement export_Button;
	
	@FindBy(xpath="//input[@data-purpose='file-name']")
	public static WebElement fileName;
	
	@FindBy(id="xlsx")
	public static WebElement fileoptions;
	
	@FindBy(xpath="//div[@data-id='print-file-options-id']")
	public static WebElement file;
	
	@FindBy(xpath="//button[@data-purpose='file-option-done']")
	public static WebElement done_Button;
	
	@FindBy(xpath="//div[@class='pad']")
	public static WebElement Jobprogress_Notification;
	
	@FindBy(xpath="//div[@class='modal-body-content']")
	public WebElement Mjl_notification;
	
	@FindBy(xpath="//button[@class='apply-button' and @data-id='global-alert']")
	public WebElement MJlclose_Notification;
	
	@FindBy(xpath="//button[ @data-id='progress-center-notification-id' and @class='apply-button action-button medium ok']")
	public WebElement Jobclose_Notification;
	
	public MJl_Export() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public  void MJLReportFileOptions(String filename) {
		setInput(fileName , filename);
		clickElement(fileoptions);
		clickElement(done_Button);
	}
	
	public  void sendToProgressCenter(WebElement ele) {
		waitForElementToClickable(ele);
		clickElement(ele);
	}
	
	
	public void generateReport(String name) {
		setInput(fileName, name);
		clickElement(file);
		clickElement(done_Button);	
	}
}
	

