package pages.WINPages;

import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Export_ManageJobArchitecture {
	@FindBy(xpath="//button[@data-action='goToExport']")
	public static WebElement export_Tab;
	
	@FindBy(xpath="//input[@value='xlsx']")
	public static WebElement xlsx_selection;
	
	@FindBy(xpath="//input[@data-label='MUPCS Catalog']")
	public static WebElement mupcs_catalog;
	
	@FindBy(xpath="//input[@data-label='Career Stream']")
	public static WebElement career_stream;
	
	@FindBy(xpath="//div[@class='editor-row section-header-row']//h3[text()= 'File Type']")
	public  WebElement file_Type;
	
	@FindBy(xpath="//div[@class='editor-row section-header-row']//h3[text()= 'Architecture Selection']")
	public  WebElement architecture_selection;
	
	@FindBy(xpath="//div[@class='editor-row section-header-row']//h3[text()= 'Architecture Selection']")
	public WebElement Hierachy_selection;
	
	@FindBy(xpath="//input[@data-purpose='filename']")
	public WebElement fileName;
	
	@FindBy(xpath="//button[@data-action='export']")
	public WebElement export_button;
	
	public Export_ManageJobArchitecture() {
		PageFactory.initElements(driver, this);
	}
	
	public void export() {
		waitForElementToClickable(export_Tab);
		clickElement(export_Tab);
		waitForElementToClickable(mupcs_catalog);
		clickElement(mupcs_catalog);
		clickElement(career_stream);
	}
	
	
	
}

