package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Jobs_Row_Edit {
	
	 	@FindBy(xpath="//button[@class='icon row-edit-icon row-edit-button']")
	    public static WebElement row_edit_button;
	 	
	 	@FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='country']//child::select")
	    public static WebElement Jobcountry;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='country']//child::select//child::option[3]")
	    public static WebElement JobCountryselect;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='country']//child::select//child::option[2]")
	    public static WebElement JobCountryreselect;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='organization']//child::select")
	    public static WebElement Organization;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='organization']//child::select//child::option")
	    public static WebElement Organizationselect;
	    
	    @FindBy(xpath = "//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Impact']//child::select")
	    public static WebElement Impact;
	    
	    @FindBy(xpath = "//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Impact']//child::select//child::option[3]")
	    public static WebElement Impactselect;
	    
	    @FindBy(xpath = "//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Impact']//child::select//child::option[2]")
	    public static WebElement Impactreselect;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Communication']//child::select")
	    public static WebElement Communication;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Communication']//child::select//child::option[4]")
	    public static WebElement Communicationselect;
	    
	    @FindBy(xpath="//table[@id='scroll-pane-body']//tr[@data-row-group-id='1']//td[@data-property='Communication']//child::select//child::option[2]")
	    public static WebElement Communicationreselect;
	    
	    @FindBy(xpath = "//button[@class='btn btn-large btn-ok']")
	    public static WebElement Save;
	    
	    @FindBy(xpath="//div[@class='popup smart-help has-title size-to-fit']//button[@class='action medium ok']")
		public static WebElement Ok;
	    
	    @FindBy(xpath="//span[@data-id='confirmation-summary']")
		public static WebElement confirmation_message;
	    
	    @FindBy(xpath="//a[@href='/v2/Benchmarking/JobsResultsPage']")
		public static WebElement Resultspage;
	    
	    Thinking thinking=new Thinking();
	    
	    Random rg = new Random();
	    
	    int randomInt = rg.nextInt(5);	
		String numberAsString = Integer.toString(randomInt);
		public String Resultset="Result set"+numberAsString;
	    
	    String random() {
		int randomInt = rg.nextInt(5);	
		String numberAsString = Integer.toString(randomInt);
		return numberAsString;
	    }
	    
	    My_jobs job=new My_jobs();
		
		public String resultsetname="1";
		
		public static String s1="";
		
	    public My_Jobs_Row_Edit(){
			PageFactory.initElements(driver, this);
		}
	    public void rowedit()
		{
	    	waitForElementToEnable(row_edit_button);	
			clickElement(row_edit_button);
			waitForPageLoad();
			//delay(30000);
			waitForElementToEnable(Jobcountry);	
			clickElement(Jobcountry);
			clickElement(JobCountryselect);
			clickElement(Jobcountry);
			clickElement(JobCountryreselect);
			clickElement(Impact);
			clickElement(Impactselect);
			clickElement(Impact);
			clickElement(Impactreselect);
			clickElement(Communication);
			clickElement(Communicationselect);
			clickElement(Communication);
			clickElement(Communicationreselect);
			clickElement(Save);
			clickElement(Ok);
			//delay(6000);
			waitForElementToDisplay(confirmation_message);		
		}
	    public void results_page() {
			clickElement(Resultspage);
			waitForPageLoad();
			waitForElementToEnable(job.jobdetails);
			//delay(4000);
	    }


}
