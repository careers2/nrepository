package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Market_Library_search_page {
	
	@FindBy(xpath="//div[@id='feature-menu-button4']")
    public static WebElement mylibrary;
	
	@FindBy(xpath="//a[text()='My Market Library']")
	public static  WebElement mymarketlibrary;
	
	@FindBy(xpath="//div[@id='page-title']//h1")
	public static WebElement MyLibrary;
	
	public My_Market_Library_search_page(){
		PageFactory.initElements(driver, this);
	}
	public void marketlibrarysearchpage()
	{
		waitForPageLoad();
		waitForElementToEnable(mylibrary);
		clickElement(mylibrary);
		//delay(2000);
		waitForElementToDisplay(mymarketlibrary);
		clickElement(mymarketlibrary);
		waitForPageLoad();

	}
		
}
