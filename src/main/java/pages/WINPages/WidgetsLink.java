package pages.WINPages;

import static driverfactory.Driver.*;
import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WidgetsLink {

	//Widgets

		@FindBy(xpath="//a[@data-id='marketdata_download_a_survey']")
		public static WebElement downloadMercerSurveyReport_link;
		
		@FindBy(xpath="//div[@class='popup download-survey-report-popup has-title size-to-page size-specific']//span[@title='Download a Mercer survey report']")
		public static WebElement surveyReportPopup;

		@FindBy(xpath="//div[@class='popup download-survey-report-popup has-title size-to-page size-specific']//button[@class='medium ok']")
		public static WebElement close_Button;
		
		@FindBy(xpath="//a[@data-id='host_edit_my_profile']")
		public static  WebElement editMyProfile_link;
		
		@FindBy(xpath="//a[@data-id='benchmarking_evaluate_job']")
		public static  WebElement evaluateAJob_link;
		
		@FindBy(xpath="//a[@data-id='admin_import_data_into_win']")
		public static  WebElement importData_link;
	
		@FindBy(xpath="//a[@data-id='benchmarking_manage_organizations']")
		public static  WebElement manageMyOrganization_link;
		
		@FindBy(xpath="//a[@data-id='marketdata_run_diagnostic_report']")
		public static  WebElement runDiagnosticReport_link;
	
		@FindBy(xpath="//a[@data-id='benchmarking_search_reference_jobs']")
		public static   WebElement searchmyReferenceJob_link;
		
		@FindBy(xpath="//a[@data-id='marketdata_search_by_job']")
		public  static   WebElement viewMMDJob_link;
	
		@FindBy(xpath="//a[@data-id='marketdata_search_by_position_class']")
		public static  WebElement viewMMDPC_link;
		 
		@FindBy(xpath="//a[@data-id='admin_view_advanced_system_setup']")
		public static  WebElement adminPage_link;
		
		

		public WidgetsLink() {
			PageFactory.initElements(driver, this);
		}
		
		
		public void accessLink(WebElement ele)
		{
			waitForPageLoad();
			waitForElementToClickable(ele);
			clickElementUsingJavaScript(driver,ele);
			waitForPageLoad();
			
		}
		
		public void close() {
			clickElement(close_Button);
		}	
}
