package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class MMDPage {
	
	@FindBy(xpath="//div[@id='feature-menu-link0']//a")
	public static WebElement mercerMarketData_Tab;
	
	@FindBy(xpath="//span[@class='label']")
	public static WebElement mercerJobLibrary;
	
	@FindBy(xpath="//a[contains(text(),'Global')]")
	public static WebElement Global_Tab;
	
	@FindBy(xpath="//h1[contains(text(), 'Mercer Market Data for Year: ')]")
	public static WebElement global_MercerMarketData;

	public MMDPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void jobLibrary() {
			waitForPageLoad();
			waitForElementToClickable(mercerMarketData_Tab);
			clickElementUsingJavaScript(driver,mercerMarketData_Tab);
			System.out.println("MMd selected");
	}
	
	public void global() {
		jobLibrary();
		waitForElementToClickable(Global_Tab);
		clickElementUsingJavaScript(driver,Global_Tab);
		System.out.println("Global Tab selected");
	}

}
