package pages.WINPages;

import static driverfactory.Driver.*;
import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MymarketView {
	
	
	@FindBy(xpath="//div[@id='marketdata_my_market_views_widget']//h3")
	public static WebElement MyMarketView_Title;
	
	@FindBy(xpath="//div[@class='widget-year-selector group edit-box select-box']//a")
	public static WebElement year_Dropdown;

	@FindBy(xpath="//div[@class=' listbox-container']//a[text()='2016']")
	public static WebElement select_year2016;
	
	@FindBy(xpath="//div[@id='grid1']//a[@title='2016 BE TRS MJC1']")
	public  WebElement library_marketview;
	
	@FindBy(xpath="//div[@id='grid1']//a[@title='2016 Beta BE TRS - PR only+global']")
	public  WebElement Nonlibrary_marketview;
	
	@FindBy(xpath="//div[@class='modal-footer']//button")
	public   WebElement Libclose_Button;
	
	@FindBy(xpath="//button[@class='apply-button medium ok']")
	public  WebElement Nonlibclose_Button;
	
	@FindBy(xpath="//div[@class='marketviewdetail']")
	public WebElement Nonlibpopup_element;
	
	@FindBy(xpath="//div[@class='entity-summary-pane']")
	public WebElement libpopup_element;
	
	@FindBy(xpath="//div[text()='2016 Beta BE TRS - PR only+global']")
	public WebElement nonlib_PopupTitle;
	
	public MymarketView() {
		PageFactory.initElements(driver, this);
	}
	
	public void SearchmarketView() {
		waitForPageLoad();
		waitForElementToDisplay(MyMarketView_Title);
		waitForElementToClickable(year_Dropdown);
		clickElement(year_Dropdown);
		clickElement(select_year2016);
		
	}
	
	public void selectmarketview(WebElement ele) {
		waitForElementToDisplay(ele);
		waitForElementToClickable(ele);
		clickElement(ele);
	}
	
	
	public void close_Popup(WebElement ele)
	{
		waitForElementToClickable(ele);
		clickElement(ele);
	}
}
