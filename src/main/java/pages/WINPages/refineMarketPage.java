package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class refineMarketPage {
	@FindBy(xpath="//button[@class='report-control-button refine-market-task']")
	public WebElement MJlrefineMarket_Tab;
	
	@FindBy(xpath="//a[@title='Industry - Super Sector']")
	public static WebElement select_IndustrySuperSector;
	
	@FindBy(xpath="//label[@title='Banking/Financial Services']")
	public static WebElement addBankingFinancial;
	
	@FindBy(xpath="//label[@title='Consumer Goods']")
	public static WebElement addConsumerGoods;
	
	@FindBy(xpath="//label[@title='Energy']")
	public static WebElement addEnergy;
	
	@FindBy(xpath="//button[@class='large ok next done-button']")
	public static WebElement done_Button;
	
	@FindBy(xpath="(//div[@class='body']//span[text()='Industry - Super Sector: Banking/Financial Services'])[1]")
	public static WebElement BankingText_Resultpage;
	
	@FindBy(xpath="(//div[@class='body']//span[text()='Industry - Super Sector: Consumer Goods'])[1]")
	public static WebElement consumerText_Resultpage;
	
	@FindBy(xpath="(//div[@class='body']//span[text()='Industry - Super Sector: Energy'])[1]")
	public static WebElement EnergyText_Resultpage;
	
	@FindBy(xpath="//button[@title='Refine Market']")
	public WebElement GloablrefineMarket_Tab;
	
	@FindBy(xpath="//a[@title='Peer Group']")
	public static WebElement select_peerGroup;
	
	@FindBy(xpath="//div[@class='list-item data-node peer-group-item']//input")
	public static WebElement addPeerGroup;

	@FindBy(xpath="(//div[@id='grid1']//td[@class='branch'])[1]//span")
	public WebElement peer_Resultpage;
	
	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//div[@class='table-portal' and @style]")
	public WebElement globalresult_page;
	
	public refineMarketPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void ClickonRefinement(WebElement ele) {
		waitForElementToClickable(ele);
		clickElementUsingJavaScript(driver,ele);
		System.out.println("refinement selected");
		waitForPageLoad();
	}
	
	public void selectrefinement() {
		waitForElementToClickable(select_IndustrySuperSector);
		clickElement(select_IndustrySuperSector);
		clickElement(addBankingFinancial);
		clickElement(addConsumerGoods);
		clickElement(addEnergy);
		clickElement(done_Button);
	}
	
	public void peerGroupRefinement() {
		waitForElementToClickable(select_peerGroup);
		clickElement(select_peerGroup);
		clickElement(addPeerGroup);
		clickElement(done_Button);
	}
	
}
