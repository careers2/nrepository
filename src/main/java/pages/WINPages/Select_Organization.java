package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Select_Organization {
	
		
	@FindBy(xpath="//*[contains(text(),'WIN SM Test [SM210]')]")
	public static WebElement orgselect;
	
	@FindBy(xpath="//b[text()='Continue']")
	public static WebElement orgselected;
	
	@FindBy(xpath ="//b[contains(text(), 'Sign Out')]")
	public static WebElement signOut_link;
	
	@FindBy(xpath="//a[@href='/v2/']")
	public static WebElement Homepage;

	@FindBy(xpath="//button[@class='medium ok search icon']")
	public static WebElement Dashboard;
	
	
public Select_Organization() {
	PageFactory.initElements(driver, this);
}

  public void select_organization() {
     waitForElementToDisplay(orgselect);
     clickElement(orgselect);
     waitForElementToDisplay(orgselected);
     clickElement(orgselected);
     waitForPageLoad();
     waitForElementToEnable(Dashboard);
   } 
  
}