package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class compareDataPage {

	@FindBy(xpath="//button[@title='Compare My Data']")
	public static WebElement cmpOnMydata_Tab;
	
	@FindBy(id="cud_is_enabled")
	public static WebElement select_cmpData;
	
	@FindBy(id="cud_statistic")
	public static WebElement Statistic_dropdown;
	
	@FindBy(id="cud_display_mode")
	public static WebElement Display_dropdown;
	
	@FindBy(id="cud_amount_difference")
	public static WebElement select_amountDifference;
	
	@FindBy(id="cud_percent_difference")
	public static WebElement select_perDifference;
	
	@FindBy(id="cud_apply_market_refinements")
	public static WebElement select_applyMR;
	
	@FindBy(id="cud_set_as_default")
	public static WebElement select_applyDefault;
	
	@FindBy(xpath="//button[@data-purpose='apply']")
	public static WebElement done_Button;
	
	@FindBy(xpath="//div[@class='split-grid']//div[@class='detail data-grid display-below portal']//div[@class='head']//span[@title='Diff Inc Wtd 25th %ile']")
	public static WebElement amountdiff_resultPage;
	
	
	@FindBy(xpath="//div[@class='split-grid']//div[@class='detail data-grid display-below portal']//div[@class='head']//span[@title='% Diff Inc Wtd 25th %ile']")
	public static WebElement percentilediff_resultPage;
	
	public compareDataPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickcompareMyData(){
		waitForPageLoad();
		waitForElementToClickable(cmpOnMydata_Tab);
		clickElementUsingJavaScript(driver,cmpOnMydata_Tab);
		System.out.println("compareData");
	}
	
	public void compare() {
		waitForElementToDisplay(select_cmpData);
		if(select_cmpData.isSelected())
		{
				System.out.println("selected");
				selEleByValue(Statistic_dropdown,"5");
				selEleByValue(Display_dropdown,"Average");
				clickElement(done_Button);
		}
	
		else 
		{
				System.out.println(" Not selected");
				clickElement(select_cmpData);
				selEleByValue(Statistic_dropdown,"6");
				selEleByValue(Display_dropdown,"Individual");
				clickElement(select_amountDifference);
				clickElement(select_perDifference);
				clickElement(select_applyMR);
				clickElement(done_Button);
		}
		
	}
	
	
	
}
