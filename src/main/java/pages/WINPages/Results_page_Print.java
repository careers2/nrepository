package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static verify.SoftAssertions.verifyElementText;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.WINPages.ProgressCenter;

public class Results_page_Print {
	
	@FindBy(xpath = "//button[@title='Print']")
    public static WebElement printbutton ;
    
    @FindBy(xpath="//div[@class='popup print-options-dialog has-title size-to-page size-specific']//button[@class='action-button apply-button medium next ok']")
    public static WebElement Continue;
    
    @FindBy(xpath="//div[@class='group edit-box filename-textbox file-name form-element']//input")
    public static WebElement printinput;
    
    @FindBy(xpath="//div[@class='popup print-filename-dialog has-title size-to-fit']//button[@class='action medium ok']")
    public static WebElement printpoupclose;
    
    @FindBy(xpath="//div[@class='report-queue-progress done']//button[@class='icon image plus']")
    public static WebElement progresscenterdropdown;
    
    @FindBy(xpath="//div[@class='column-body-crosstab pos']//following::div[4]")
    public static WebElement crosstab;
    
    @FindBy(xpath="//div[@class='column-body-crosstab pos']//following::div[3]")
    public static WebElement jobevaluationsummary;
    
    @FindBy(xpath="//div[@class='column-body-crosstab pos']//following::div[5]")
    public static WebElement organizationhierarchy;
    
    @FindBy(xpath="//div[@class='column-body-crosstab pos']//following::div[6]")
    public static WebElement jobhierarchy;
    
    @FindBy(xpath="//div[text()='Select an Organization']")
    public static WebElement selectanorganization;
    
    @FindBy(xpath="//a[text()='Show All']")
    public static WebElement showall;
    
    @FindBy(xpath="//div[@class='report-queue-progress busy']")
    public static WebElement progresscenter;
    
    @FindBy(xpath="//button[@class='action medium ok']")
    public static WebElement progresscenterclose;     
	
    /*@FindBy(xpath="//div[@class='table-portal']")
    public static WebElement progresscenter;
    
    @FindBy(xpath="//div[@class='report-queue-progress busy']")
    public static WebElement progresscenter;
    
    @FindBy(xpath="//div[@class='report-queue-progress busy']")
    public static WebElement progresscenter;
    
    @FindBy(xpath="//div[@class='report-queue-progress busy']")
    public static WebElement progresscenter;*/
    
    public static String Reportname="Grid Report";
    
	 public Results_page_Print(){
			PageFactory.initElements(driver, this);
		}
	
public void print_results() {
	waitForElementToDisplay(printbutton);
	clickElement(printbutton);
	//grid
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	waitForElementToDisplay(printinput);
	setInput(printinput,Reportname);
	waitForElementToDisplay(printpoupclose);
	clickElement(printpoupclose);
	waitForElementToEnable(progresscenterclose);
	clickElement(progresscenterclose);
	//delay(4000);
	/*ProgressCenter prog= new ProgressCenter();
	prog.waitReportGeneration();
	prog.progress(prog.GlobalprogressCenter_button);
	verifyElementText(ProgressCenter.complete_status, "Complete");
	verifyElementText(ProgressCenter.report_name, Reportname);
	prog.progress(prog.GlobalProgress_close);*/
	//crosstab
	/*waitForElementToDisplay(printbutton);
	clickElement(printbutton);
	waitForElementToDisplay(crosstab);
	clickElement(crosstab);
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	waitForElementToDisplay(printpoupclose);
	clickElement(printpoupclose);*/
	//jobevaluationsummary
	/*waitForElementToDisplay(printbutton);
	clickElement(printbutton);
	waitForElementToDisplay(jobevaluationsummary);
	clickElement(jobevaluationsummary);
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	waitForElementToDisplay(printpoupclose);
	clickElement(printpoupclose);
	//organizationhierarchy
	waitForElementToDisplay(printbutton);
	clickElement(printbutton);
	waitForElementToDisplay(organizationhierarchy);
	clickElement(organizationhierarchy);
	waitForElementToDisplay(selectanorganization);
	clickElement(selectanorganization);
	waitForElementToDisplay(showall);
	clickElement(showall);
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	waitForElementToDisplay(printpoupclose);
	clickElement(printpoupclose);
	//jobhierarchy
	waitForElementToDisplay(printbutton);
	clickElement(printbutton);
	waitForElementToDisplay(jobhierarchy);
	clickElement(jobhierarchy);
	waitForElementToDisplay(selectanorganization);
	clickElement(selectanorganization);
	waitForElementToDisplay(showall);
	clickElement(showall);
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	waitForElementToDisplay(printpoupclose);
	clickElement(printpoupclose);*/
	//waitForElementToDisplay(jobcountry);
	//clickElement(jobcountry);

	//waitForElementToDisplay(joborg);
	//clickElement(joborg);
}

}
