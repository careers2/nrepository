package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class My_jobs {
	   
	    
	    @FindBy(xpath="//*[@class='body']//*[text()='Search']")
	    public static WebElement myjobssearch;
	    
	    @FindBy(xpath="//a[@data-record='0']")
	    public static WebElement jobdetails;
	    
	    @FindBy(xpath = "//div[@class='popup bench-job-details-popup full-page has-title size-to-page size-specific']//button[@class='action close-button medium ok']")
	    public static WebElement jobdetailsclose;
	   
	    @FindBy(xpath = "//b[text()='Market Comparisons']")
	    public static WebElement jobdetailsload;
	    
	    Thinking thinking=new Thinking();
	    
	    public My_jobs(){
			PageFactory.initElements(driver, this);
		}
	    public void job() throws InterruptedException
		{
			waitForPageLoad();
			waitForElementToDisplay(myjobssearch);
			clickElement(myjobssearch);			    
			waitForElementToDisplay(jobdetails);
			clickElement(jobdetails);
			//delay(5000);
			waitForElementToEnable(jobdetailsload);
			waitForElementToEnable(jobdetailsclose);
		}
	    public void jobclose() {
			
			clickElement(jobdetailsclose);
		}
}
