package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class Global_PrintFunctionality {
	
	@FindBy(xpath="//button[@class='tab medium icon chart-tab-button']")
	public static WebElement chart_Tab;
	
	@FindBy(xpath="//button[@class='tab medium icon chart-tab-button selected']")
	public static WebElement selectedchart_Tab;
	
	@FindBy(xpath="//div[@class='edit-box select-box chart-name-selector chart-selector form-element']//a")
	public static WebElement selectChartName_Dropdown;
	
	@FindBy(xpath="//a[contains(text(),'Pay Range By Component')]")
	public static WebElement selectChart;

	@FindBy(xpath="//button[@class='ok medium chart-refresh-button form-element']")
	public static WebElement refreshChart_Button;
	
	@FindBy(xpath="//button[@title='Print']")
	public static WebElement print_Button;
	
	@FindBy(xpath="//button[@class='apply-button action-button medium next ok']")
	public static WebElement continue_Button;

	@FindBy(xpath="//button[@class='tab medium icon table-tab-button']")
	public static WebElement table_Chart;
	
	@FindBy(xpath="//button[@class='tab medium icon table-tab-button selected']")
	public static WebElement selectedtable_Chart;
	
	@FindBy(xpath="//div[@class='list-item data-node category']//a[contains(text(),'Grid')]")
	public static WebElement Grid_Tab;
	
	
	public Global_PrintFunctionality() {
		PageFactory.initElements(driver, this);
	}
	
	public void chartPrint() {
	
		waitForElementToClickable(chart_Tab);
		 clickElementUsingJavaScript(driver,chart_Tab);
		 System.out.println("chart selected");
		 waitForElementToClickable(print_Button);
		 clickElementUsingJavaScript(driver,print_Button);
		 waitForElementToClickable(continue_Button);
		 clickElement(continue_Button);
	} 
		 
	
	public void GridPrint() {
		waitForElementToDisplay(table_Chart);
		clickElementUsingJavaScript(driver,table_Chart);
		System.out.println("table selected");
		clickElement(print_Button);
		clickElement(Grid_Tab);
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);	
		
	}
	
}
