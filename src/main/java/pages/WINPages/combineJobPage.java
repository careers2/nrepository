package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;



public class combineJobPage {
	
	@FindBy(xpath="//button[@data-purpose='combine-jobs']")
	public static WebElement combineJobs_Tab;
	
	@FindBy(xpath="//input[@data-record='0']")
	public static WebElement selectJob_1;
	
	@FindBy(xpath="//input[@data-record='1']")
	public static WebElement selectJob_2;
	
	@FindBy(xpath="//input[@data-record='2']")
	public static WebElement selectJob_3;
	
	@FindBy(xpath="//button[@data-purpose='combine-jobs']")
	public static WebElement combineButton;
	
	@FindBy(id="combine-job-title")
	public static WebElement jobTitle_Tab;
	
	@FindBy(id="combine-job-code")
	public static WebElement jobCode_Tab;
	
	@FindBy(xpath="//button[@data-purpose='combine-apply']")
	public static WebElement applyButton;
	
	@FindBy(xpath="//button[@data-purpose='save-combined-jobs']")
	public static WebElement DoneButton;
	
	@FindBy(xpath="//span[@title='combine1']")
	public static WebElement combine1_ResultPage;
	
	public combineJobPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void clickcombineJob() {
		waitForElementToDisplay(editViewPage.result_page);
		waitForElementToClickable(combineJobs_Tab);
		clickElementUsingJavaScript(driver,combineJobs_Tab);
	}
	
	public void combinejob() {
		waitForElementToClickable(selectJob_1);
		clickElement(selectJob_1);
		clickElement(selectJob_2);
		clickElement(selectJob_3);
		clickElement(combineButton);
	}
	
	public void jobDetail() {
		setInput(jobTitle_Tab , "combine1");
		setInput(jobCode_Tab , "123");
		clickElement(applyButton);
		clickElement(DoneButton);
	}
	
	
}
