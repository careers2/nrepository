package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class Manage_Benchmark_Settings {
	
	
	@FindBy(xpath="//button[@class='medium action benchmark-button']")
	public static WebElement ManageBenchmarkSettings;
	
	@FindBy(xpath="//button[@class='action cancel medium']")
	public static WebElement Cancel;
	
	@FindBy(xpath="//div[@class='popup admin-benchmark-settings has-title size-to-page size-specific']")
	public static WebElement Benchmarkpopup;
	
	@FindBy(xpath="//a[@href='/v2/Admin/UserProfileAdmin']")
	public static WebElement Userprofilepage;

	
	
	public Manage_Benchmark_Settings() {
		PageFactory.initElements(driver, this);
	}

	public void benchmarkbutton()
	{	
		waitForElementToDisplay(ManageBenchmarkSettings);
		clickElement(ManageBenchmarkSettings);
		waitForElementToDisplay(Benchmarkpopup);
		
	}
	public void benchmarkpopupcancel(){
		waitForElementToDisplay(Cancel);
		clickElement(Cancel);
    }	
	
}
