package pages.WINPages;

import static driverfactory.Driver.*;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class label_ManageJobArchitecture {
	
	@FindBy(xpath="//button[@data-action='goToLabels']")
	public static WebElement label_Tab;
	
	@FindBy(xpath="//input[@data-property='family']")
	public static WebElement MyFamily_1;
	
	@FindBy(xpath="//input[@data-property='families']")
	public static WebElement MyFamilies_2;
	
	@FindBy(xpath="//button[@data-action='save']")
	public static WebElement save_button;
	
	@FindBy(xpath="//div[@class='editor-row section-header-row']//h3[text()= 'Family Hierarchy Labels']")
	public WebElement family_Hierarchy;
	
	@FindBy(xpath="//div[@class='editor-row section-header-row']//h3[text()= 'Career Stream Labels']")
	public WebElement career_stream;
	
	public label_ManageJobArchitecture() {
		PageFactory.initElements(driver, this);
	}
	
	Random rg = new Random();	
	int randomInt = rg.nextInt(10000);	
	String numberAsString = Integer.toString(randomInt);	
	public String familyname1="family"+numberAsString;
	public String familyname2="families"+numberAsString;
	
	public void label() {
		clickElement(label_Tab);
		setInput( MyFamily_1,familyname1 );
		setInput( MyFamilies_2,familyname2);
		waitForElementToClickable(save_button);
		clickElement(save_button);
		setInput( MyFamily_1,"My Family" );
		setInput( MyFamilies_2,"My Families");
	}
	
	public void save() {
		waitForElementToClickable(save_button);
		clickElement(save_button);
	}
}
