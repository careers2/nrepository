package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyJobs_Resultspage {
	
	@FindBy(xpath="//input[@data-record='-1']")
    public static WebElement selectresults;
	    
	@FindBy(xpath="//button[@class='large ok next continue-button']")
	public static WebElement Continue_to_reults;
	
	@FindBy(xpath = "//button[@class='image benchmarking-icon']")
    public static WebElement benchmarkicon ;
    
    @FindBy(xpath="//button[@class='large save-as-button']//b[text()='Save As']")
    public static WebElement saveas;
    
    @FindBy(xpath="//div[@class='group edit-box new-result-name form-element']//input[@type='text']")
    public static WebElement saveasinput;
    
    @FindBy(xpath="//button[@class='action medium ok']")
    public static WebElement Save;
    
    @FindBy(xpath="//div[@class='group edit-box select-box my-saved-results']//span[text()='My Saved Results']")
    public static WebElement savedresultset;
    
    @FindBy(xpath="//div[@class='list-item  data']//following::a[1]")
    public static WebElement savedresultsetselect;
    
    @FindBy(xpath="//div[@class='popup market-pricing-popup has-title size-to-page size-specific']//button[@class='action close-button medium']")
    public static WebElement benchmarkclose;
    
    @FindBy(xpath="//*[@id=\\\"organization\\\"]/div/div/select")
    public static WebElement joborg;
    
    @FindBy(xpath="//b[text()='Save and Close']")
    public static WebElement addjobclose;
    
    @FindBy(xpath="//button[text()='Add New Job']")
    public static WebElement addjob;
    
    @FindBy(xpath="//option[text()='Test org 1 (1)']")
    public static WebElement joborgselect;
    
    @FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
    
    @FindBy(xpath="//a[@href='/v2/Benchmarking/JobsResultsPage']")
	public static WebElement pagetitle;
    
    @FindBy(xpath="//button[@class='action medium ms-market-data-button ok']")
	public static WebElement benchmarkdetailsload;
    
    public static String resultsetname="1";
	
    My_jobs job=new My_jobs();
	 public MyJobs_Resultspage(){
			PageFactory.initElements(driver, this);
		}
	
public void myjobs_results_page() throws InterruptedException {
	waitForElementToEnable(selectresults);
	clickElement(selectresults);
	waitForElementToEnable(Continue_to_reults);
	clickElement(Continue_to_reults);
	waitForPageLoad();
	//delay(2000);
	waitForElementToEnable(job.jobdetails);
	clickElement(job.jobdetails);
	//delay(5000);
	waitForElementToEnable(job.jobdetailsload);
	waitForElementToEnable(job.jobdetailsclose);
	
}
public void jobdetailsclose() {
	clickElement(job.jobdetailsclose);
}
public void benchmarkicon() {
	waitForElementToEnable(benchmarkicon);
	clickElement(benchmarkicon);
	//delay(3000);
	waitForElementToEnable(benchmarkdetailsload);
	waitForElementToEnable(benchmarkclose);
}
public void benchmarkclose() {
	clickElement(benchmarkclose);
}
public void save() {
	waitForElementToDisplay(saveas);
	clickElement(saveas);
	waitForElementToDisplay(saveasinput);
	setInput(saveasinput,resultsetname);
	waitForElementToDisplay(Save);
	clickElement(Save);
	waitForElementToDisplay(Continue);
	clickElement(Continue);
	//delay(2000);
	waitForPageLoad();
	waitForElementToEnable(job.jobdetails);
}
public void resultset() {
	waitForElementToEnable(savedresultset);
	clickElement(savedresultset);
	WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
	clickElement(resultsetaccess);
	//delay(4000);
	waitForPageLoad();
	waitForElementToEnable(job.jobdetails);
	waitForElementToEnable(pagetitle);
}
}
