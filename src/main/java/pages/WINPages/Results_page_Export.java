package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static verify.SoftAssertions.verifyElementText;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Results_page_Export {

	@FindBy(xpath = "//button[@title='Export']")
    public static WebElement Export ;
    
    @FindBy(xpath="//div[@class='popup print-filename-dialog has-title size-to-fit']//b[text()='Done']")
    public static WebElement Done;
    
    @FindBy(xpath="//div[@class='popup print-filename-dialog has-title size-to-fit']//input[@id='xls']")
    public static WebElement xls;
    
    @FindBy(xpath="//button[@class='action medium ok']")
    public static WebElement progresscenterclose;
    
    @FindBy(xpath="//div[@class='group edit-box filename-textbox file-name form-element']//input")
    public static WebElement exportinput;
    
    public static String Exportname="Export";
	
	 public Results_page_Export(){
			PageFactory.initElements(driver, this);
		}
	
public void export_results() {
	//delay(2000);
	waitForElementToDisplay(Export);
	clickElement(Export);
	waitForElementToDisplay(exportinput);
	setInput(exportinput,Exportname);
	waitForElementToDisplay(Done);
	clickElement(Done);
	waitForElementToEnable(progresscenterclose);
	clickElement(progresscenterclose);
	//delay(3000);
	/*ProgressCenter prog= new ProgressCenter();
	prog.waitReportGeneration();
	prog.progress(prog.GlobalprogressCenter_button);
	verifyElementText(ProgressCenter.complete_status, "Complete");
	verifyElementText(ProgressCenter.report_name, Exportname);
	prog.progress(prog.GlobalProgress_close);*/
	
}

public void export_results_xls() {
	//delay(3000);
	waitForElementToDisplay(Export);
	clickElement(Export);
	waitForElementToDisplay(exportinput);
	setInput(exportinput,Exportname);
	waitForElementToDisplay(xls);
	clickElement(xls);
	waitForElementToDisplay(Done);
	clickElement(Done);
	waitForElementToEnable(progresscenterclose);
	clickElement(progresscenterclose);
	/*prog.waitReportGeneration();
	prog.progress(prog.GlobalprogressCenter_button);
	verifyElementText(ProgressCenter.complete_status, "Complete");
	verifyElementText(ProgressCenter.report_name, Exportname);
	prog.progress(prog.GlobalProgress_close);
	delay(2000);*/
	
	
	
	
	//waitForElementToDisplay(joborg);
	//clickElement(joborg);
}
}
