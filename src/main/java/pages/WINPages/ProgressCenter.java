package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProgressCenter {
	
	@FindBy(xpath="//button[@class='icon image plus']")
	public  WebElement Progress_Center;
	
	@FindBy(xpath="//button[@data-purpose='show-progress-center']")
	public  WebElement JobprogressCenter_button;
	
	@FindBy(xpath="//button[@class='image plus']")
	public  WebElement GlobalprogressCenter_button;
	
	@FindBy(xpath="//div[@class='progress']")
	public WebElement GlobalProgress_close;
	
	 @FindBy(xpath="//button[@class='icon image plus']")
	public WebElement DiagnosticProgressCenter_button;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col0 root']//span[text()='Complete']")
	public  static WebElement Jobcomplete_status;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col1 root']//a[@class='report-name']")
	public static  WebElement Jobreport_name;
	
	@FindBy(xpath="//div[@class='alert-grid data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col0 root']//span[text()='Complete']")
	public  static WebElement Globalcomplete_status;
	
	@FindBy(xpath="//div[@class='alert-grid data-grid display-below portal fixed-width']//table[@class='data-table']//tbody//tr[@class='odd'][1]//td[@class='col1 root']//a[@class='report-name']")
	public static  WebElement Globalreport_name;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal fixed-width']//div[@class='table-portal' and @style]")
	public static WebElement MJlreport_Table;
	
	@FindBy(xpath="//div[@class='report-queue-alert-box popup container' and @style='display: block; z-index: 1002;']")
	public static WebElement globalreport_Table;
	
	@FindBy(xpath="//div[@class='popup report-queue-alert-box has-title size-specific']//table[@class='data-table']//tr[@class='odd'][1]//td[@class='col0 root'][1]//span[text()='Complete']")
	public  static WebElement complete_status;
	
	@FindBy(xpath="//div[@class='popup report-queue-alert-box has-title size-specific']//table[@class='data-table']//tr[@class='odd'][1]//td[@class='col0 root'][1]//following::a[1]")
	public static  WebElement report_name;
	
	public  ProgressCenter() {
		PageFactory.initElements(driver, this);
	}
	
	public void waitReportGeneration() {
		delay(25000); 
	}
	
	public void waitdiagnosticReportGeneration() {
		delay(30000); 
	}
	
	public void waitforReportGeneration() {
		delay(35000); 
	}
	
	 public void  progress(WebElement element ) {
		waitForElementToClickable(element);
		clickElement(element);
	 }
	
}
