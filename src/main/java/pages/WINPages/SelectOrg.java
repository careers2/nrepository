package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectOrg {

	@FindBy(xpath="//div[contains(text(),'WIN QA Organization [winqa123]')]")
	public static WebElement orgname;
	
	@FindBy(xpath="//*[@id=\"doneButton\"]/div/b")
	public static WebElement cntbutton;

	public SelectOrg() {
		PageFactory.initElements(driver, this);
	}
	
	public void Organization()
	{
		waitForElementToDisplay(orgname);
		clickElement(orgname);
		clickElement(cntbutton);
	}	
}
