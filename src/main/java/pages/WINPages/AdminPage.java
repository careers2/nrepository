package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {
	
	@FindBy(xpath="//div[@id='page-header']//li[@class='has-dropdown']//i[@class='m-icon-menu']//parent::a")
	public static WebElement Tools;
	@FindBy(xpath="//a[@href='/v2/Admin/AdminHomePage']")
	public static WebElement Administration;
	
	public AdminPage()
	{
		PageFactory.initElements(driver, this);
		
	}

	public void Adminbutton()
	{
		waitForElementToDisplay(Tools);
		clickElement(Tools);
		waitForElementToDisplay(Administration);
		clickElement(Administration);
	}

}
