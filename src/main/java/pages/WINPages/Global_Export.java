package pages.WINPages;

import static driverfactory.Driver.*;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Global_Export {
	@FindBy(xpath="//button[@title='Export']")
	public static WebElement export_Button;
	
	@FindBy(xpath="//div[@class='popup-content']//input[@maxlength='200']")
	public static WebElement fileName;
	
	@FindBy(xpath="//button[@class='ok medium']")
	public static WebElement done_Button;
	
	@FindBy(xpath="//*[contains(text(),'Close']")
	public static WebElement close_button;
	
	
	public Global_Export() {
		PageFactory.initElements(driver, this);
	}
	
	public void sendGlobalexport() {
		waitForElementToClickable(export_Button);
		clickElement(export_Button);	
	}
	
	public  void GlobalReportfileOptions(String filename)
	{
		waitForElementToClickable(fileName);
		setInput(fileName , filename);
		clickElement(done_Button);
	}
	
	public void closePopup() {
		waitForElementToClickable(close_button);
		clickElement(close_button);
	}
}
