package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyEmployees_Resultspage {
	 @FindBy(xpath="//input[@data-record='-1']")
	    public static WebElement selectresults;
	    
	    @FindBy(xpath="//button[@class='large ok next continue-button']")
	    public static WebElement Continue_to_reults;
	    
	    @FindBy(xpath="//button[@class='large save-as-button']")
	    public static WebElement saveas;
	    
	    @FindBy(xpath = "//div[@class='save-as-dialog']//input[@type='text']")
	    public static WebElement saveasinput;
	    
	    @FindBy(xpath="//button[@class='action medium ok']")
	    public static WebElement Save;
	    
	    @FindBy(xpath="//div[@class='group edit-box select-box my-saved-results']//span[text()='My Saved Results']")
	    public static WebElement resultset;
	    
	    @FindBy(xpath = "//div[@class='list-item  data']//following::a[1]")
	    public static WebElement firstresultset;
	    
	    @FindBy(xpath="//div[@id='page-title']//h1")
		public static WebElement Myemployeesresult;
	    
	    @FindBy(xpath="//h1[@class='results-page']")
		public static WebElement resultpage;
	    
	    @FindBy(xpath="//b[text()='Continue']")
		public WebElement Continue;
	    
	    @FindBy(xpath="//a[@href='/v2/Benchmarking/EmployeesResultsPage']")
		public static WebElement pagetitle;
	    
	    @FindBy(xpath="//a[@data-record='0' and @data-column='1']")
		public WebElement jobdetails;
	    
	    @FindBy(xpath="//div[@class='popup my-saved-results-dropdown tab select right has-title size-specific']//div[@class='list-item  data'][1]//div[@class='label'][1]")
		public WebElement savedresultsetname;
	    
	    MyEmpdetails myemp = new MyEmpdetails();
	    Thinking thinking=new Thinking();
	    
	    Random rg = new Random();	
		int randomInt = rg.nextInt(100);	
		String numberAsString = Integer.toString(randomInt);
		public String Resultset="Result set"+numberAsString;
		
		public String resultsetname="1";
		
		public static String s1="";
		
	    public MyEmployees_Resultspage(){
			PageFactory.initElements(driver, this);
		}
	    public void resultspage()
		{
			waitForElementToDisplay(selectresults);
			clickElement(selectresults);
			waitForElementToDisplay(Continue_to_reults);
			clickElement(Continue_to_reults);	
			waitForPageLoad();
			//delay(6000);
			waitForElementToEnable(myemp.empdetailslink);
			clickElement(myemp.empdetailslink);
			//delay(5000);
			waitForElementToEnable(myemp.empdetailsload);
			waitForElementToDisplay(myemp.empdetailslinkclose);
			
		}
	    public void emplinkclose() 
	    {
			clickElement(myemp.empdetailslinkclose);
	    }
	    public void jobdetails() {

			waitForElementToDisplay(jobdetails);
			clickElement(jobdetails);
			//delay(8000);
			waitForElementToEnable(myemp.jobdetailsload);
			waitForElementToEnable(myemp.jobdetailsclose);
			
		}
		public void jobdetailsclose() {
			
			clickElement(myemp.jobdetailsclose);
			
		}
	    public void save() 
	    {
	    	
			waitForElementToDisplay(saveas);
			clickElement(saveas);
			waitForElementToDisplay(saveasinput);
			setInput(saveasinput,resultsetname);
			waitForElementToDisplay(Save);
			clickElement(Save);
			waitForElementToEnable(Continue);
			clickElement(Continue);
			waitForPageLoad();
			waitForElementToEnable(myemp.empdetailslink);
			//delay(4000);
	    }
	    public void resultset() 
	    {
	    	waitForElementToEnable(resultset);
			clickElement(resultset);
			WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
			clickElement(resultsetaccess);
			//delay(2000);
			waitForPageLoad();   
			waitForElementToEnable(myemp.empdetailslink);
			waitForElementToEnable(pagetitle);
		}

}
