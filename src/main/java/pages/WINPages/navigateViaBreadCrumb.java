package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class navigateViaBreadCrumb {
	
	@FindBy(xpath = "//a[text()='Mercer Market Data']")
	public  WebElement mmd_breadcrumb;
	
	@FindBy(xpath="//a[text()='Home']")
	public  WebElement home_Breadcrumb;
	
	@FindBy(xpath = "//a[text()='Administration']")
	public  WebElement admin_breadcrumb;
	
	@FindBy(xpath = "//a[@class='dropdown-target']")
	public  WebElement Tools_dropdown;
	
	@FindBy(xpath = "//li[@class='modules']//a[@href='/v2/Admin/AdminHomePage']")
	public static  WebElement Administration;
	
	@FindBy(xpath = "//a[@href='/v2/Admin/AdminHomePage']")
	public static  WebElement Administration_Tools;
	
	public navigateViaBreadCrumb() {
		PageFactory.initElements(driver, this);
	}
	
	public void navigateBack(WebElement e) {
		waitForElementToDisplay(e);
		clickElementUsingJavaScript(driver,e);
		System.out.println("navigated page");
	}
	
	public void navigateusingtools(WebElement e) {
		waitForElementToDisplay(e);
		clickElementUsingJavaScript(driver,e);
		clickElement(Administration_Tools);
		System.out.println("navigated page");
	}
	
	
}
