package pages.WINPages;
import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.InitTests;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;


public class Access_Manage_Evaluation {
	
	
	
	@FindBy(xpath="//b[text()='Manage Evaluation Approvals']")
	public static WebElement Access_Manage_Evaluation_Button;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//div[@class='data']//input[@data-property='canApprove']")
	public static WebElement Eval_App;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//div[@class='data']//select[@data-property='threshold']")
	public static WebElement Max_PC;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//div[@class='data']//select[@data-property='threshold']//option[@value='84']")
	public static WebElement PC_Value_1;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//div[@class='data']//select[@data-property='threshold']//option[@value='87']")
	public static WebElement PC_Value_2;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//td//select[@data-property='approver1']")
	public static WebElement Approver_1_drop_down;
	
	@FindBy(xpath="//table[@class='data-table']//tr[@data-property='all'][1]//td//select[@data-property='approver1']//option[text()='87 - WINUSER, kishorTest']")
	public static WebElement Approver_1_Value;

	@FindBy(xpath="//button[@class='btn btn-large btn-ok']")
	public static WebElement Save_Button;
	
	
	public Access_Manage_Evaluation() {
		PageFactory.initElements(driver, this);
	}
	
	public void Manage_Evaluation_Approvals() throws InterruptedException{
		waitForElementToDisplay(Access_Manage_Evaluation_Button);
		clickElement(Access_Manage_Evaluation_Button);
		waitForElementToDisplay(Eval_App);
		clickElement(Eval_App);
		if ( !(Eval_App.isSelected()) )
		{
		     clickElement(Eval_App);
		}
		waitForElementToDisplay(Max_PC);
		clickElement(Max_PC);
		waitForElementToDisplay(PC_Value_1);
		clickElement(PC_Value_1);
		waitForElementToDisplay(Approver_1_drop_down);
		clickElement(Approver_1_drop_down);
		waitForElementToDisplay(Approver_1_Value);
		clickElement(Approver_1_Value);
		waitForElementToDisplay(Save_Button);
		clickElement(Save_Button);
		
		waitForElementToDisplay(Eval_App);
		clickElement(Eval_App);
		if ( !(Eval_App.isSelected()) )
		{
		     clickElement(Eval_App);
		}
		waitForElementToDisplay(Max_PC);
		clickElement(Max_PC);
		waitForElementToDisplay(PC_Value_2);
		clickElement(PC_Value_2);
		waitForElementToDisplay(Save_Button);
		clickElement(Save_Button);
		
	}

}
