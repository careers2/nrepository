package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Manage_My_Organization {

	@FindBy(xpath="//a[text()='Manage My Organizations']")
	public static WebElement managemyorganization;
	
	@FindBy(xpath="//button[@class='icon medium ok search-button icon submit']")
	public static WebElement Search;

	@FindBy(xpath="//a[@data-record='0' and @data-column='0']")
	public static WebElement Organizationdetails;		
	
	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static WebElement Close;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static WebElement ManageMyOrganization;
	
	@FindBy(xpath="//b[text()='Organization Sizing']")
	public static WebElement Organizationdetailsload;

	public Manage_My_Organization() {
		PageFactory.initElements(driver, this);
	}
	public void organization()
	{	
		clickElement(managemyorganization);
		waitForPageLoad();
	}
	public void organizationpage() {
		clickElement(Search);
		clickElement(Organizationdetails);
		waitForElementToEnable(Organizationdetailsload);
		clickElement(Close);
	}

}
