package pages.WINPages;

import static driverfactory.Driver.*;

import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DiagnosticReportPage {
	@FindBy(xpath="//div[@id='feature-menu-link1']//a")
	public static WebElement DiagnosticReport_Tab;
	
	@FindBy(xpath="//h1[text()='Diagnostic Report']")
	public static WebElement diagnosticPage;
	
	@FindBy(xpath="//div[@data-property='MarketViewId']//div[@class='body']")
	public static WebElement MarketView_Dropdown;
	
	@FindBy(xpath="//a[@title='2017 Global MV TEST E2E3 - INC']")
	public static WebElement select_MarketView;
	
	@FindBy(xpath="//div[@data-property='PrimaryMarketComparison']//div[@class='body']")
	public  static WebElement primaryMarket_Dropdown;
	
	@FindBy(xpath="//div[@data-id='1']")
	public static WebElement select_primarymarket;

	@FindBy(xpath="//div[@data-property='SecondaryMarketComparison']//div[@class='body']")
	public  static WebElement SecondaryMarket_Dropdown;
	
	@FindBy(xpath="//div[@data-id='2']")
	public static WebElement select_secondarymarket;
	
	@FindBy(xpath="//div[@data-property='FirstComparisonStatistic']//div[@class='body']//span[@class='twisty']")
	public  static WebElement FirstComparison_Dropdown;
	
	@FindBy(xpath="//div[@data-id='7']")
	public static WebElement select_firstComparison;
	
	@FindBy(xpath="//div[@data-property='SecondComparisonStatistic']//div[@class='body']//span[@class='twisty']")
	public  static WebElement SecondaryComparison_Dropdown;
	
	@FindBy(xpath="//div[@data-id='5']")
	public static WebElement select_secondarycomparison;
	
	@FindBy(xpath="//button[@class='large ok run-report']")
	public static WebElement RunReport_Tab;
	
	@FindBy(xpath="//button[@class='action-button apply-button medium next ok']")
	public static WebElement Continue_Button;
	
	@FindBy(xpath="//div[@class='body']//input[@maxlength='200']")
	public static WebElement fileName;
	
	@FindBy(xpath="//button[@class='action medium ok']")
	public static WebElement done_Button;
	
	@FindBy(xpath="//img[@class='chart-image']")
	public static WebElement chart_image;
	
	public DiagnosticReportPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void ClickDiagnosticReport() {
		waitForElementToClickable(DiagnosticReport_Tab);
		clickElementUsingJavaScript(driver,DiagnosticReport_Tab);
	}
	
	public void ReportSelections() throws InterruptedException {
		waitForElementToClickable(MarketView_Dropdown);
		clickElement(MarketView_Dropdown);
		clickElement(select_MarketView);
		waitForElementToDisplay(chart_image);
		waitForElementToClickable(primaryMarket_Dropdown);
		clickElementUsingJavaScript(driver,primaryMarket_Dropdown);
		clickElement(select_primarymarket);
		clickElementUsingJavaScript(driver,SecondaryMarket_Dropdown);
		clickElement(select_secondarymarket);
		scrollToElement(driver,FirstComparison_Dropdown);
		clickElement(FirstComparison_Dropdown);
		clickElement(select_firstComparison);
		clickElement(SecondaryComparison_Dropdown);
		clickElement(select_secondarycomparison);	
	}
	
	public void RunReport() {
		waitForElementToClickable(RunReport_Tab);
		clickElement(RunReport_Tab);
		clickElement(Continue_Button);
		setInput(fileName,"DiagnosticReport_GlobalMVE2E3");
		clickElement(done_Button);
	}
}
