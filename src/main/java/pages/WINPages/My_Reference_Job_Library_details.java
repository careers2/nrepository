package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Reference_Job_Library_details {
	
	
	@FindBy(xpath="//button[@class='icon medium ok search-button icon submit']")
	public static  WebElement referencejobsearch;
	
	@FindBy(xpath="//a[@data-record='0' and @data-column='0']")
	public static  WebElement detailspopup;

	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static  WebElement popupclose;
	
	Thinking think=new Thinking();
	
	@FindBy(xpath="//b[text()='Reference Evaluation']")
	public static  WebElement detailspopupload;
	
	@FindBy(xpath="//div[@id='search-criteria']//input[@type='text']")
	public static  WebElement referencejobinput;
	
	/*@FindBy(xpath="//div[@class=' listbox-container']//following::a[1]")
	public static  WebElement Organizationselect;
	
	@FindBy(xpath="//div[@class='group edit-box select-box form-element selector job-country span3']//following::a[1]")
	public static  WebElement Country;
	
	@FindBy(xpath="//div[@class='popup select-list create-jobs countries right tab select has-title size-specific']//following::a[1]")
	public  static  WebElement Countryselect;
	
	@FindBy(xpath="//div[@class='form-element selector job-code span3']//following::div")
	public static  WebElement Jobcodeext;
	
	@FindBy(xpath="//div[@class='popup tab combobox-popup right has-title size-to-fit']//following::input[1]")
	public static  WebElement jobextselect;*/
	
	public My_Reference_Job_Library_details(){
		PageFactory.initElements(driver, this);
	}
	
	public void reference_jobs_details()
	{	
		waitForPageLoad();
		waitForElementToEnable(referencejobinput);
		setInput(referencejobinput,"Head");
		waitForElementToEnable(referencejobsearch);
		clickElement(referencejobsearch);
		waitForElementToEnable(detailspopup);
		clickElement(detailspopup);
		//think.thinking();
		waitForElementToEnable(detailspopupload);
		waitForElementToEnable(popupclose);
		
	}
	public void detailsclose() {
		clickElement(popupclose);
		
	}

}
