package pages.WINPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.*;

public class MJL_PrintFunctionality {
	
	@FindBy(xpath="//button[@data-id='chart-tab']")
	public static WebElement chart_Tab;
	
	@FindBy(id="report-chart-image-selector")
	public static WebElement selectAnalysis_Dropdown;
	
	@FindBy(id="chart-name")
	public static WebElement selectChartName_Dropdown;
	
	@FindBy(id="market-view")
	public static WebElement selectMVDropdown;
	
	@FindBy(id="refinement-select")
	public static WebElement selectRefinement_Dropdown;

	@FindBy(id="statistic-select")
	public static WebElement selectStatistic_Dropdown;
	
	@FindBy(xpath="//button[@title='Refresh Chart']")
	public static WebElement refreshChart_Button;
	
	@FindBy(xpath="//button[@data-purpose='report-print']")
	public static WebElement print_Button;
	
	@FindBy(xpath="//button[@data-purpose='print-continue']")
	public static WebElement continue_Button;
	
	@FindBy(xpath="//button[@data-id='report-tab']")
	public static WebElement table_Tab;
	
	@FindBy(xpath="//div[@class='print-options-dialog child-select']//h4")
	public static WebElement sample_Tab;
	
	@FindBy(xpath="//li[@data-id='3']")
	public static WebElement comparison_tab;
	
	@FindBy(xpath="//li[@data-id='1']")
	public WebElement list_tab;
	
	@FindBy(xpath="//li[@data-id='0']")
	public WebElement summary_tab;
	
	@FindBy(xpath="//li[@data-id='14']")
	public static WebElement chartwithtable_tab;
	
	public MJL_PrintFunctionality() {
		PageFactory.initElements(driver, this);
	}
	
	public void chartPrint() {
		 waitForPageLoad();
		 waitForElementToClickable(chart_Tab);
		 clickElementUsingJavaScript(driver,chart_Tab);
		 System.out.println("Chart selected");
		 waitForElementToClickable(selectAnalysis_Dropdown);
		 selEleByValue(selectAnalysis_Dropdown , "141:Finance Business Partners - Experienced Professional|142:FIN.02.001.P20");
		 selEleByValue(selectChartName_Dropdown ,"PayMix");
		 selEleByValue(selectMVDropdown ,"b58a0282-ecc9-e511-86d5-002481adb66c");
		 selEleByValue(selectRefinement_Dropdown ,"00");
		 selEleByValue(selectStatistic_Dropdown ,"7");
		 waitForElementToClickable(refreshChart_Button);
		 clickElementUsingJavaScript(driver,refreshChart_Button);
		 printReport(chartwithtable_tab);
		
	}
	
	public void comparisonPrint() {
		 waitForElementToClickable(table_Tab);
		 clickElementUsingJavaScript(driver,table_Tab);
		 System.out.println("Table selected");
		 printReport(comparison_tab);
	}
	
	public void printReport(WebElement ele) {
		 waitForElementToClickable(print_Button);
		 clickElementUsingJavaScript(driver,print_Button);
		 waitForElementToDisplay(sample_Tab);
		 waitForElementToClickable(ele);
		 clickElement(ele);
		 clickElement(continue_Button);
	}
}
