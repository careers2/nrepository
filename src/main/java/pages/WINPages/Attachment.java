package pages.WINPages;

import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Attachment {
	@FindBy(xpath="//div[@id='feature-menu-link5']//a")
	public static WebElement attachment_tab;
	
	public Attachment() {
		PageFactory.initElements(driver, this);
	}
	
	public void accessAttachment() {
		waitForPageLoad();
		waitForElementToClickable(attachment_tab);
		clickElementUsingJavaScript(driver,attachment_tab);
	}
}
