package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MUPCS_Catalog_Results_Page {
	
	@FindBy(xpath="//input[@data-record='-1']")
	public static WebElement resultset;
	
	@FindBy(xpath="//button[@class='large ok next continue-button']")
	public static WebElement Continue;
	
	@FindBy(xpath="//button[@class='icon icon send-to-mml bulk-task']")
	public static WebElement Sendtomyreferencelibrary;
	
	@FindBy(xpath="//div[@class='send-to-reference-library-container']//input[@data-purpose='copy-all-from-mercer']")
	public static WebElement selectall;
	
	@FindBy(xpath="//button[@class='action apply-button medium ok']")
	public static WebElement Send;
	
	@FindBy(xpath="//button[@class='apply-button cancel medium']")
	public static WebElement Overwrite;
	
	@FindBy(xpath="//button[@class='action medium ok']")
	public static WebElement Ok;
	
	@FindBy(xpath="//a[@href='/v2/Benchmarking/MupcsResultsPage']")
	public static WebElement MUPCSCatalogResultspage;
	
	@FindBy(xpath="//a[@data-record='0']")
	public static WebElement MUPCSCatalogResultspageload;
	
	@FindBy(xpath="//div[@class='popup size-to-fit']//div[@class='content']")
	public static WebElement Sendtomyreferencelibrarypopup;
	
	@FindBy(xpath="//a[@data-record='0']")
	public static WebElement Searchresultsload;
	
	
	
	public MUPCS_Catalog_Results_Page() {
		PageFactory.initElements(driver, this);
	}
	public void mupcscatalogresultspage() throws InterruptedException
	{	
		waitForElementToEnable(Searchresultsload);
		waitForElementToDisplay(resultset);
		clickElement(resultset);
		clickElement(Continue);
		waitForPageLoad();
		waitForElementToEnable(MUPCSCatalogResultspageload);
		waitForElementToEnable(Sendtomyreferencelibrary);
	}
	public void mupcscatalogresults() {
		waitForElementToDisplay(Sendtomyreferencelibrary);
		clickElement(Sendtomyreferencelibrary);
		waitForElementToDisplay(selectall);
		clickElement(selectall);
		waitForElementToDisplay(Send);
		clickElement(Send);
		waitForElementToDisplay(Overwrite);
		clickElement(Overwrite);
		waitForElementToEnable(Sendtomyreferencelibrarypopup);
	}
	public void sendtoreferenecelibrarypopup() {
		clickElement(Ok);	
	}
	
}
