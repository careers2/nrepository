package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_jobs_search_page {
	
	 @FindBy(id="feature-menu-button2")
	 public static WebElement Myjobs;
	 
	 @FindBy(xpath="//a[@href='/v2/Benchmarking/JobsSearchPage']")
	 public static WebElement Myjobssearchpage;
	 
	 public My_jobs_search_page(){
			PageFactory.initElements(driver, this);
		}
	 public void jobssearchpage() throws InterruptedException
		{
			waitForElementToDisplay(Myjobs);
			clickElement(Myjobs);
			waitForPageLoad();
		}

}
