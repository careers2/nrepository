package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.selEleByVisbleText;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Jobs_Add_New_Job {
	
	@FindBy(xpath = "//input[@data-property='JobTitle']")
    public static WebElement jobtitle;
    
    @FindBy(xpath="//*[@id=\\\"search-criteria\\\"]/div[10]/div[1]/div[4]/button/div/b")
    public static WebElement jobsearch;
    
    @FindBy(xpath="//button[@class='action icon large manage-jobs-button']//b[text()='Manage Jobs']")
    public static WebElement managejob;
    
    @FindBy(xpath="/html/body/div[23]/div[10]/div/div[1]/div/div[1]/div[2]/div/div[3]/input")
    public static WebElement jobcode;
    
    @FindBy(xpath="//div[@data-property='JobCodeExt']//input[@class='display ellipsis']")
    public static WebElement jobext;
    
    @FindBy(xpath="//select[@data-property='CountryCode']")
    public static WebElement jobcountry;
    
    @FindBy(xpath="//option[text()='Albania (AL)']")
    public static WebElement jobcountryselect;
    
    @FindBy(xpath="//span[@class='combobox-title']")
    public static WebElement jobexttwisty;
    
    @FindBy(xpath=" //div[@data-property='JobCodeExt']//div[@class='body']")
    public static WebElement jobexttwistyexpand;
    
    @FindBy(xpath="//select[@data-property='OrganizationId']")
    public static WebElement joborg;
    
	@FindBy(xpath="//b[text()='Save']")
	public static WebElement Save;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']//following::b")
	public static WebElement Close;
    
    @FindBy(xpath="//button[text()='Add New Job']")
    public static WebElement addjob;
    
    @FindBy(xpath="//select[@data-property='OrganizationId']//following::option[1]")
    public static WebElement joborgselect;
	
    @FindBy(xpath="//div[@class='popup bench-job-details-popup full-page has-title size-to-page size-specific']//div[@data-form-item-property='JobTitle']//child::div")
    public static WebElement jobnameverify;
    
    @FindBy(xpath = "//b[text()='Market Comparisons']")
    public static WebElement jobdetailsload;
    
    public static String JobExt="2";
	
    Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String Jobtitle="Job"+numberAsString;
	
    
public My_Jobs_Add_New_Job() {
	PageFactory.initElements(driver, this);
}

public void add_job() {
	
	waitForElementToDisplay(managejob);
	clickElement(managejob);
	waitForElementToDisplay(addjob);
	clickElement(addjob);
	//delay(5000);
	waitForElementToDisplay(jobtitle);
	setInput(jobtitle,Jobtitle);
	//waitForElementToDisplay(jobcode);
	//setInput(jobcode,"1004");
	waitForElementToDisplay(jobexttwistyexpand);
	clickElement(jobexttwistyexpand);
	waitForElementToDisplay(jobext);
	setInput(jobext,JobExt);
	waitForElementToDisplay(jobexttwisty);
	clickElement(jobexttwisty);
	waitForElementToDisplay(jobcountry);
	clickElement(jobcountry);
	waitForElementToDisplay(jobcountryselect);
	clickElement(jobcountryselect);
	waitForElementToDisplay(joborg);
	clickElement(joborg);
	waitForElementToDisplay(joborgselect);
	clickElement(joborgselect);
	//waitForElementToDisplay(joborg);
	//clickElement(joborg);
	waitForElementToDisplay(Save);
	clickElement(Save);
	waitForElementToEnable(jobdetailsload);
	//delay(4000);
	waitForElementToDisplay(Close);
}
public void add_job_close() {
	clickElement(Close);
}

}
