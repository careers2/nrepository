package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Market_Library_Results_page {
	
	@FindBy(xpath="//input[@data-record='-1']")
	public static  WebElement resultsselect;
	
	@FindBy(xpath="//button[@class='large ok next continue-button']//b[text()='Continue']")
	public static  WebElement Continue;
	
	@FindBy(xpath="//b[text()='Continue']")
	public static  WebElement overwrite;
	
	@FindBy(xpath="//div[@id='grid3']//following::a[1]")
	public static  WebElement recorddetailspopup;
	
	@FindBy(xpath="//div[@id='grid3']//following::a[2]")
	public static  WebElement sourcedetailspopup;
	
	@FindBy(xpath="//button[@class='large save-as-button']")
	    public static WebElement saveas;
	    
	@FindBy(xpath = "//div[@class='save-as-dialog']//input[@type='text']")
	    public static WebElement saveasinput;
	    
	@FindBy(xpath="//button[@class='action medium ok']")
	    public static WebElement Save;
	    
	@FindBy(xpath="//div[@class='group edit-box select-box my-saved-results']//span[text()='My Saved Results']")
	    public static WebElement resultset;
	    
	@FindBy(xpath = "//div[@class='list-item  data']//a[@title='1']")
	    public static WebElement firstresultset;
	
	@FindBy(xpath="//div[@id='page-title']//h1")
	public static WebElement MyLibraryresults;
	
	@FindBy(xpath="//a[@href='/v2/Benchmarking/LibraryResultsPage']")
	public static WebElement pagetitle;
	
	@FindBy(xpath="//div[@class='popup my-saved-results-dropdown tab select right has-title size-specific']//a[@title='1']")
	public static WebElement set;
	
	public static String s1="";
	
	
	public String resultsetname="1";
	
	My_Market_Library_details details=new My_Market_Library_details();
	
	public My_Market_Library_Results_page (){
		PageFactory.initElements(driver, this);
	}
	
	public void libraryresultspage()
	{
		waitForElementToEnable(resultsselect);
		clickElement(resultsselect);
		waitForElementToEnable(Continue);
		clickElement(Continue);
		waitForPageLoad();
		//delay(4000);
	}
	public void libraryresultspagedetails() {
		waitForElementToEnable(recorddetailspopup);
		clickElement(recorddetailspopup);
		waitForElementToEnable(details.recorddetailspopupload);
		waitForElementToEnable(details.popupclose);
		//delay(2000);
	}
	
	public void recorddetailsclose()
	{
		
		clickElement(details.popupclose);
	}
	public void sourcedetils()
	{
		waitForElementToEnable(sourcedetailspopup);
		clickElement(sourcedetailspopup);
		waitForElementToEnable(details.sourcedetailspopupload);
		waitForElementToEnable(details.mmlpopupclose);
		//delay(2000);
	}
	public void sourcedetilsclose()
	{
		
		clickElement(details.mmlpopupclose);	
	}
	public void save()
	{
		waitForElementToEnable(saveas);
		clickElement(saveas);
		waitForElementToEnable(saveasinput);
		setInput(saveasinput,resultsetname);
		waitForElementToEnable(Save);
		clickElement(Save);
		waitForElementToEnable(overwrite);
		clickElement(overwrite);
		//delay(1000);
		waitForPageLoad();
		waitForElementToEnable(recorddetailspopup);
		waitForElementToEnable(pagetitle);
	}
	
	public void resultset() {
		waitForElementToEnable(resultset);
		clickElement(resultset);
		WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
		clickElement(resultsetaccess);
		//delay(4000);
		waitForPageLoad();
		waitForElementToEnable(recorddetailspopup);
		waitForElementToEnable(pagetitle);
		
	}

}
