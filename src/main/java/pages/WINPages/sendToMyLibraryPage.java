package pages.WINPages;

import static driverfactory.Driver.driver;
import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class sendToMyLibraryPage {
	
	@FindBy(xpath="//button[@class='report-control-button send-to-library-task']")
	public WebElement MJlsendToLibraryTab;
	
	@FindBy(xpath="//button[@data-purpose='send-to-mml']")
	public WebElement GlobalsendToLibrary_Tab;
	
	@FindBy(xpath="//p[contains(text(),'This data has been sent to the My Market Library')]")
	public WebElement text_message;
	
	@FindBy(xpath="//button[@class='apply-button' and @data-id='global-alert']")
	public WebElement alert_close;
	
	@FindBy(xpath="//div[@class='send-to-mml-success-dialog dialog-popup popup container']//div[@class='popup-content']")
	public WebElement Global_popup;
	
	@FindBy(xpath="//div[@class='send-to-mml-success-dialog dialog-popup popup container']//button[@class='ok medium']")
	public WebElement popup_close;
	
	@FindBy(xpath="//input[@data-purpose='copy-all-from-mercer']")
	public static WebElement copy_all_mercer;
	
	@FindBy(xpath="//tr[@class='header-row']//button[@title='Auto Generate Reference Job Code']")
	public static WebElement autoGenerateReferenceJob;
	
	@FindBy(xpath="//button[@class='apply-button ok medium action']")
	public static WebElement send_button;
	
	@FindBy(xpath="//div[@class='modal min']//p[contains(text(),'The data has now been sent to My Reference Job Library and is now available for use in Job Evaluation')]")
	public WebElement referenceJobText_message;
	
	@FindBy(xpath="//button[text()='Overwrite']")
	public static WebElement overwrite_button;
	
	@FindBy(xpath="//div[@class='thinking']")
	public static WebElement thinking;
	
	public sendToMyLibraryPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void sendRecord(WebElement ele) {
		 waitForElementToClickable(ele);
		 clickElementUsingJavaScript(driver,ele);
		  
	}
	
	public void closePopup(WebElement ele) {
		
		 waitForElementToClickable(ele);
		 clickElement(ele);
	}
	
	public void generateJobcode() {
		waitForElementToClickable(copy_all_mercer);
		 clickElement(copy_all_mercer);
		 //waitForElementToClickable(autoGenerateReferenceJob);
		 //clickElement(autoGenerateReferenceJob);
		 waitForElementToClickable(send_button);
		 clickElement(send_button);
		 waitForElementToClickable(overwrite_button);
		 clickElement(overwrite_button);
		 waitForElementToDisplay(referenceJobText_message);
	}
	
	
}

