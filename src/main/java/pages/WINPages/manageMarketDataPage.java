package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class manageMarketDataPage {

	
	@FindBy(xpath="//button[@class='report-control-button manage-mv-task']")
	public static WebElement manageMarketViews_Tab;

	@FindBy(id="cb-15130219-ebc9-e511-86d5-002481adb66c")
	public static WebElement parentMV;
	
	@FindBy(id="cb-b58a0282-ecc9-e511-86d5-002481adb66c")
	public static WebElement childMv;

	@FindBy(xpath="//button[@data-purpose='combine-marketviews']")
	public static WebElement combineButton;
	
	@FindBy(xpath="//button[@class='inpt-grp-button-action']")
	public static WebElement mmvDone_Button;

	@FindBy(xpath="(//div[@class='body']//a[@title='Combined Market View 1'])[1]")
	public static WebElement combineMarketView1;
	
	public manageMarketDataPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void combineMVPage() {
		waitForElementToClickable(manageMarketViews_Tab);
		clickElementUsingJavaScript(driver,manageMarketViews_Tab);
		System.out.println("manage Market View  selected");
	}
	
	public void combineMv() 
	{
		clickElement(parentMV);
		clickElement(childMv);
		clickElement(combineButton);
		waitForElementToClickable(mmvDone_Button);
		clickElement(mmvDone_Button);
		
	}
	
	
}
