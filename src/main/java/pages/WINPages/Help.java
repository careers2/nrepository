package pages.WINPages;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.InitTests;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class Help {
	@FindBy(xpath="//*[@class='m-icon-help']")
	public static WebElement help_link;

	@FindBy(xpath="//a[text()='Reference Guides']")
	public static WebElement Referrence_Guides;
	
	@FindBy(xpath="//a[text()='System Requirements']")
	public static WebElement System_Requirements;
	
	@FindBy(xpath="//a[text()='Customer Service']")
	public static WebElement Customer_Service;
	
	@FindBy(xpath="//a[text()='Mercer Job Library Start Guide']")
	public static WebElement MJL_Start_Guide;
	
	@FindBy(xpath="//a[text()='Home']")
	public static WebElement Home;
		
	@FindBy(xpath="//*[@id='btntoc']")
	public static WebElement Content;
	
	@FindBy(xpath="//*[@id='btnidx']")
	public static WebElement Index;
	
	@FindBy(xpath="//*[@id='btnfts']")
	public static WebElement Search;
	
	@FindBy(xpath="//*[@name='keywordField']")
	public static WebElement Search_Box;
	
	@FindBy(xpath="//*[@name='searchString']")
	public static WebElement Search_Box_Above;
	
	@FindBy(xpath="//*[@id='btnhide']")
	public static WebElement Hide_Navigation_Tab;
	
	@FindBy(xpath="//a[text()='The WIN Environment']")
	public static WebElement WIN_Env_link;
	
	@FindBy(xpath="//a[text()='Adding Users to WIN']")
	public static WebElement Add_User_to_WIN_link;
	
	@FindBy(xpath="//title[text()='Mercer WIN Online Help Center']")
	public static WebElement WIN_Title;
		
public Help() {
	PageFactory.initElements(driver, this);
}

public void Help_Link() throws InterruptedException{
	delay(2000);
	waitForElementToDisplay(help_link);
	clickElement(help_link);
}

public void Links() throws InterruptedException{
	waitForElementToDisplay(Referrence_Guides);
	clickElement(Referrence_Guides);
	Thread.sleep(5000);
	waitForElementToDisplay(Home);
	clickElement(Home);
	Thread.sleep(5000);
	waitForElementToDisplay(System_Requirements);
	clickElement(System_Requirements);
	Thread.sleep(5000);
	waitForElementToDisplay(Home);
	clickElement(Home);
	Thread.sleep(5000);
	waitForElementToDisplay(Customer_Service);
	clickElement(Customer_Service);
	Thread.sleep(5000);
	waitForElementToDisplay(Home);
	clickElement(Home);
	Thread.sleep(5000);
	waitForElementToDisplay(MJL_Start_Guide);
	clickElement(MJL_Start_Guide);
	Thread.sleep(5000);
	waitForElementToDisplay(Home);
	clickElement(Home);
}

public void Side_Tab() throws InterruptedException{
	waitForElementToDisplay(Hide_Navigation_Tab);
	clickElement(Hide_Navigation_Tab);
	waitForElementToDisplay(Content);
	clickElement(Content);
	waitForElementToDisplay(Hide_Navigation_Tab);
	clickElement(Hide_Navigation_Tab);
	waitForElementToDisplay(Index);
	clickElement(Index);
	waitForElementToDisplay(Hide_Navigation_Tab);
	clickElement(Hide_Navigation_Tab);
	waitForElementToDisplay(Search);
	clickElement(Search);
}

public void Search_Button() throws InterruptedException{
	waitForElementToDisplay(Search);
	clickElement(Search);
	waitForElementToDisplay(Search_Box);
	clickElement(Search_Box);
	setInput(Search_Box, "The WIN Environment");
	waitForElementToDisplay(WIN_Env_link);
	clickElement(WIN_Env_link);
	waitForElementToDisplay(Search);
	clickElement(Search);
	waitForElementToDisplay(Search_Box_Above);
	clickElement(Search_Box_Above);
	setInput(Search_Box_Above, "Adding Users to WIN");
	waitForElementToDisplay(Add_User_to_WIN_link);
	clickElement(Add_User_to_WIN_link);
}

}

