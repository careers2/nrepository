package pages.WINPages;

import static driverfactory.Driver.*;
import static driverfactory.Driver.driver;

import java.util.Random;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Global_editView {
	@FindBy(xpath="//button[@class='medium action icon edit-view-button edit-flyout']")
	public static WebElement edit_Dropdown;
	
	@FindBy(xpath="//div[@class='flyout-popup tab right popup container']//button")
	public static WebElement edit_button;
	
	@FindBy(xpath="//a[text()='Standard']")
	public WebElement Standard;
    
	@FindBy(xpath="//a[text()='Standard']//following::input[4]")
	public WebElement select_Family;
	
	@FindBy(xpath="//a[text()='Standard']//following::input[5]")
	public WebElement select_familyCode;
	
	@FindBy(xpath="//button[@class='large ok next done-button']")
	public WebElement Done_button;
	
	@FindBy(xpath="//div[@class='dialog-popup popup container']//input[@type='checkbox']")
	public WebElement default_checkbox;
	
	@FindBy(xpath="//div[@class='dialog-popup popup container']//input[@type='text']")
	public WebElement editviewname;

	@FindBy(xpath="//button[@class='ok medium']")
	public WebElement save;
	
	@FindBy(xpath="//div[@class='summary data-grid display-below portal']//div[@class='table-portal' and @style]")
	public static WebElement Gloablresult_page;
	
	Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String viewname="View"+numberAsString;
	
	public Global_editView(){
		PageFactory.initElements(driver, this);
	}
	
	public void editviewbutton()
	{	waitForPageLoad();
		waitForElementToDisplay(Gloablresult_page);
		waitForElementToEnable(edit_Dropdown);
		clickElementUsingJavaScript(driver,edit_Dropdown);
		clickElement(edit_button);
	}
	public void editviewselections() {
		clickElement(Standard);			   	
		clickElement(select_Family);
		clickElement(select_familyCode);
		
	}
	public void edit_view_save()
	{
		waitForElementToDisplay(Done_button);
		clickElement(Done_button);
		clickElement(default_checkbox);
		waitForElementToDisplay(editviewname);
		setInput(editviewname,viewname);
		waitForElementToDisplay(save);
		clickElement(save);
	}

}