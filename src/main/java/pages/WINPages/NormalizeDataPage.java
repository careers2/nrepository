package pages.WINPages;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NormalizeDataPage {
	@FindBy(xpath="//button[@class='report-control-button normalize-data-task']")
	public static WebElement  normalizeData_Tab;
	
	@FindBy(xpath="//input[@id='toggle-data-scaling']")
	public static WebElement nd_Datascaling;
	
	@FindBy(xpath="//select[@id='data-scaling-type']")
	public static WebElement nd_scalingType;
	
	@FindBy(xpath="//input[@id='toggle-convert-currency']")
	public static WebElement nd_convertCurrency;
	
	@FindBy(xpath="//div[@data-purpose='currency-selector']")
	public static  WebElement nd_currencyType;
	
	@FindBy(xpath="//div[@data-selected-currency='AUD']")
	public static WebElement nd_currencySelect;
	
	@FindBy(xpath="//div[@data-purpose='period-selector']")
	public static WebElement nd_periodType;
	
	@FindBy(xpath="//div[@data-average-enum='17' and @data-rate-year='2016']")
	public static WebElement nd_periodselect;
	
	@FindBy(xpath="//button[@data-purpose='calculate']")
	public static WebElement calculate_Button;
	
	@FindBy(xpath="//button[@data-purpose='apply']")
	public static WebElement apply_Button;
	
	@FindBy(xpath="//button[@class='report-control-button normalize-data-task btn-on']//following::div[1]")
	public static WebElement normalizeOn_resultpage;
	
	public NormalizeDataPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void ClicknormalizeData() {
	//Normalize data
	waitForElementToClickable(normalizeData_Tab);
	clickElementUsingJavaScript(driver,normalizeData_Tab);
	}
	
	public void normalize() {
	clickElement(nd_Datascaling);
	selEleByValue(nd_scalingType,"2");
	clickElement(nd_convertCurrency);
	clickElement(nd_currencyType);
	clickElement(nd_currencySelect);
	clickElement(nd_periodType); 
	waitForElementToDisplay(nd_periodselect);
	clickElement(nd_periodselect);
	clickElement(calculate_Button);	
	waitForElementToDisplay(apply_Button);
	clickElementUsingJavaScript(driver,apply_Button);
	System.out.println("applied button");
	
	}
	
}
