package pages.WINPages;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.delay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

public class MyEmpdetails {
	
	@FindBy(xpath="//button[@class='icon medium ok search-button icon submit']")
	public WebElement myempsearch;
	
	@FindBy(xpath="//a[@data-record='0']")
	public WebElement empdetailslink;
    
	@FindBy(xpath="//button[@class='action close-button medium ok']//b[text()='Close']")
	public WebElement empdetailslinkclose;
	
	@FindBy(xpath="//a[@data-record='0' and @data-column='2']")
	public WebElement jobdetails;
	
	@FindBy(xpath="//div[@class='popup bench-job-details-popup full-page has-title size-to-page size-specific']//b[text()='Close']")
	public WebElement jobdetailsclose;
	
	@FindBy(xpath="//b[text()='Market Comparison Graph']")
	public WebElement empdetailsload;
	
	@FindBy(xpath="//b[text()='Market Comparisons']")
	public WebElement jobdetailsload;
	
	Thinking thinking=new Thinking();
	
	public MyEmpdetails(){
		PageFactory.initElements(driver, this);
	}
	
	public void empdetails()
	{
		//waitForPageLoad();
		//thinking.thinking();
		waitForElementToDisplay(myempsearch);
		clickElement(myempsearch);			    
		waitForElementToDisplay(empdetailslink);
		clickElement(empdetailslink);
		//thinking.thinking(;
		waitForElementToEnable(empdetailsload);
		waitForElementToDisplay(empdetailslinkclose);
		//delay(3000);
		
	}
	
	public void empdetailsclose() {
		
		clickElement(empdetailslinkclose);
		
	}
	
	public void jobdetails() {

		waitForElementToDisplay(jobdetails);
		clickElement(jobdetails);
		//delay(8000);
		waitForElementToEnable(jobdetailsload);
		waitForElementToEnable(jobdetailsclose);
		
	}
	public void jobdetailsclose() {
		
		clickElement(jobdetailsclose);
		
	}
}
