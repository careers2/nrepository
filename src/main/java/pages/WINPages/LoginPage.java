package pages.WINPages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;

public class LoginPage {

	@FindBy(id="ctl00_MainSectionContent_Email")
	public static WebElement Email;
 
	@FindBy(id="ctl00_MainSectionContent_Password")
	public static WebElement passwordInput;
	
	@FindBy(id="ctl00_MainSectionContent_ButtonSignin")
	public static WebElement signin_Button;


	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username,String password)
	{
		setInput(Email,username);
		setInput(passwordInput,password);
		clickElement(signin_Button);
		
	}
	
	
}

