package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Employees {
	
	@FindBy(id="feature-menu-link3")
	public WebElement myempl;
	
	@FindBy(xpath="//a[@href='/v2/Benchmarking/EmployeesSearchPage']")
	public static WebElement Myemployees;

	public My_Employees(){
		PageFactory.initElements(driver, this);
	}
	public void Myemployees()
	{
		waitForPageLoad();
		waitForElementToDisplay(myempl);
		clickElement(myempl);
		waitForPageLoad();
	}
}
