package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Jobs_Create_from_Reference_jobs {

	@FindBy(xpath="//button[@class='action icon large manage-jobs-button']//b[text()='Manage Jobs']")
    public static WebElement managejob;
	
	@FindBy(xpath="//button[text()='Create From Reference Jobs']")
	public static  WebElement createfromreferencejobs;
	
	@FindBy(xpath="//div[@class='popup popup-search copy-jobs-referencejob-popup-search has-title size-to-page size-specific']//button[@class='icon medium ok search-button icon submit']")
	public static  WebElement referencejobssearch;
	
	@FindBy(xpath="//div[@class='popup popup-search copy-jobs-referencejob-popup-search has-title size-to-page size-specific']//a[@data-record='0']")
	public static  WebElement referencejobname;
	
	@FindBy(xpath="//div[@class='popup popup-search copy-jobs-referencejob-popup-search has-title size-to-page size-specific']//div[@class='group edit-box keyword-search-criteria']//input")
	public static  WebElement referencejobinput;
	
	@FindBy(xpath="//div[@class='popup popup-search copy-jobs-referencejob-popup-search has-title size-to-page size-specific']//input[@data-record='0']")
	public static  WebElement referencejobselect;
	
	@FindBy(xpath="//button[@class='continuebutton medium next ok']")
	public static  WebElement Done;
	
	@FindBy(xpath="//div[@class='popup size-specific size-to-page span6 create-jobs-popup has-title size-to-fit']//div[@class='group edit-box select-box form-element selector job-organization span3']")
	public static  WebElement Organization;

	@FindBy(xpath="//div[@class='popup select-list create-jobs organizations right tab select has-title size-specific']//a[text()='Unassigned Organization (0)']")
	public static  WebElement Organizationselect;
	
	@FindBy(xpath="//div[@class='group edit-box select-box form-element selector job-country span3']//following::a[1]")
	public static  WebElement Country;
	
	@FindBy(xpath="//div[@class='popup select-list create-jobs countries right tab select has-title size-specific']//a[text()='Global (0)']")
	public  static  WebElement Countryselect;
	
	@FindBy(xpath="//div[@class='form-element selector job-code span3']//following::div")
	public static  WebElement Jobcodeext;
	
	@FindBy(xpath="//div[@class='popup tab combobox-popup right has-title size-to-fit']//div[@data-id='1']//a")
	public static  WebElement jobextselect;
	
	@FindBy(xpath="//button[@class='done-button medium ok']")
	public static  WebElement Create;
	
	@FindBy(xpath="//div[@class='popup create-jobs-confirmation-dialog has-title size-to-fit']//button[@class='action medium ok']")
	public static  WebElement Ok;
	
	@FindBy(xpath="//input[@value='MyJobSearch']")
	public static  WebElement progresspopup;
	
	@FindBy(xpath="//div[@class='popup progress-center-confirmation-popup has-title size-to-fit']//button[@class='action medium ok']")
	public static  WebElement Ok1;
	
	public My_Jobs_Create_from_Reference_jobs(){
		PageFactory.initElements(driver, this);
	}
	
	public void create_from_referenece_jobs()
	{
		
		waitForElementToDisplay(managejob);
		clickElement(managejob);
		waitForElementToDisplay(createfromreferencejobs);
		clickElement(createfromreferencejobs);	
		//delay(6000);
		waitForElementToDisplay(referencejobinput);
		setInput(referencejobinput,"Chairman");
		waitForElementToDisplay(referencejobssearch);
		clickElement(referencejobssearch);
		waitForElementToDisplay(referencejobselect);
		clickElement(referencejobselect);
		waitForElementToDisplay(Done);
		clickElement(Done);
		waitForElementToDisplay(Organization);
		clickElement(Organization);
		waitForElementToDisplay(Organizationselect);
		clickElement(Organizationselect);
		waitForElementToDisplay(Country);
		clickElement(Country);
		waitForElementToDisplay(Countryselect);
		clickElement(Countryselect);			    
		waitForElementToDisplay(Jobcodeext);
		clickElement(Jobcodeext);
		waitForElementToDisplay(jobextselect);
		clickElement(jobextselect);
		waitForElementToDisplay(Create);
		clickElement(Create);
		waitForElementToDisplay(Ok);
		clickElement(Ok);	
		waitForElementToDisplay(progresspopup);
		clickElement(progresspopup);
		waitForElementToDisplay(Ok1);
		clickElement(Ok1);
		delay(3000);
		
	}
}
