package pages.WINPages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Thinking {

	@FindBy(xpath="//div[@class='big-win-common-thinking']")
    public static By thinking;
	
	public Thinking(){
		PageFactory.initElements(driver, this);
	}
	public void thinking()
	{
		if(((WebElement) thinking)!=null)
		{
		new WebDriverWait(driver,10).until(ExpectedConditions.invisibilityOfElementLocated(thinking));
		}
		else
		{
		System.out.println("Next Step");
		}
	}
}
