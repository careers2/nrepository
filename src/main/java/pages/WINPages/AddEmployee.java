package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.Random;

public class AddEmployee {
	@FindBy(xpath="//button[@class='action add-button icon large']")
	public WebElement addemp;
	
	@FindBy(xpath="//input[@data-property='Name']")
	public WebElement addempname;
	
	@FindBy(xpath="//input[@data-property='EmployeeID']")
	public WebElement addempiden;
	
	@FindBy(xpath="//div[@class='combobox form-value span3']//div[@class='body']")
	public WebElement myempjobtwisty;
	
	@FindBy(xpath="//button[@class='icon medium ok search-button']")
	public WebElement myempjobsearch;
	
	@FindBy(xpath="//div[@class='even level0 list-item']//following::a[1]")
	public WebElement myempjob;
	
	@FindBy(xpath="//b[text()='Save']")
	public WebElement addempSave;
	
	@FindBy(xpath="//button[@class='action close-button medium ok']//following::b")
	public WebElement addempclose;
	
	@FindBy(xpath="//b[text()='Market Comparison Graph']")
	public WebElement empdetailsload;
	
	@FindBy(xpath="//b[text()='Market Comparison Table']")
	public WebElement empdetailsloads;
	
	@FindBy(xpath="//div[@class='popup bench-employee-details-popup full-page has-title size-to-page size-specific']//div[@data-form-item-property='Name']//div[@class='form-value read-only span3']")
	public WebElement employeename;
	
	Thinking thinking=new Thinking();
	
	Random rg = new Random();	
	int randomInt = rg.nextInt(100000);	
	String numberAsString = Integer.toString(randomInt);	
	public String empname="EMP"+numberAsString;
	public String empiden=numberAsString;
	
	public AddEmployee() {
		PageFactory.initElements(driver, this);
	}
	
	public void addemp() throws InterruptedException
	{
		waitForPageLoad();
		//delay(3000);
		waitForElementToDisplay(addemp);
		clickElement(addemp);
		//delay(5000);
		waitForElementToDisplay(addempname);
		setInput(addempname,empname);
		waitForElementToDisplay(addempiden);
		setInput(addempiden,empiden);
		waitForElementToDisplay(myempjobtwisty);
		clickElement(myempjobtwisty);
		waitForElementToDisplay(myempjobsearch);
		clickElement(myempjobsearch);
		waitForElementToDisplay(myempjob);
		clickElement(myempjob);
		waitForElementToDisplay(addempSave);
		clickElement(addempSave);
		waitForElementToEnable(empdetailsload);
		waitForElementToEnable(empdetailsload);
		waitForElementToEnable(empdetailsloads);
		waitForElementToDisplay(employeename);
		//delay(3000);
	}
	public void addempclose() {
		//delay(3000);
		waitForElementToEnable(addempclose);
		clickElement(addempclose);
	}

}
