package pages.WINPages;

import static driverfactory.Driver.driver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.InitTests;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.setInput;


public class User_Profile {
	
	@FindBy(xpath="//*[@name='FirstName']")
	public static WebElement First_Name;
	
	@FindBy(xpath="//*[@name='LastName']")
	public static WebElement Last_Name;
	
	@FindBy(xpath="//*[@name='Company']")
	public static WebElement Company;
	
	@FindBy(xpath="//*[@name='Address1']")
	public static WebElement Address_1;
	
	@FindBy(xpath="//*[@name='Address2']")
	public static WebElement Address_2;
	
	@FindBy(xpath="//*[@name='City']")
	public static WebElement City;
	
	@FindBy(xpath="//*[@name='State']")
	public static WebElement Province;
	
	@FindBy(xpath="//*[@name='PostalCode']")
	public static WebElement PostalCode;
	
	@FindBy(xpath="//*[@name='Phone']")
	public static WebElement Phone;
	
	@FindBy(xpath="//*[@name='CurrentPassword']")
	public static WebElement CurrentPassword;
	
	@FindBy(xpath="//*[@name='NewPassword']")
	public static WebElement NewPassword;
	
	@FindBy(xpath="//*[@name='ConfirmPassword']")
	public static WebElement ConfirmPassword;
	
	@FindBy(xpath="//b[text()='Change Password']")
	public static WebElement Confirm_Button;
	
	@FindBy(xpath="//b[text()='Save']")
	public static WebElement Save_Button;
	
	@FindBy(xpath="//b[text()='Done']")
	public static WebElement Done_Button;
	
	@FindBy(xpath="//b[text()='Cancel']")
	public static WebElement Cancel_Button;
	
	@FindBy(xpath="//div[@class='overlay']")
	public static WebElement Language_Drop_Down;
	
	@FindBy(xpath="//*[text()='Portugu�s (Portuguese-Brazil)']")
	public static WebElement Portuguese_Brazil;
	
	@FindBy(xpath="//*[text()='English (English)']")
	public static WebElement English;
	
	@FindBy(xpath="//*[@id=\"buttonSave\"]/div/b")
	public static WebElement Portuguese_Brazil_Button;
	
	public User_Profile() {
		PageFactory.initElements(driver, this);
	}
	
	public void Edit_User() throws InterruptedException{
		waitForElementToDisplay(Address_1);
		clickElement(Address_1);
		setInput(Address_1, "Opposite Satya Sai Hospital, ITPL Main Road");
		waitForElementToDisplay(Address_2);
		clickElement(Address_2);
		setInput(Address_2, "Gopalan Global Axis, Brookefield");
		waitForElementToDisplay(City);
		clickElement(City);
		setInput(City, "Bangalore");
		waitForElementToDisplay(Province);
		clickElement(Province);
		setInput(Province, "Karnataka");
		waitForElementToDisplay(PostalCode);
		clickElement(PostalCode);
		setInput(PostalCode, "560066");
		waitForElementToDisplay(Phone);
		clickElement(Phone);
		setInput(Phone, "08061011061");
		waitForElementToDisplay(Save_Button);
		clickElement(Save_Button);
	}
	
	public void Change_Language_Portu_Brazil() throws InterruptedException{
	    
		waitForElementToDisplay(Language_Drop_Down);
		clickElement(Language_Drop_Down);
		waitForElementToDisplay(Portuguese_Brazil);
		clickElement(Portuguese_Brazil);
		waitForElementToDisplay(Save_Button);
		clickElement(Save_Button);
		waitForElementToDisplay(Portuguese_Brazil_Button);
	}
	
	public void Change_Language_English() throws InterruptedException{
		waitForElementToDisplay(Language_Drop_Down);
		clickElement(Language_Drop_Down);
		waitForElementToDisplay(English);
		clickElement(English);
		waitForElementToDisplay(Portuguese_Brazil_Button);
		clickElement(Portuguese_Brazil_Button);
		waitForElementToDisplay(Save_Button);
	}
}
