package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class Manage_IPE_Settings {
	
	@FindBy(xpath="//button[@class='medium action ipe-button']")
	public static WebElement IPEButton;
	
	@FindBy(xpath="//div[@class='popup admin-ipe-settings has-title size-to-page size-specific']")
	public static WebElement IPESettingspopup;

	@FindBy(xpath="//button[@data-id='configurations']")
	public static WebElement Configtab;		
	
	@FindBy(xpath="//button[@class='action close-button medium ok']")
	public static WebElement Close;
	

	public Manage_IPE_Settings() {
		PageFactory.initElements(driver, this);
	}
	public void ipe() throws InterruptedException
	{	
		waitForElementToDisplay(IPEButton);
		clickElement(IPEButton);
	}
	public void ipepopup() {
		waitForElementToDisplay(Configtab);
		clickElement(Configtab);
		waitForElementToDisplay(Close);
		clickElement(Close);
	}

}
