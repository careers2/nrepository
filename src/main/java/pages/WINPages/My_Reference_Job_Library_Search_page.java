package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToEnable;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Reference_Job_Library_Search_page {

	@FindBy(xpath="//div[@id='feature-menu-button4']")
    public static WebElement mymarketlibrary;
	
	@FindBy(xpath="//a[@alt='My Reference Job Library']")
	public static  WebElement myreferencejoblibrary;
	
	@FindBy(xpath="//div[@id='page-title']//h1")
	public static WebElement MyReferenceJobLibrary;
	
	public My_Reference_Job_Library_Search_page(){
		PageFactory.initElements(driver, this);
	}
	
	public void reference_jobs()
	{
		waitForElementToEnable(mymarketlibrary);
		clickElement(mymarketlibrary);
		waitForElementToEnable(myreferencejoblibrary);
		clickElement(myreferencejoblibrary);
		waitForPageLoad();
	}
}
