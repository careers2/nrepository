package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class Manage_User_Profiles {
	
	@FindBy(xpath="//div/b[contains(., 'Manage User Profiles')]")
	public static WebElement ManageUserProfiles;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal']//div[@class='scroll-size']//tr[1]//child::a[1]")
	public static WebElement link;
	
	@FindBy(xpath="//button[@class='medium ok save-button']")
	public static WebElement Done;
	
	@FindBy(xpath="//div[@id='breadcrumb']//a[@href='/v2/Admin/AdminHomePage']")
	public static WebElement Administration;
	
	@FindBy(xpath= "//button[@class='medium action eval-appr-button']")
	public static WebElement ManageEvaluationApprovals;
	
	@FindBy(xpath="//button[@class='medium action profile-button' and @data-purpose='users-report']")
	public static WebElement UserReports;
	
	@FindBy(xpath="//a[@href='/v2/Admin/UserProfileAdmin']")
	public static WebElement ManageUserProfile;
	
	public Manage_User_Profiles()
	{
		PageFactory.initElements(driver, this);
		
	}

	public void Profiles()
	{
		waitForElementToDisplay(ManageUserProfiles);
		clickElement(ManageUserProfiles);
	}
	public void Profilelink() {
		waitForElementToDisplay(link);
		clickElement(link);
		waitForElementToDisplay(Done);
		clickElement(Done);
		waitForElementToDisplay(Administration);
		clickElement(Administration);
		
	}

}
