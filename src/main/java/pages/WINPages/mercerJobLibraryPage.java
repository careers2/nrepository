package pages.WINPages;

import static driverfactory.Driver.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class mercerJobLibraryPage {
	
	@FindBy(xpath="//button[@class='dropdown-target select-view-button']")
	public static WebElement view_Dropdown;
	
	@FindBy(xpath="//button[@dataview-id='0c242c38-28d5-446c-9b51-5726e8731d4a']")
	public static WebElement select_JobView;
	
	@FindBy(xpath="//button[@dataview-id='8c20a9e8-9020-406e-a2ef-360893e8b017']")
	public static WebElement select_PCView;
	
	@FindBy(xpath="//button[@data-id='mv']")
	public  static WebElement marketViewFilter;
	
	@FindBy(xpath="//li[@data-id='EU']")
	public static WebElement click_EMEA;
	
	@FindBy(id="EU.BE.15130219-ebc9-e511-86d5-002481adb66c")
	public static WebElement select_parentMv;
	
	@FindBy(id="EU.BE.b58a0282-ecc9-e511-86d5-002481adb66c")
	public static WebElement select_childMv;
	
	@FindBy(id="EU.BE.d44cec69-07cc-e511-86d5-002481adb66c")
	public static WebElement select_mvMJC1 ;
	
	@FindBy(id="EU.DK.f559409a-00cc-e511-86d5-002481adb66c")
	public static WebElement select_mvDETRS;
	
	@FindBy(xpath="//button[@class='apply-button']")
	public static WebElement mvApply_Button;
	
	@FindBy(xpath="//button[@data-purpose='search-submit']")
	public static WebElement search_Button;
	
	@FindBy(xpath="//div[@class='data-grid display-below portal']//input[@data-record='-1']")
	public static WebElement select_allJob;
	
	@FindBy(xpath="//button[@data-id='searchContinueButton']")
	public static WebElement continue_Button;
	
	@FindBy(xpath="//span[@class='current-view result']")
	public static WebElement currentViewResult;
	
	@FindBy(xpath="//div[@class='report-model-title']")
	public static WebElement result_Page;
	
	@FindBy(xpath="//button[@data-purpose='show-option-detail' and @data-id='f']")
	public static WebElement familyFlyout;

	@FindBy(xpath="//li[@data-id='HRM']//input")
	public static WebElement select_HumanResources;
	
	@FindBy(xpath="(//div[@class='data-grid display-below portal']//div[@class='body']//tbody//tr[@class='odd'])[1]")
	public static WebElement mjl_tableresult;
	
	@FindBy(xpath="//div[@class='data-grid mjl-results-grid display-below portal']//div[@class='table-portal' and @style]")
	public  WebElement admin_tableresult;
	
	public mercerJobLibraryPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void jobView() throws InterruptedException {
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		scrollToElement(driver,select_JobView);
		clickElement(select_JobView);
		mvFilter(select_parentMv , select_childMv);
		waitForElementToClickable(mjl_tableresult);
		waitForElementToClickable(select_allJob);
		clickElementUsingJavaScript(driver,select_allJob);
		System.out.println("Jobs selected");
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);		
	}	
		
	public void mvFilter (WebElement ele1 , WebElement ele2 ) {
		waitForElementToClickable(marketViewFilter);
		clickElementUsingJavaScript(driver,marketViewFilter);
		System.out.println("market view selected");
		clickElement(click_EMEA);
		clickElement(ele1);
		clickElement(ele2);
		clickElement(mvApply_Button);
		clickElement(search_Button);
	}
		
	
	public void navigateJobView() throws InterruptedException {
		waitForPageLoad();
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		scrollToElement(driver,select_JobView);
		clickElement(select_JobView);
		waitForElementToClickable(search_Button);
		clickElementUsingJavaScript(driver,search_Button);
		//Thinking.loading();
		delay(4000);
		waitForElementToDisplay(mjl_tableresult);
		waitForElementToClickable(select_allJob);
		clickElementUsingJavaScript(driver,select_allJob);
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);
	}
	
	public void pcView() {
		System.out.println("mercer Job Library-PC View");
		waitForElementToClickable(view_Dropdown);
		clickElement(view_Dropdown);
		clickElement(select_PCView);
		mvFilter(select_mvMJC1 , select_mvDETRS);
		waitForElementToClickable(continue_Button);
		clickElementUsingJavaScript(driver,continue_Button);
	}
	
	
	public void admin_mjl() {
		waitForElementToClickable(familyFlyout);
		clickElementUsingJavaScript(driver,familyFlyout);
		waitForElementToClickable(select_HumanResources);
		clickElementUsingJavaScript(driver,select_HumanResources);
		clickElement(mvApply_Button);
		clickElement(search_Button);
		waitForElementToClickable(continue_Button);
		clickElement(continue_Button);
	}
}
