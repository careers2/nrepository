package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.getTextFromWebElementUsingJavaScript;
import static driverfactory.Driver.getElementWithTitle;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForElementToEnable;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.delay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class My_Reference_Job_Library_Resultspage {
	
	@FindBy(xpath="//input[@data-record='-1']")
	public static  WebElement resultsselect;
	
    @FindBy(xpath="//button[@class='large ok next continue-button']")
    public static WebElement Continue_to_reults;
    
    @FindBy(xpath="//table[@class='data-table']//a[@data-record='0' and @data-column='0']")
	public static  WebElement detailspopup;

	@FindBy(xpath="//button[@class='large save-as-button']")
	public static WebElement saveas;
	    
	@FindBy(xpath = "//div[@class='save-as-dialog']//input[@type='text']")
	public static WebElement saveasinput;
	   
	@FindBy(xpath="//button[@class='action medium ok']")
	public static WebElement Save;
	    
	@FindBy(xpath="//div[@class='group edit-box select-box my-saved-results']//span[text()='My Saved Results']")
	public static WebElement resultset;
	    
	@FindBy(xpath = "//div[@class='list-item  data']//following::a[1]")
	public static WebElement firstresultset;

	@FindBy(xpath="//div[@id='page-title']//h1")
	public static WebElement MyReferenceJobLibraryresults;
	
	@FindBy(xpath="//b[text()='Continue']")
	public WebElement Continue;
	
	@FindBy(xpath="//a[@href='/v2/Benchmarking/ReferenceJobsResultsPage']")
	public static WebElement pagetitle;
	
	@FindBy(xpath="//div[text()='1']")
	public static WebElement set;
	
	@FindBy(xpath="//div[@class='popup my-saved-results-dropdown tab select right has-title size-specific']//following::a[1]//child::div[1]")
	public static WebElement firstresultsettext;
	
	public String resultsetname="1";
	public static String s1="";
	
	My_Reference_Job_Library_details details=new My_Reference_Job_Library_details();
	Thinking think=new Thinking();
	
	public My_Reference_Job_Library_Resultspage (){
		PageFactory.initElements(driver, this);
	}
	
	public void referenece_jobs_results_page()
	{
		waitForElementToEnable(resultsselect);
		clickElement(resultsselect);
		waitForElementToEnable(Continue_to_reults);
		clickElement(Continue_to_reults);
		waitForPageLoad();
		waitForElementToEnable(detailspopup);
		clickElement(detailspopup);
		waitForElementToEnable(details.detailspopupload);
		waitForElementToEnable(details.popupclose);
	}
	
	public void detailsclose()
	{
		
		clickElement(details.popupclose);
	}
	public void save()
	{
		waitForElementToEnable(saveas);
		clickElement(saveas);
		waitForElementToEnable(saveasinput);
		setInput(saveasinput,resultsetname);	
		waitForElementToEnable(Save);
		clickElement(Save);
		waitForElementToEnable(Continue);
		clickElement(Continue);
		//delay(3000);
		waitForElementToEnable(detailspopup);
		waitForElementToEnable(pagetitle);
	}
	
	public void resultset() {
		waitForElementToEnable(resultset);
		clickElement(resultset);
		WebElement resultsetaccess=getElementWithTitle("a",resultsetname);
		clickElement(resultsetaccess);
		//delay(4000);
		waitForElementToEnable(detailspopup);
		waitForElementToEnable(pagetitle);
		
	}

}
