package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MUPCS_Catalog {
	
	@FindBy(xpath="//button[@class='medium action mupcs-button']")
	public static WebElement MUPCSCatalog;
	
	@FindBy(xpath="//a[@href='/v2/Benchmarking/MupcsSearchPage']")
	public static WebElement MUPCSCatalogpage;
	
	@FindBy(xpath="//button[@class='medium open-criteria-button open-popup']//b[text()='Mercer Job Family/Sub-Family']")
	public static WebElement Familyfilter;

	@FindBy(xpath="//div[@class='popup search-criteria-popup anchored-popup size-to-fit']//div[@class='sub-level-group selected']//div[@class='sub-level-all']//child::input[1]")
	public static WebElement checkbox;		
	
	@FindBy(xpath="//div[@class='popup search-criteria-popup anchored-popup size-to-fit']//button[@class='action-button apply-button medium ok']")
	public static WebElement Apply;
	
	@FindBy(xpath="//button[@class='icon medium ok search-button icon submit']")
	public static WebElement Search;	
	
	public MUPCS_Catalog() {
		PageFactory.initElements(driver, this);
	}
	public void mupcscatalog() throws InterruptedException
	{	
		waitForElementToDisplay(MUPCSCatalog);
		clickElement(MUPCSCatalog);
		
	}
	public void mupcscatalogpage() throws InterruptedException
	{
		waitForElementToDisplay(Familyfilter);
		clickElement(Familyfilter);
		waitForElementToDisplay(checkbox);
		clickElement(checkbox);
		waitForElementToDisplay(Apply);
		clickElement(Apply);
		waitForElementToDisplay(Search);
		clickElement(Search);
	}
}
