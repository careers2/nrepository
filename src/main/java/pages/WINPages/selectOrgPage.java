package pages.WINPages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class selectOrgPage {

	@FindBy(xpath="//*[contains(text(),'win [win123]')]")
	public static WebElement Select_Org;
	
	@FindBy(id="doneButton")
	public static WebElement continue_Button;
	
	public selectOrgPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void Organization()
	{
		waitForElementToDisplay(Select_Org);
		clickElement(Select_Org);
		clickElement(continue_Button);
	}
	
}
