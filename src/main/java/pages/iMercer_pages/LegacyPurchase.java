package pages.iMercer_pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;

public class LegacyPurchase {
	@FindBy (id = "ctl03_imgBuyNow")
	WebElement BuyNowButton;
	@FindBy(id = "ctl03_po_btnAdd2")
	WebElement AddToCartButton;
	@FindBy (id = "ctl03_txtTitle")
	WebElement TitleInput;
	@FindBy (id = "ctl03_imgBtnContinue")
	WebElement ContinueButton;
	@FindBy (id = "ctl03_imgBtnCheckout") 
	WebElement CheckoutButton;
	@FindBy(id = "ctl03_lblHeader")
	public WebElement BillingInfoHeader;
	@FindBy (id = "ctl03_txtInfo")
	WebElement CommentBox;
	@FindBy (id = "ctl03_btnContinue2")
	WebElement ContinueButton2;
	@FindBy (id = "ctl03_uxInvoiceImageButton")
	WebElement InvoiceButton;
	@FindBy (id = "ctl03_lblHeader")
	public WebElement OrderReceipt;
	@FindBy(id = "ctl03_osOrder_pnlInvoiceDetails")
	public WebElement OrderSummary;
	@FindBy(xpath ="//span[text()='Order/Payment Information']//following::span[3]")
	public WebElement OrderNumber;
	@FindBy(id = "ctl03_ddlCountry")
	WebElement BillingCountry;
	
	public LegacyPurchase() {
		PageFactory.initElements(driver, this);
		
	}
	public void addToCart() throws InterruptedException {
		clickElement(BuyNowButton);
		clickElement(AddToCartButton);
		setInput(TitleInput,"Testing");
		clickElement(ContinueButton);
		clickElement(CheckoutButton);
		
	}
	public void checkout() throws InterruptedException {
		selEleByVisbleText(BillingCountry,"Chile");
		//setInput(CommentBox,"Testing");
		clickElementUsingJavaScript(driver,CheckoutButton);
		clickElementUsingJavaScript(driver,ContinueButton2);
		clickElementUsingJavaScript(driver,ContinueButton2);
		clickElement(InvoiceButton);
	}
	
}
