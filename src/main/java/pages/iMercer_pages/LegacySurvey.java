package pages.iMercer_pages;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;

public class LegacySurvey {
	
	@FindBy (id = "ctl03_lnkProducts")
	WebElement AToZProductListingLink;
	@FindBy (id ="ctl03_lblGroupName")
	public WebElement SurveyListingPageHeader; 
	@FindBy (id = "ctl03_repSurveys_ctl04_lnkSurvey")
	WebElement SportsSurveyVijay;
	@FindBy (id = "ctl03_lblSurveyName")
	public WebElement SurveyPageHeader;
	

	public LegacySurvey() {
		PageFactory.initElements(driver, this);
		
	}
	public void selectSurvey(String survey) throws InterruptedException {
		clickElementUsingJavaScript(driver,AToZProductListingLink);
		clickElement(driver.findElement(By.xpath("//a[text()='"+survey+"']")));
	}
	
}
