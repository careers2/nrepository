package pages.iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.iMercer_pages.MFA;

import static driverfactory.Driver.*;

public class Login {

@FindBy (id = "ctl01_lblWelcome")
public WebElement WelcomeToiMercer;
@FindBy (id ="lblSelectedRegion")
public WebElement RegionBanner;
@FindBy (id = "myaccounts")
WebElement MyAccountsLink;
@FindBy (id ="Header1_iMercerHeader_iMercerMiniHeader_hyperlnkSignIn")
WebElement SignInButtonToLoginPage;
@FindBy( id = "ctl00_MainSectionContent_Email")
WebElement EmailIdInput;
@FindBy (id ="ctl00_MainSectionContent_Password")
WebElement PasswordInput;
@FindBy( id ="ctl00_MainSectionContent_ButtonSignin")
WebElement SignInButton;
@FindBy (id= "welcomeWithName")
public WebElement LoggedInUserName;

public Login() {
	PageFactory.initElements(driver, this);
}

public void regionSelection(String region) {
	clickElement(driver.findElement(By.partialLinkText(region)));
}
public void login(String username,String password) throws InterruptedException {
	hoverOverElement(driver,MyAccountsLink);
	clickElement(SignInButtonToLoginPage);
	setInput(EmailIdInput,username);
	setInput(PasswordInput,password);
	clickElement(SignInButton);
	delay(5000);
	if (driver.getPageSource().contains("Contact Us for Help")) {
		navigateBack(driver);
		setInput(PasswordInput, password + Keys.ENTER);
		delay(5000);
	}
	MFA mfa = new MFA();
	mfa.authenticate();
	delay(6000);
}

}
