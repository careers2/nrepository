package pages.iMercer_pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class DNNsiteSurveys {
@FindBy (id = "changeLocation")
WebElement ChangeLocationDropdown;
@FindBy (id = "lblSelectedRegion")
public WebElement LocationLabel;
@FindBy (id = "Header1_Repeater1_ctl01_lnkTabName")
WebElement RewardsMenu;
@FindBy(xpath = "//a[contains(text(),'Surveys & Reports')]")
WebElement SurveysAndReportsOption;
@FindBy (xpath ="//span[contains(text(),'SURVEYS AND REPORTS')]")
public WebElement SurveysAndReportsPageHeader;
@FindBy(id = "Span_compensation")
WebElement ExpandCompensation;
@FindBy (id = "Span_us-compensation")
WebElement ExpandUS;
@FindBy( id ="Span_department-function")
WebElement ExpandDepartmentFunction;
@FindBy (id ="Div_engineering-2")
WebElement DepartmentFunction_Engineering;
@FindBy(xpath ="//a[text()='Pre_Testing_HC']")
WebElement PretestingHC;
@FindBy(css = ".SurveyTitle")
public WebElement SurveyTitle;
//@FindBy(xpath = "//div[contains(@class,'category')]//a[(text()='Benefits')]")
//div[contains(@class,'category')]//a[(text()='Benefits Survey Asia Pacific')]


public DNNsiteSurveys() {
	PageFactory.initElements(driver, this);
}
public void changeLocation(String region) throws InterruptedException {
	hoverOverElement(driver,ChangeLocationDropdown);
	clickElement(driver.findElement(By.partialLinkText(region)));
	waitForElementToDisplay(LocationLabel);
	
}
public void openSurveysAndReportsPage() throws InterruptedException {
	hoverOverElement(driver,RewardsMenu);
	clickElement(SurveysAndReportsOption);
}
public void selectSurvey(String menu,String submenu1,String submenu2,String submenu3,String survey) throws InterruptedException {
	clickElement(driver.findElement(By.xpath("//a[text()='"+menu+"']//following::div[1]")));
	clickElement(driver.findElement(By.xpath("//li[contains(@id,'"+menu.toLowerCase()+"')]//a[text()='"+submenu1+"']//following::div[1]")));
	clickElement(driver.findElement(By.xpath("//li[contains(@id,'"+menu.toLowerCase()+"')]//a[text()='"+submenu2+"']//following::div[1]")));
	clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//li[contains(@id,'"+menu.toLowerCase()+"')]//a[text()='"+submenu3+"']")));
	clickElementUsingJavaScript(driver,driver.findElement(By.xpath("//a[text()='"+survey+"']")));
}
public void selectSurvey(String menu,String submenu) {
	clickElement(driver.findElement(By.xpath("//div[contains(@class,'category')]//a[(text()='"+menu+"')]")));
	clickElement(driver.findElement(By.xpath("//div[contains(@class,'category')]//a[(text()='"+submenu+"')]")));
}


}
