package pages.iMercer_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.*;

public class DNNsitePurchase {
	@FindBy (id = "hcCCUserslnk")
	WebElement AddToCartButton;
	@FindBy (xpath = "//th[contains(text(),'SHOPPING CART')]")
	public WebElement CartPageHeader;
	@FindBy (id="ShoppingCartCheckoutBtn")
	WebElement CheckOutButton;
	@FindBy (id = "specialinstructions")
	WebElement CommentBox;
	@FindBy (id = "openpayment")
	WebElement OpenPayment;
	@FindBy( xpath ="//input[@value='Invoice']")
	WebElement InvoiceRadioButton;
	@FindBy(id = "openorderdetail")
	WebElement OpenOrderDetail;
	@FindBy(id = "hcConfirmButton")
	WebElement SubmitOrderButton;
	@FindBy(xpath ="//span[text()='ORDER/PAYMENT INFORMATION']//following::span[2]")
	public WebElement OrderNumber;
	@FindBy(id = "divOrderReceipt")
	public WebElement OrderReceipt;
	@FindBy(id = "invoiceProducts")
	public WebElement OrderSummary;
	
	public DNNsitePurchase() {
		PageFactory.initElements(driver, this);
	}
	public void addToCart() throws InterruptedException {
		clickElementUsingJavaScript(driver,AddToCartButton);
	}
	public void checkout() throws InterruptedException {
		clickElement(CheckOutButton);
		setInput(CommentBox,"Testing");
		clickElement(OpenPayment);
		clickElement(InvoiceRadioButton);
		clickElementUsingJavaScript(driver,OpenOrderDetail);
		clickElementUsingJavaScript(driver,SubmitOrderButton);
		System.out.println("Exec complete");
		
	}
}
