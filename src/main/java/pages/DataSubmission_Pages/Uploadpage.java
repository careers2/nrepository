package pages.DataSubmission_Pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Uploadpage {
	@FindBy(id = "_titleLabel")
	public static WebElement surveyTitle;
	
	@FindBy(id = "_regionLabel")
	public static WebElement RegionLabel;
	
	@FindBy(id = "_languageLabel")
	public static WebElement languageLabel;
	
	@FindBy(id = "_regionDropDownList")
	public static WebElement regionDropDown;
	
	@FindBy(id = "_languageDropDownList")
	public static WebElement languageDropDown;

	
	@FindBy(id = "_emailInstructionsTextBox")
	public static WebElement Instructiontextbox;
	
	@FindBy(id = "_emailTextBox")
	public static WebElement emailtextbox;
	
	@FindBy(id = "_fileUpload")
	public static WebElement Choosefile;
	
	@FindBy(id = "_uploadButton")
	public static WebElement Uploadbutton;
	
	@FindBy(xpath = "//h4[contains(text(),'Your file file.xls was uploaded successfully.')]")
	public static WebElement successMgs;
	
	
		
	public Uploadpage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void uploadFile()
	{
		setInput(Instructiontextbox,"text");
		setInput(emailtextbox,"vignesh.g@mercer.com");
		Choosefile.sendKeys("C:\\Users\\ananya-palled\\Desktop\\file.xls");
		clickElement(Uploadbutton);
		
	}
	}

