package pages.BelongNBA_Pages;

import static driverfactory.Driver.driver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ContactsPage {
	
	@FindBy(xpath ="//h2[contains(text(),'Contacts')]")
	public static WebElement Contactheading;
	
	
	public ContactsPage() {
		PageFactory.initElements(driver, this);
	}

}
