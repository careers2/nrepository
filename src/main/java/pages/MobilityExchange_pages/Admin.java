package pages.MobilityExchange_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.hoverOverElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.switchToFrame;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Admin {
	@FindBy (xpath = "//label[contains(text(),'You are signed in as:')]")
	WebElement Dashboard;
	@FindBy (id = "personaBar-iframe")
	WebElement DNNToolbar;
	@FindBy (id = "Manage")
	WebElement ManageTab;
	@FindBy( id = "Link_0_90")
	WebElement MercerOption;
	@FindBy (xpath = "//h3[contains(text(),'Product Catalog Management')]")
	WebElement ProductCatalogManagement;
	@FindBy (css = "[name=toggleImportProcess]")
	public WebElement ProductCatalogCreationButton ;
	@FindBy(id = "btnTogglePublishUnPublishProcess")
	public WebElement PublishUnpublishButton;
	@FindBy(id = "virtUpdateProductDescription")
	public WebElement UpdateProductDescriptionsButton;
	
	public Admin() {
		PageFactory.initElements(driver, this);
	}
	
		public void adminPage() {
		waitForElementToDisplay(Dashboard);
		waitForElementToDisplay(DNNToolbar);
		switchToFrame(driver,DNNToolbar);
		hoverOverElement(driver,ManageTab);
		clickElement(MercerOption);
		clickElement(ProductCatalogManagement);
		
		
		
	}

}
