package pages.MobilityExchange_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Logout {

	@FindBy(css = ".btn-group.quick-links")
	WebElement MyAccountButton;
	@FindBy(linkText = "Logout")
	WebElement LogoutButton;
	@FindBy(linkText = "Are you an expatriate? Please take a short survey")
	public WebElement HomePageSurvey;
	
	
	public Logout() {
		PageFactory.initElements(driver, this);
	}
	
	public void logout() {
		clickElement(MyAccountButton);
		clickElement(LogoutButton);
		waitForElementToDisplay(HomePageSurvey);
	}
}
