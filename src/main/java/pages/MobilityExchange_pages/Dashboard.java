package pages.MobilityExchange_pages;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.delay;
import static driverfactory.Driver.driver;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.switchToWindow;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Dashboard {

	@FindBy (xpath = "//*[@class='app-title']")
	public	WebElement GHRMApplicationName;
	@FindBy (xpath= "//*[@class='selected']//child::em")
	public WebElement ICSApplicationName;
	public boolean purchased;
	public boolean available;
	@FindBy (css = "li.tab:nth-child(2)")
	WebElement MyDataTab;
	@FindBy (xpath = "//span[contains(text(),'Please select a product')]")
	WebElement SelectProductDropdown;
	@FindBy (css = "li.product:nth-child(2)")
	WebElement PurchasedProducts;
	@FindBy (css = "li.item-to-buy:nth-child(1) > div:nth-child(1) > span:nth-child(2)")
	WebElement AvailableForPurchase;
	
	public Dashboard() {
		PageFactory.initElements(driver, this)	;
		}
		public void openLink(String link) {
			clickElement(driver.findElement(By.partialLinkText(link)));
			int i = driver.getWindowHandles().size();
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			driver.switchTo().window(tabs.get(i-1));
			delay(10000);		
		}
		public void MyDataTab(String product) {
			switchToWindow("Dashboard");
			clickElement(MyDataTab);
			clickElement(SelectProductDropdown);
			clickElement(driver.findElement(By.partialLinkText(product)));
			delay(10000);
			purchased= isElementExisting(driver,PurchasedProducts,40);
			available = isElementExisting(driver,AvailableForPurchase,40);
			
		}
}
