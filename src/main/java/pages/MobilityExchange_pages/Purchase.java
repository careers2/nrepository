package pages.MobilityExchange_pages;

import static driverfactory.Driver.*;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Purchase {
@FindBy(linkText = "Shop")
WebElement ShopTab;
@FindBy(linkText = "Product Catalog")
WebElement ProductCatalogOption;
@FindBy (css = "//span[contains(text(),'Product Catalog')]")
public WebElement ProductCatalogBreadcrumb;
@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][1]")
WebElement SelectProduct1;
@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][2]")
WebElement SelectProduct2;
@FindBy (xpath = "//table[@id='searchGrid']//following::td[@class='buyCol_D'][3]")
WebElement SelectProduct3;
@FindBy (xpath = "//a[contains(text(),'add to cart')]")
WebElement SelectToAddToCartButton;
@FindBy (xpath = "//a[contains(text(),'Keep Shopping')]")
WebElement KeepShoppingButton;
@FindBy (xpath = "//input[@value='Secure Checkout']")
public WebElement SecureCheckoutButton;
@FindBy (xpath = "//h3[contains(text(),'Billing Address ')]")
WebElement BillingAddressSection;
@FindBy (id = "billingfirstname")
WebElement BillingFirstName;
@FindBy (id = "billinglastname")
WebElement BillingLastName;
@FindBy (id = "billingaddress")
WebElement BillingAddress;
@FindBy (id = "billingstate")
WebElement BillingState;
@FindBy (id = "billingcity")
WebElement BillingCity;
@FindBy (id = "billingzip")
WebElement BillingZip;
@FindBy (xpath = "//a[contains(text(),'Place Order')]")
WebElement PlaceOrderButton;
@FindBy (xpath = "//h2[contains(text(),'Thank You! Order Received. ')]")
public WebElement OrderComplete;
@FindBy (xpath = "//a[contains(text(),'Download')]")
WebElement DownloadPDF;

public Purchase() {
	PageFactory.initElements(driver, this);
}

public void openProductCatalog() {
	hoverOverElement(driver,ShopTab);
	clickElement(ProductCatalogOption);
}
public void purchaseProduct(String category) throws InterruptedException {
	clickElement(driver.findElement(By.partialLinkText(category)));
	scrollToElement(driver,SelectProduct1);
	clickElement(SelectProduct1);
	if(isElementExisting(driver,SelectProduct2,2))
	clickElement(SelectProduct2);
	if(isElementExisting(driver,SelectProduct3,2))
	clickElement(SelectProduct3);
	clickElement(SelectToAddToCartButton);
}
public void continueShopping() {
	clickElement(KeepShoppingButton);
}
public void checkout() {
	clickElement(SecureCheckoutButton);
	clickElement(BillingAddressSection);
	if(BillingFirstName.getText()=="" && BillingFirstName.isEnabled())
	{
	setInput(BillingFirstName,"John");
	setInput(BillingLastName,"Mathews");
	setInput(BillingAddress,"72, Park Avenue");
	setInput(BillingCity,"Kansas");
	selEleByVisbleText(BillingState,"Delvine");
	setInput(BillingZip,"5600");
	}
	clickElement(PlaceOrderButton);
	clickElement(DownloadPDF);
}
}
